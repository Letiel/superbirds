<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class API extends CI_Controller{
	function __construct(){
		parent::__construct();
	}

	function pesquisa(){
		$this->load->model("AvesModel", "aves");
		// $post = $this->input->post();

		// ----------------------------------------
		$this->load->library("pagination");
		$cidade = $this->input->get("c");
		$pesquisa = $this->input->post("pesquisa");
		$pag = (int) $this->input->get("pn");
		$maximo = 2;
		$inicio = ($pag == null) ? 0 : $pag;
		if($inicio > 0)
			$inicio = $maximo * ($inicio -1);

		$config['base_url'] = "/lista/aves"."?p=$pesquisa".(!empty($cidade) ? "&c=$cidade" : "");
		$config['per_page'] = $maximo;
		$config['first_link'] = '<<';
		$config['last_link'] = '>>';
		$config['next_link'] = '>';
		$config['prev_link'] = '<';   
		$config['full_tag_open'] 	= '<ul class="pagination">';
		$config['full_tag_close'] 	= '</ul>';
		$config['num_tag_open'] 	= '<li class="waves-effect">';
		$config['num_tag_close'] 	= '</li>';
		$config['cur_tag_open'] 	= '<li class="active"><a>';
		$config['cur_tag_close'] 	= '</a></li>';
		$config['next_tag_open'] 	= '<li class="waves-effect">';
		$config['next_tagl_close'] 	= '</li>';
		$config['prev_tag_open'] 	= '<li class="waves-effect">';
		$config['prev_tagl_close'] 	= '</li>';
		$config['first_tag_open'] 	= '<li class="waves-effect">Primeira';
		$config['first_tagl_close'] = '</li>';
		$config['last_tag_open'] 	= '<li class="waves-effect">Última';
		$config['last_tagl_close'] 	= '</li>';
		$config["prev_link"] = "<i class='material-icons'>chevron_left</i>";
		$config["next_link"] = "<i class='material-icons'>chevron_right</i>";
		$config['use_page_numbers'] = TRUE;
		$config['enable_query_strings'] = TRUE;
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = "pn";
		// $config['reuse_query_string'] = TRUE;//aproveitar outros GET existentes
		// $config['uri_segment'] = 2;
		$config['num_links'] = 5;

		$config['total_rows'] = $this->aves->getListaAves($pesquisa, $cidade)->num_rows();
		$this->pagination->initialize($config);

		$aves = $this->aves->getListaAves($pesquisa, $cidade, $inicio, $maximo);

		// ----------------------------------------

		echo json_encode(array("aves"=>$aves->result(), "total"=>$aves->num_rows()));
	}

	function visitar_ave(){
		$this->load->model("AvesModel", "aves");
		$post = $this->input->post();

		echo json_encode($this->aves->getAveSite($post["id"])->first_row());
	}
}