<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Artigos extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model("ArtigosModel", "artigos");
		$this->usuarios->logado(true);
	}

	function index(){
		$limites = (object) $this->usuarios->anuncios_usuario();
		$this->load->library("pagination");
		$pag = (int) $this->uri->segment(2);
		$maximo = 16;
		$inicio = ($pag == null) ? 0 : $pag;
		if($inicio > 0)
			$inicio = $maximo * ($inicio -1);

		$config['base_url'] = "/artigos";
		$config['per_page'] = $maximo;
		$config['first_link'] = '<<';
		$config['last_link'] = '>>';
		$config['next_link'] = '>';
		$config['prev_link'] = '<';   
		$config['full_tag_open'] 	= '<ul class="pagination">';
		$config['full_tag_close'] 	= '</ul>';
		$config['num_tag_open'] 	= '<li class="waves-effect">';
		$config['num_tag_close'] 	= '</li>';
		$config['cur_tag_open'] 	= '<li class="active"><a>';
		$config['cur_tag_close'] 	= '</a></li>';
		$config['next_tag_open'] 	= '<li class="waves-effect">';
		$config['next_tagl_close'] 	= '</li>';
		$config['prev_tag_open'] 	= '<li class="waves-effect">';
		$config['prev_tagl_close'] 	= '</li>';
		$config['first_tag_open'] 	= '<li class="waves-effect">Primeira';
		$config['first_tagl_close'] = '</li>';
		$config['last_tag_open'] 	= '<li class="waves-effect">Última';
		$config['last_tagl_close'] 	= '</li>';
		$config["prev_link"] = "<i class='material-icons'>chevron_left</i>";
		$config["next_link"] = "<i class='material-icons'>chevron_right</i>";
		$config['use_page_numbers'] = TRUE;
		$config['enable_query_strings'] = TRUE;
		$config['page_query_string'] = FALSE;
		$config['uri_segment'] = 2;
		$config['num_links'] = 3;

		$config['total_rows'] = $this->artigos->getArtigos()->num_rows();
		$this->pagination->initialize($config);

		$artigos = $this->artigos->getArtigos($inicio, $maximo);
		$artigos = $artigos->result();
		$this->load->view("artigos", array("artigos"=>$artigos, "paginacao"=>$this->pagination->create_links(), "limites"=>$limites));
	}

	function alterar_situacao(){
		$this->form_validation->set_rules("id", "ID", "required|is_numeric");
		$this->form_validation->set_rules("situacao", "Situação", "required|max_length[1]");

		if($this->form_validation->run()){
			$this->artigos->alterar_situacao();
		}
	}
}