<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Criadores extends CI_Controller{
	function __construct(){
		parent::__construct();
	}

	function index(){
		$this->load->library("pagination");
		$cidade = $this->input->get("c");
		$pesquisa = $this->input->get("p");
		$pag = (int) $this->input->get("pag");
		$maximo = 12;
		$inicio = ($pag == null) ? 0 : $pag;
		if($inicio > 0)
			$inicio = $maximo * ($inicio -1);

		$config['base_url'] = "/criadores"."?p=$pesquisa".(!empty($cidade) ? "&c=$cidade" : "");
		$config['per_page'] = $maximo;
		$config['first_link'] = '<<';
		$config['last_link'] = '>>';
		$config['next_link'] = '>';
		$config['prev_link'] = '<';   
		$config['full_tag_open'] 	= '<ul class="pagination">';
		$config['full_tag_close'] 	= '</ul>';
		$config['num_tag_open'] 	= '<li class="waves-effect">';
		$config['num_tag_close'] 	= '</li>';
		$config['cur_tag_open'] 	= '<li class="active"><a>';
		$config['cur_tag_close'] 	= '</a></li>';
		$config['next_tag_open'] 	= '<li class="waves-effect">';
		$config['next_tagl_close'] 	= '</li>';
		$config['prev_tag_open'] 	= '<li class="waves-effect">';
		$config['prev_tagl_close'] 	= '</li>';
		$config['first_tag_open'] 	= '<li class="waves-effect">Primeira';
		$config['first_tagl_close'] = '</li>';
		$config['last_tag_open'] 	= '<li class="waves-effect">Última';
		$config['last_tagl_close'] 	= '</li>';
		$config["prev_link"] = "<i class='material-icons'>chevron_left</i>";
		$config["next_link"] = "<i class='material-icons'>chevron_right</i>";
		$config['use_page_numbers'] = TRUE;
		$config['enable_query_strings'] = TRUE;
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = "pag";
		// $config['reuse_query_string'] = TRUE;//aproveitar outros GET existentes
		// $config['uri_segment'] = 2;
		$config['num_links'] = 5;

		$config['total_rows'] = $this->usuarios->getCriadores($pesquisa, $cidade)->num_rows();
		$this->pagination->initialize($config);

		$criadores = $this->usuarios->getCriadores($pesquisa, $cidade, $inicio, $maximo)->result();
		foreach ($criadores as $criador) {
			$criador->total_aves = $this->usuarios->getTotalAvesCriador($criador->id)->first_row()->total_aves;
		}
		$this->load->view("site-criadores", array("criadores"=>$criadores, "paginacao"=>$this->pagination->create_links(), "pesquisa"=>$pesquisa));
	}

	function clubes(){
		$this->load->library("pagination");
		$cidade = $this->input->get("c");
		$pesquisa = $this->input->get("p");
		$pag = (int) $this->input->get("pag");
		$maximo = 12;
		$inicio = ($pag == null) ? 0 : $pag;
		if($inicio > 0)
			$inicio = $maximo * ($inicio -1);

		$config['base_url'] = "/clubes"."?p=$pesquisa".(!empty($cidade) ? "&c=$cidade" : "");
		$config['per_page'] = $maximo;
		$config['first_link'] = '<<';
		$config['last_link'] = '>>';
		$config['next_link'] = '>';
		$config['prev_link'] = '<';   
		$config['full_tag_open'] 	= '<ul class="pagination">';
		$config['full_tag_close'] 	= '</ul>';
		$config['num_tag_open'] 	= '<li class="waves-effect">';
		$config['num_tag_close'] 	= '</li>';
		$config['cur_tag_open'] 	= '<li class="active"><a>';
		$config['cur_tag_close'] 	= '</a></li>';
		$config['next_tag_open'] 	= '<li class="waves-effect">';
		$config['next_tagl_close'] 	= '</li>';
		$config['prev_tag_open'] 	= '<li class="waves-effect">';
		$config['prev_tagl_close'] 	= '</li>';
		$config['first_tag_open'] 	= '<li class="waves-effect">Primeira';
		$config['first_tagl_close'] = '</li>';
		$config['last_tag_open'] 	= '<li class="waves-effect">Última';
		$config['last_tagl_close'] 	= '</li>';
		$config["prev_link"] = "<i class='material-icons'>chevron_left</i>";
		$config["next_link"] = "<i class='material-icons'>chevron_right</i>";
		$config['use_page_numbers'] = TRUE;
		$config['enable_query_strings'] = TRUE;
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = "pag";
		// $config['reuse_query_string'] = TRUE;//aproveitar outros GET existentes
		// $config['uri_segment'] = 2;
		$config['num_links'] = 5;

		$config['total_rows'] = $this->usuarios->getClubes($pesquisa, $cidade)->num_rows();
		$this->pagination->initialize($config);

		$criadores = $this->usuarios->getClubes($pesquisa, $cidade, $inicio, $maximo)->result();
		$this->load->view("site-clubes", array("criadores"=>$criadores, "paginacao"=>$this->pagination->create_links(), "pesquisa"=>$pesquisa));
	}

	function aves(){
		$criador = $this->uri->segment(3);
		if(empty($criador) || !is_numeric($criador)){
			$this->session->set_flashdata("toast", "Materialize.toast('Criador não encontrado.', 10000, 'yellow');");
			redirect("/");
		}else{
			$this->load->model("AvesModel", "aves");
			$this->load->model("UsuariosModel", "usuarios");

			$criador = $this->usuarios->getCriador($criador);

			if($criador->num_rows() == 0){
				$this->session->set_flashdata("toast", "Materialize.toast('Criador não encontrado.', 10000, 'yellow');");
				redirect("/");
			}
			

			$criador = $criador->first_row();

			$this->load->library("pagination");
			$cidade = $this->input->get("c");
			$pesquisa = $this->input->get("p");
			$pag = (int) $this->input->get("pag");
			$maximo = 10;
			$inicio = ($pag == null) ? 0 : $pag;
			if($inicio > 0)
				$inicio = $maximo * ($inicio -1);

			$config['base_url'] = "/criadores/aves/$criador->id";
			$config['per_page'] = $maximo;
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['next_link'] = '>';
			$config['prev_link'] = '<';   
			$config['full_tag_open'] 	= '<ul class="pagination">';
			$config['full_tag_close'] 	= '</ul>';
			$config['num_tag_open'] 	= '<li class="waves-effect">';
			$config['num_tag_close'] 	= '</li>';
			$config['cur_tag_open'] 	= '<li class="active"><a>';
			$config['cur_tag_close'] 	= '</a></li>';
			$config['next_tag_open'] 	= '<li class="waves-effect">';
			$config['next_tagl_close'] 	= '</li>';
			$config['prev_tag_open'] 	= '<li class="waves-effect">';
			$config['prev_tagl_close'] 	= '</li>';
			$config['first_tag_open'] 	= '<li class="waves-effect">Primeira';
			$config['first_tagl_close'] = '</li>';
			$config['last_tag_open'] 	= '<li class="waves-effect">Última';
			$config['last_tagl_close'] 	= '</li>';
			$config["prev_link"] = "<i class='material-icons'>chevron_left</i>";
			$config["next_link"] = "<i class='material-icons'>chevron_right</i>";
			$config['use_page_numbers'] = TRUE;
			$config['enable_query_strings'] = TRUE;
			$config['page_query_string'] = TRUE;
			$config['query_string_segment'] = "pag";
			// $config['reuse_query_string'] = TRUE;//aproveitar outros GET existentes
			// $config['uri_segment'] = 2;
			$config['num_links'] = 5;

			$config['total_rows'] = $this->aves->getAvesCriador($criador->id)->num_rows();
			$this->pagination->initialize($config);

			$aves = $this->aves->getAvesCriador($criador->id, $maximo, $inicio)->result();
			$this->load->view("site-criador-aves", array("aves"=>$aves, "criador"=>$criador, "paginacao"=>$this->pagination->create_links()));

		}
	}

	function produtos(){
		$criador = $this->uri->segment(3);
		if(empty($criador) || !is_numeric($criador)){
			$this->session->set_flashdata("toast", "Materialize.toast('Criador não encontrado.', 10000, 'yellow');");
			redirect("/");
		}else{
			$this->load->model("ProdutosModel", "produtos");
			$this->load->model("UsuariosModel", "usuarios");

			$criador = $this->usuarios->getCriador($criador);

			if($criador->num_rows() == 0){
				$this->session->set_flashdata("toast", "Materialize.toast('Criador não encontrado.', 10000, 'yellow');");
				redirect("/");
			}
			

			$criador = $criador->first_row();

			$this->load->library("pagination");
			$cidade = $this->input->get("c");
			$pesquisa = $this->input->get("p");
			$pag = (int) $this->input->get("pag");
			$maximo = 10;
			$inicio = ($pag == null) ? 0 : $pag;
			if($inicio > 0)
				$inicio = $maximo * ($inicio -1);

			$config['base_url'] = "/criadores/produtos/$criador->id";
			$config['per_page'] = $maximo;
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['next_link'] = '>';
			$config['prev_link'] = '<';   
			$config['full_tag_open'] 	= '<ul class="pagination">';
			$config['full_tag_close'] 	= '</ul>';
			$config['num_tag_open'] 	= '<li class="waves-effect">';
			$config['num_tag_close'] 	= '</li>';
			$config['cur_tag_open'] 	= '<li class="active"><a>';
			$config['cur_tag_close'] 	= '</a></li>';
			$config['next_tag_open'] 	= '<li class="waves-effect">';
			$config['next_tagl_close'] 	= '</li>';
			$config['prev_tag_open'] 	= '<li class="waves-effect">';
			$config['prev_tagl_close'] 	= '</li>';
			$config['first_tag_open'] 	= '<li class="waves-effect">Primeira';
			$config['first_tagl_close'] = '</li>';
			$config['last_tag_open'] 	= '<li class="waves-effect">Última';
			$config['last_tagl_close'] 	= '</li>';
			$config["prev_link"] = "<i class='material-icons'>chevron_left</i>";
			$config["next_link"] = "<i class='material-icons'>chevron_right</i>";
			$config['use_page_numbers'] = TRUE;
			$config['enable_query_strings'] = TRUE;
			$config['page_query_string'] = TRUE;
			$config['query_string_segment'] = "pag";
			// $config['reuse_query_string'] = TRUE;//aproveitar outros GET existentes
			// $config['uri_segment'] = 2;
			$config['num_links'] = 5;

			$config['total_rows'] = $this->produtos->getProdutosCriador($criador->id)->num_rows();
			$this->pagination->initialize($config);

			$produtos = $this->produtos->getProdutosCriador($criador->id, $maximo, $inicio)->result();
			$this->load->view("site-criador-produtos", array("produtos"=>$produtos, "criador"=>$criador, "paginacao"=>$this->pagination->create_links()));

		}
	}

	function servicos(){
		$criador = $this->uri->segment(3);
		if(empty($criador) || !is_numeric($criador)){
			$this->session->set_flashdata("toast", "Materialize.toast('Criador não encontrado.', 10000, 'yellow');");
			redirect("/");
		}else{
			$this->load->model("ServicosModel", "servicos");
			$this->load->model("UsuariosModel", "usuarios");

			$criador = $this->usuarios->getCriador($criador);

			if($criador->num_rows() == 0){
				$this->session->set_flashdata("toast", "Materialize.toast('Criador não encontrado.', 10000, 'yellow');");
				redirect("/");
			}
			

			$criador = $criador->first_row();

			$this->load->library("pagination");
			$cidade = $this->input->get("c");
			$pesquisa = $this->input->get("p");
			$pag = (int) $this->input->get("pag");
			$maximo = 10;
			$inicio = ($pag == null) ? 0 : $pag;
			if($inicio > 0)
				$inicio = $maximo * ($inicio -1);

			$config['base_url'] = "/criadores/servicos/$criador->id";
			$config['per_page'] = $maximo;
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['next_link'] = '>';
			$config['prev_link'] = '<';   
			$config['full_tag_open'] 	= '<ul class="pagination">';
			$config['full_tag_close'] 	= '</ul>';
			$config['num_tag_open'] 	= '<li class="waves-effect">';
			$config['num_tag_close'] 	= '</li>';
			$config['cur_tag_open'] 	= '<li class="active"><a>';
			$config['cur_tag_close'] 	= '</a></li>';
			$config['next_tag_open'] 	= '<li class="waves-effect">';
			$config['next_tagl_close'] 	= '</li>';
			$config['prev_tag_open'] 	= '<li class="waves-effect">';
			$config['prev_tagl_close'] 	= '</li>';
			$config['first_tag_open'] 	= '<li class="waves-effect">Primeira';
			$config['first_tagl_close'] = '</li>';
			$config['last_tag_open'] 	= '<li class="waves-effect">Última';
			$config['last_tagl_close'] 	= '</li>';
			$config["prev_link"] = "<i class='material-icons'>chevron_left</i>";
			$config["next_link"] = "<i class='material-icons'>chevron_right</i>";
			$config['use_page_numbers'] = TRUE;
			$config['enable_query_strings'] = TRUE;
			$config['page_query_string'] = TRUE;
			$config['query_string_segment'] = "pag";
			// $config['reuse_query_string'] = TRUE;//aproveitar outros GET existentes
			// $config['uri_segment'] = 2;
			$config['num_links'] = 5;

			$config['total_rows'] = $this->servicos->getServicosCriador($criador->id)->num_rows();
			$this->pagination->initialize($config);

			$servicos = $this->servicos->getServicosCriador($criador->id, $maximo, $inicio)->result();
			$this->load->view("site-criador-servicos", array("servicos"=>$servicos, "criador"=>$criador, "paginacao"=>$this->pagination->create_links()));

		}
	}

	function artigos(){
		$criador = $this->uri->segment(3);
		if(empty($criador) || !is_numeric($criador)){
			$this->session->set_flashdata("toast", "Materialize.toast('Criador não encontrado.', 10000, 'yellow');");
			redirect("/");
		}else{
			$this->load->model("ServicosModel", "servicos");
			$this->load->model("UsuariosModel", "usuarios");
			$this->load->model("EventosModel", "eventos");
			$this->load->model("ArtigosModel", "artigos");

			$criador = $this->usuarios->getCriador($criador);

			if($criador->num_rows() == 0){
				$this->session->set_flashdata("toast", "Materialize.toast('Criador não encontrado.', 10000, 'yellow');");
				redirect("/");
			}
			

			$criador = $criador->first_row();

			$this->load->library("pagination");
			$cidade = $this->input->get("c");
			$pesquisa = $this->input->get("p");
			$pag = (int) $this->input->get("pag");
			$maximo = 10;
			$inicio = ($pag == null) ? 0 : $pag;
			if($inicio > 0)
				$inicio = $maximo * ($inicio -1);

			$config['base_url'] = "/criadores/artigos/$criador->id";
			$config['per_page'] = $maximo;
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['next_link'] = '>';
			$config['prev_link'] = '<';   
			$config['full_tag_open'] 	= '<ul class="pagination">';
			$config['full_tag_close'] 	= '</ul>';
			$config['num_tag_open'] 	= '<li class="waves-effect">';
			$config['num_tag_close'] 	= '</li>';
			$config['cur_tag_open'] 	= '<li class="active"><a>';
			$config['cur_tag_close'] 	= '</a></li>';
			$config['next_tag_open'] 	= '<li class="waves-effect">';
			$config['next_tagl_close'] 	= '</li>';
			$config['prev_tag_open'] 	= '<li class="waves-effect">';
			$config['prev_tagl_close'] 	= '</li>';
			$config['first_tag_open'] 	= '<li class="waves-effect">Primeira';
			$config['first_tagl_close'] = '</li>';
			$config['last_tag_open'] 	= '<li class="waves-effect">Última';
			$config['last_tagl_close'] 	= '</li>';
			$config["prev_link"] = "<i class='material-icons'>chevron_left</i>";
			$config["next_link"] = "<i class='material-icons'>chevron_right</i>";
			$config['use_page_numbers'] = TRUE;
			$config['enable_query_strings'] = TRUE;
			$config['page_query_string'] = TRUE;
			$config['query_string_segment'] = "pag";
			// $config['reuse_query_string'] = TRUE;//aproveitar outros GET existentes
			// $config['uri_segment'] = 2;
			$config['num_links'] = 5;

			$config['total_rows'] = $this->artigos->getArtigosCriador($criador->id)->num_rows();
			$this->pagination->initialize($config);

			$artigos = $this->artigos->getArtigosCriador($criador->id, $maximo, $inicio)->result();
			$this->load->view("site-criador-artigos", array("artigos"=>$artigos, "criador"=>$criador, "paginacao"=>$this->pagination->create_links()));

		}
	}

	function eventos(){
		$criador = $this->uri->segment(3);
		if(empty($criador) || !is_numeric($criador)){
			$this->session->set_flashdata("toast", "Materialize.toast('Criador não encontrado.', 10000, 'yellow');");
			redirect("/");
		}else{
			$this->load->model("ServicosModel", "servicos");
			$this->load->model("UsuariosModel", "usuarios");
			$this->load->model("EventosModel", "eventos");

			$criador = $this->usuarios->getCriador($criador);

			if($criador->num_rows() == 0){
				$this->session->set_flashdata("toast", "Materialize.toast('Criador não encontrado.', 10000, 'yellow');");
				redirect("/");
			}
			

			$criador = $criador->first_row();

			$this->load->library("pagination");
			$cidade = $this->input->get("c");
			$pesquisa = $this->input->get("p");
			$pag = (int) $this->input->get("pag");
			$maximo = 10;
			$inicio = ($pag == null) ? 0 : $pag;
			if($inicio > 0)
				$inicio = $maximo * ($inicio -1);

			$config['base_url'] = "/criadores/eventos/$criador->id";
			$config['per_page'] = $maximo;
			$config['first_link'] = '<<';
			$config['last_link'] = '>>';
			$config['next_link'] = '>';
			$config['prev_link'] = '<';   
			$config['full_tag_open'] 	= '<ul class="pagination">';
			$config['full_tag_close'] 	= '</ul>';
			$config['num_tag_open'] 	= '<li class="waves-effect">';
			$config['num_tag_close'] 	= '</li>';
			$config['cur_tag_open'] 	= '<li class="active"><a>';
			$config['cur_tag_close'] 	= '</a></li>';
			$config['next_tag_open'] 	= '<li class="waves-effect">';
			$config['next_tagl_close'] 	= '</li>';
			$config['prev_tag_open'] 	= '<li class="waves-effect">';
			$config['prev_tagl_close'] 	= '</li>';
			$config['first_tag_open'] 	= '<li class="waves-effect">Primeira';
			$config['first_tagl_close'] = '</li>';
			$config['last_tag_open'] 	= '<li class="waves-effect">Última';
			$config['last_tagl_close'] 	= '</li>';
			$config["prev_link"] = "<i class='material-icons'>chevron_left</i>";
			$config["next_link"] = "<i class='material-icons'>chevron_right</i>";
			$config['use_page_numbers'] = TRUE;
			$config['enable_query_strings'] = TRUE;
			$config['page_query_string'] = TRUE;
			$config['query_string_segment'] = "pag";
			// $config['reuse_query_string'] = TRUE;//aproveitar outros GET existentes
			// $config['uri_segment'] = 2;
			$config['num_links'] = 5;

			$config['total_rows'] = $this->eventos->getEventosCriador($criador->id)->num_rows();
			$this->pagination->initialize($config);

			$eventos = $this->eventos->getEventosCriador($criador->id, $maximo, $inicio)->result();
			$this->load->view("site-criador-eventos", array("eventos"=>$eventos, "criador"=>$criador, "paginacao"=>$this->pagination->create_links()));

		}
	}

	function ver(){
		$criador = (int) $this->uri->segment("3");
		$criador = $this->usuarios->getCriador($criador);
		if($criador->num_rows() > 0){
			$criador = $criador->first_row();
			if(!empty($criador->superbirds_subdominio)){
				redirect($criador->superbirds_subdominio);
			}

			$this->load->model("AvesModel", "aves");
			$this->load->model("ProdutosModel", "produtos");
			$this->load->model("ServicosModel", "servicos");
			$this->load->model("ArtigosModel", "artigos");
			$this->load->model("EventosModel", "eventos");

			$aves = $this->aves->getAvesCriador($criador->id, 3)->result();
			$produtos = $this->produtos->getProdutosCriador($criador->id, 3)->result();
			$servicos = $this->servicos->getServicosCriador($criador->id, 3)->result();
			$artigos = $this->artigos->getArtigosCriador($criador->id, 3)->result();
			$eventos = $this->eventos->getEventosCriador($criador->id, 3)->result();
			$this->load->view("site-ver-criador", array("criador"=>$criador, "aves"=>$aves, "produtos"=>$produtos, "servicos"=>$servicos, "artigos"=>$artigos, "eventos"=>$eventos));
		}else{
			$this->session->set_flashdata("toast", "Materialize.toast('Criador não encontrado.', 10000, 'yellow');");
			redirect("/");
		}
	}

	function contato(){
		$this->form_validation->set_error_delimiters("", "");

		$this->form_validation->set_rules("id", "Id do criador", "required|is_numeric");
		$this->form_validation->set_rules("nome", "Nome", "required|max_length[100]");
		$this->form_validation->set_rules("email", "E-mail", "required|max_length[100]|valid_email");
		$this->form_validation->set_rules("mensagem", "Mensagem", "required|nl2br");

		if($this->form_validation->run()){

			$post = $this->input->post();

			$this->load->model("UsuariosModel", "usuarios");
			$usuario = $this->usuarios->getCriador($post["id"]);
			if($usuario->num_rows() != 1){
				echo "Usuário não encontrado!";
				exit();
			}

			$usuario = $usuario->first_row();

			$this->load->library('email');

			$config['charset'] = 'utf-8';
			$config['wordwrap'] = TRUE;
			$config['mailtype'] = "html";

			$this->email->initialize($config);

			$this->email->from('contato@superbirds.acessonaweb.com.br', 'SuperBirds');

			if($usuario->email_form_contato == "U"){/*CONTATO DEVE APONTAR PARA EMAIL DO USUÁRIO OU EMAIL DO SUPERBIRDS*/
				if(!empty($usuario->email))
					$this->email->to($usuario->email);
				else
					echo "$usuario->nome não possui um um e-mail de contato inválido.";
			}else{
				if(!empty($usuario->superbirds_email))
					$this->email->to($usuario->superbirds_email);
				else
					echo "$usuario->nome não possui um um e-mail de contato inválido.";
			}

			$this->email->subject('Contato - SuperBirds');
			$this->email->message("
			<h3>Contato pelo site.</h3>
			<h4>Você recebeu um contato através do botão de contato no <a href='".base_url()."'>Superbirds</a>.</h4>
			<br>
			<p>Nome: <b>$post[nome]</b></p>
			<p>E-mail: <b>$post[email]</b></p>
			<p>Mensagem: <b>$post[mensagem]</b></p>");

			if($this->email->send()){
				$this->session->set_flashdata("toast", "Materialize.toast('Mensagem enviada com sucesso.', 5000, 'blue');");
			}else{
				$this->session->set_flashdata("toast", "Materialize.toast('<p>Não foi possível entrar em contato.', 70000, 'red');Materialize.toast('Envie um e-mail para&nbsp;<b>contato@superbirds.com.br</b>', 77000, 'red');");
			}
		}else{
			echo validation_errors();
		}
	}
}