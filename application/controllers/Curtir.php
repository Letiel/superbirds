<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Curtir extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->load->model("CurtidasModel", "curtidas");
	}

	function anuncio(){
		$id = $this->uri->segment(3);
		if(!empty($id) && is_numeric($id)){
			$anuncio = $this->curtidas->getAnuncio($id)->first_row();
			if($anuncio != null){
				$this->curtidas->curtirAnuncio($anuncio);
			}else{
				$this->session->set_flashdata("toast", "Materialize.toast('Anúncio não encontrado!', 5000, 'blue');");
				redirect("/");
			}
		}else{
			$this->session->set_flashdata("toast", "Materialize.toast('Anúncio não encontrado!', 5000, 'blue');");
			redirect("/");
		}
	}

	function artigo(){
		$id = $this->uri->segment(3);
		if(!empty($id) && is_numeric($id)){
			$this->curtidas->curtirArtigo($id);
		}else{
			$this->session->set_flashdata("toast", "Materialize.toast('Artigo não encontrado!', 5000, 'blue');");
			redirect("/");
		}
	}

	function evento(){
		$id = $this->uri->segment(3);
		if(!empty($id) && is_numeric($id)){
			$this->curtidas->curtirEvento($id);
		}else{
			$this->session->set_flashdata("toast", "Materialize.toast('Evento não encontrado!', 5000, 'blue');");
			redirect("/");
		}
	}

	function usuario(){
		$id = $this->uri->segment(3);
		if(!empty($id) && is_numeric($id)){
			$this->curtidas->curtirUsuario($id);
		}else{
			$this->session->set_flashdata("toast", "Materialize.toast('Usuário não encontrado!', 5000, 'blue');");
			redirect("/");
		}
	}

	function votar(){
		$this->form_validation->set_error_delimiters("", "");

		$this->form_validation->set_rules("id_anuncio", "Id", "required|is_numeric");
		$this->form_validation->set_rules("voto", "Voto", "required|is_numeric");

		if($this->form_validation->run()){
			$this->curtidas->votar_anuncio();
		}else{
			echo validation_errors();
		}
	}
}