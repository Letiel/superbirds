<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Denuncia extends CI_Controller{
	function __construct(){
		parent::__construct();
	}

	function realizar(){
		$post = $this->input->post();

		$this->load->library('email');

		$config['charset'] = 'utf-8';
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = "html";

		$this->email->initialize($config);

		$this->email->from('contato@superbirds.acessonaweb.com.br', 'SuperBirds');
		// $this->email->to("contato@superbirds.acessonaweb.com.br");
		$this->email->to("letiel@gmail.com");
		$this->email->subject('Denúncia - SuperBirds');

		switch ($post["tipo"]) {
			case 'ave':
				$this->load->model("AvesModel", "aves");
				$ave = $this->aves->getAveSite($post["id_anuncio"])->first_row();
				$this->email->message("
				<h3>Denúncia de anúncio no SuperBirds.</h3>
				<br>
				<p>Anúncio denunciado: <a href='".base_url()."ver/ave/$ave->id'>$ave->nome</b></p>
				<p>Usuário: <a href='".base_url()."criadores/ver/$ave->id_usuario'>$ave->nome_usuario</b></p>
				<p>Descrição da denúncia: <b>$post[descricao_denuncia]</b></p>");

				$this->db->flush_cache();
				$this->db->start_cache();
				$this->db->where("anuncios.id", $post["id_anuncio"]);
				$this->db->update("anuncios", array("denunciado"=>"D"));
				$this->db->stop_cache();
				$this->db->flush_cache();
				break;
			case 'produto':

				$this->load->model("ProdutosModel", "produtos");
				$produto = $this->produtos->getProdutoSite($post["id_anuncio"])->first_row();

				$this->email->message("
				<h3>Denúncia de anúncio no SuperBirds.</h3>
				<br>
				<p>Anúncio denunciado: <a href='".base_url()."ver/produto/$produto->id'>$produto->nome</b></p>
				<p>Usuário: <a href='".base_url()."criadores/ver/$produto->id_usuario'>$produto->nome_usuario</b></p>
				<p>Descrição da denúncia: <b>$post[descricao_denuncia]</b></p>");

				$this->db->flush_cache();
				$this->db->start_cache();
				$this->db->where("anuncios.id", $post["id_anuncio"]);
				$this->db->update("anuncios", array("denunciado"=>"D"));
				$this->db->stop_cache();
				$this->db->flush_cache();
				break;
			case 'servico':

				$this->load->model("ServicosModel", "servicos");
				$servico = $this->servicos->getServicoSite($post["id_anuncio"])->first_row();

				$this->email->message("
				<h3>Denúncia de anúncio no SuperBirds.</h3>
				<br>
				<p>Anúncio denunciado: <a href='".base_url()."ver/servico/$servico->id'>$servico->nome</b></p>
				<p>Usuário: <a href='".base_url()."criadores/ver/$servico->id_usuario'>$servico->nome_usuario</b></p>
				<p>Descrição da denúncia: <b>$post[descricao_denuncia]</b></p>");

				$this->db->flush_cache();
				$this->db->start_cache();
				$this->db->where("anuncios.id", $post["id_anuncio"]);
				$this->db->update("anuncios", array("denunciado"=>"D"));
				$this->db->stop_cache();
				$this->db->flush_cache();
				break;
			case 'artigo':
				$this->load->model("ArtigosModel", "artigos");
				$artigo = $this->artigos->getArtigoSite($post["id_artigo"])->first_row();

				$this->email->message("
				<h3>Denúncia de anúncio no SuperBirds.</h3>
				<br>
				<p>Anúncio denunciado: <a href='".base_url()."ver/artigo/$artigo->id'>$artigo->nome</b></p>
				<p>Usuário: <a href='".base_url()."criadores/ver/$artigo->id_usuario'>$artigo->nome_usuario</b></p>
				<p>Descrição da denúncia: <b>$post[descricao_denuncia]</b></p>");

				$this->db->flush_cache();
				$this->db->start_cache();
				$this->db->where("artigos.id", $post["id_artigo"]);
				$this->db->update("artigos", array("denunciado"=>"D"));
				$this->db->stop_cache();
				$this->db->flush_cache();
				break;
			case 'evento':
				$this->load->model("EventosModel", "eventos");
				$evento = $this->eventos->getEventoSite($post["id_evento"])->first_row();

				$this->email->message("
				<h3>Denúncia de anúncio no SuperBirds.</h3>
				<br>
				<p>Anúncio denunciado: <a href='".base_url()."ver/evento/$evento->id'>$evento->nome</b></p>
				<p>Usuário: <a href='".base_url()."criadores/ver/$evento->id_usuario'>$evento->nome_usuario</b></p>
				<p>Descrição da denúncia: <b>$post[descricao_denuncia]</b></p>");

				$this->db->flush_cache();
				$this->db->start_cache();
				$this->db->where("eventos.id", $post["id_evento"]);
				$this->db->update("eventos", array("denunciado"=>"D"));
				$this->db->stop_cache();
				$this->db->flush_cache();
				break;
			case 'usuario':
				$this->load->model("UsuariosModel", "usuarios");
				$usuario = $this->usuarios->getCriador($post["id_criador"])->first_row();

				$this->email->message("
				<h3>Denúncia de usuário no SuperBirds.</h3>
				<br>
				<p>Usuário denunciado: <a href='".base_url()."criadores/ver/$usuario->id'>$usuario->nome</b></p>
				<p>Descrição da denúncia: <b>$post[descricao_denuncia]</b></p>");

				$this->db->flush_cache();
				$this->db->start_cache();
				$this->db->where("usuarios.id", $post["id_criador"]);
				$this->db->update("usuarios", array("denunciado"=>"D"));
				$this->db->stop_cache();
				$this->db->flush_cache();
				break;
		}

		if($this->email->send()){
			$this->session->set_flashdata("toast", "Materialize.toast('Denúncia realizada com sucesso.', 5000, 'blue');Materialize.toast('Aguarde avaliação da administração.', 7000, 'blue');");
		}else{
			$this->session->set_flashdata("toast", "Materialize.toast('<p>Não foi possível realizar a denúncia.', 70000, 'red');Materialize.toast('Envie um e-mail para&nbsp;<b>contato@superbirds.com.br</b>', 77000, 'red');");
		}
		redirect("/");
	}
}