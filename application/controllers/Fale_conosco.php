<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fale_conosco extends CI_Controller{
	function __construct(){
		parent::__construct();
	}

	function index(){

		$this->form_validation->set_error_delimiters("<p class='red-text'>", "</p>");
		$this->form_validation->set_rules("nome", "Nome", "required");
		$this->form_validation->set_rules("email", "E-mail", "required|valid_email");
		$this->form_validation->set_rules("descricao", "Mensagem", "required");
		if($this->form_validation->run()){
			$post = $this->input->post();

			$this->load->library('email');

			$config['charset'] = 'utf-8';
			$config['wordwrap'] = TRUE;
			$config['mailtype'] = "html";

			$this->email->initialize($config);

			$this->email->from('letiel@superbirds.acessonaweb.com.br', 'SuperBirds');
			$this->email->to("letiel@gmail.com");
			$this->email->subject('Contato - SuperBirds');
			$this->email->message("
			<h3>Contato pelo site.</h3>
			<br>
			<p>Nome: <b>$post[nome]</b></p>
			<p>E-mail: <b>$post[email]</b></p>
			<p>Mensagem: <b>$post[descricao]</b></p>");

			if($this->email->send()){
				$this->session->set_flashdata("toast", "Materialize.toast('Mensagem enviada com sucesso.', 5000, 'blue');Materialize.toast('Aguarde nosso contato.', 7000, 'blue');");
				redirect("/");
			}else{
				$this->session->set_flashdata("toast", "Materialize.toast('<p>Não foi possível entrar em contato.', 70000, 'red');Materialize.toast('Envie um e-mail para&nbsp;<b>contato@superbirds.com.br</b>', 77000, 'red');");
			}
			

		}

		$this->load->view("site-fale-conosco");
	}
}