<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
		parent::__construct();
	}

	public function index(){
		$this->load->model("HomeModel", "home");
		$aves_destaque = $this->home->getAvesDestaque(12)->result();
		$aves_famosas = $this->home->getAvesFamosas(12)->result();
		$aves_curtidas = $this->home->getAvesCurtidas(12)->result();
		$total_anuncios = $this->home->getTotalAnuncios()->num_rows();
		$proximos_eventos = $this->home->getProximosEventos(null, 3)->result();
		$this->load->view("home", array(
			"aves_destaque"=>$aves_destaque,
			"aves_famosas"=>$aves_famosas,
			"aves_curtidas"=>$aves_curtidas,
			"total_anuncios"=>$total_anuncios,
			"proximos_eventos"=>$proximos_eventos
		));
	}


	public function interno(){
		if($this->usuarios->logado()){
			$this->load->model("AvesModel", "aves");
			$this->load->model("ProdutosModel", "produtos");
			$this->load->model("ServicosModel", "servicos");
			$this->load->view('interno', array(
				"aves"=>$this->aves->getAves()->num_rows(),
				"produtos"=>$this->produtos->getProdutos()->num_rows(),
				"servicos"=>$this->servicos->getServicos()->num_rows(),
			));
		}
		else
			$this->load->view('login');
	}

	public function confirmar_email(){
		$this->usuarios->confirmar_email();
	}

	public function login(){
		$this->form_validation->set_rules("login", "Login", "required");
		$this->form_validation->set_rules("senha", "Senha", "required");
		if($this->form_validation->run()){
			$this->usuarios->login();
		}
		$this->load->view('login');
	}

	public function logout(){
		$this->session->unset_userdata("logado");
		$this->session->unset_userdata("id");
		$this->session->unset_userdata("login");
		$this->session->unset_userdata("tipo");
		redirect(base_url()."login");
	}

	public function cadastro(){
		$this->load->model("PlanosModel", "planos");
		$this->load->model("CidadesModel", "cidades");
		function validar_cpf($cpf){
			$cpf = preg_replace('/[^0-9]/', '', (string) $cpf);
			// Valida tamanho
			if (strlen($cpf) != 11)
				return false;
			// Calcula e confere primeiro dígito verificador
			for ($i = 0, $j = 10, $soma = 0; $i < 9; $i++, $j--)
				$soma += $cpf{$i} * $j;
			$resto = $soma % 11;
			if ($cpf{9} != ($resto < 2 ? 0 : 11 - $resto))
				return false;
			// Calcula e confere segundo dígito verificador
			for ($i = 0, $j = 11, $soma = 0; $i < 10; $i++, $j--)
				$soma += $cpf{$i} * $j;
			$resto = $soma % 11;
			return $cpf{10} == ($resto < 2 ? 0 : 11 - $resto);
		}

		function validar_cnpj($cnpj){
			$cnpj = preg_replace('/[^0-9]/', '', (string) $cnpj);
			// Valida tamanho
			if (strlen($cnpj) != 14)
				return false;
			// Valida primeiro dígito verificador
			for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++)
			{
				$soma += $cnpj{$i} * $j;
				$j = ($j == 2) ? 9 : $j - 1;
			}
			$resto = $soma % 11;
			if ($cnpj{12} != ($resto < 2 ? 0 : 11 - $resto))
				return false;
			// Valida segundo dígito verificador
			for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++)
			{
				$soma += $cnpj{$i} * $j;
				$j = ($j == 2) ? 9 : $j - 1;
			}
			$resto = $soma % 11;
			return $cnpj{13} == ($resto < 2 ? 0 : 11 - $resto);
		}

		$cidades = $this->cidades->getCidades()->result();
		$planos = $this->planos->getValores()->result();

		$tipo = $this->input->post("tipo_documento");
		$this->form_validation->set_rules("nome", "Nome", "required");
		if($tipo == "cpf"){
			$this->form_validation->set_rules("cpfcnpj", "CPF", "required|validar_cpf", array("validar_cpf"=>"CPF inválido."));
		}else if($tipo == "cnpj"){
			$this->form_validation->set_rules("cpfcnpj", "CNPJ", "required|validar_cnpj", array("validar_cnpj"=>"CNPJ inválido."));
		}
		$this->form_validation->set_rules("id_cidade", "Cidade", "required");
		$this->form_validation->set_rules("email", "E-mail", "required|valid_email");
		$this->form_validation->set_rules("ramo_principal", "Ramo Principal", "required");
		$this->form_validation->set_rules("id_plano", "Plano", "required");
		$this->form_validation->set_rules("superbirds_usuario", "Login", "required|is_unique[usuarios.superbirds_usuario]", array("is_unique"=>"Login já existente."));
		$this->form_validation->set_rules("superbirds_senha", "Senha", "required");
		$this->form_validation->set_rules("confirma_senha", "Confirmação de Senha", "required|matches[superbirds_senha]");

		if($this->form_validation->run()){
			$this->usuarios->cadastrar();
		}else{ //preciso pegar o nome da cidade para deixar o select selecionado
			$cidade = $this->input->post("id_cidade");
			if(!empty($cidade))
				$this->session->set_flashdata("cidade", $this->cidades->getCidade($cidade)->first_row()->nome);
		}

		$this->load->view("cadastro", array("planos"=>$planos, "cidades"=>$cidades));
	}

	public function quem_somos(){
		$this->load->view("site-quem-somos");
	}

	public function termos_de_uso(){
		$this->load->view("site-termos-de-uso");
	}
}
