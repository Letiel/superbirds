<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lista extends CI_Controller{
	function __construct(){
		parent::__construct();
	}

	function aves(){
		$this->load->model("AvesModel", "aves");

		$this->load->library("pagination");
		$cidade = $this->input->get("c");
		$pesquisa = $this->input->get("p");

		$classe = $this->input->get("classe");
		$mutacao = $this->input->get("mutacao");
		$cor = $this->input->get("cor");
		$unidade = $this->input->get("unidade");

		$pag = (int) $this->input->get("pag");
		$maximo = 12;
		$inicio = ($pag == null) ? 0 : $pag;
		if($inicio > 0)
			$inicio = $maximo * ($inicio -1);

		$config['base_url'] = "/lista/aves"."?p=$pesquisa".(!empty($cidade) ? "&c=$cidade" : "").(!empty($classe) ? "&classe=$classe" : "").(!empty($mutacao) ? "&mutacao=$mutacao" : "").(!empty($mutacao) ? "&cor=$cor" : "").(!empty($unidade) ? "&unidade=$unidade" : "");
		$config['per_page'] = $maximo;
		$config['first_link'] = '<<';
		$config['last_link'] = '>>';
		$config['next_link'] = '>';
		$config['prev_link'] = '<';   
		$config['full_tag_open'] 	= '<ul class="pagination">';
		$config['full_tag_close'] 	= '</ul>';
		$config['num_tag_open'] 	= '<li class="waves-effect">';
		$config['num_tag_close'] 	= '</li>';
		$config['cur_tag_open'] 	= '<li class="active"><a>';
		$config['cur_tag_close'] 	= '</a></li>';
		$config['next_tag_open'] 	= '<li class="waves-effect">';
		$config['next_tagl_close'] 	= '</li>';
		$config['prev_tag_open'] 	= '<li class="waves-effect">';
		$config['prev_tagl_close'] 	= '</li>';
		$config['first_tag_open'] 	= '<li class="waves-effect">Primeira';
		$config['first_tagl_close'] = '</li>';
		$config['last_tag_open'] 	= '<li class="waves-effect">Última';
		$config['last_tagl_close'] 	= '</li>';
		$config["prev_link"] = "<i class='material-icons'>chevron_left</i>";
		$config["next_link"] = "<i class='material-icons'>chevron_right</i>";
		$config['use_page_numbers'] = TRUE;
		$config['enable_query_strings'] = TRUE;
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = "pag";
		// $config['reuse_query_string'] = TRUE;//aproveitar outros GET existentes
		// $config['uri_segment'] = 2;
		$config['num_links'] = 5;

		$config['total_rows'] = $this->aves->getListaAves($pesquisa, $cidade)->num_rows();

		$classes = $this->aves->getClasses()->result();
		$mutacoes = $this->aves->getMutacoes()->result();
		$cores = $this->aves->getCores()->result();
		$unidades = $this->aves->getUnidades()->result();

		$this->pagination->initialize($config);

		$this->load->model("cidadesModel", "cidades");
		$cidade = $this->cidades->getCidade($cidade)->first_row();

		$aves = $this->aves->getListaAves($pesquisa, (empty($cidade->id) ? null : $cidade->id), $inicio, $maximo)->result();
		$this->load->view("site-aves", array("aves"=>$aves, "classes"=>$classes, "mutacoes"=>$mutacoes, "cores"=>$cores, "unidades"=>$unidades, "paginacao"=>$this->pagination->create_links(), "pesquisa"=>$pesquisa, "classe_"=>$classe, "mutacao_"=>$mutacao, "cor_"=>$cor, "unidade_"=>$unidade, "cidade_"=>empty($cidade->id) ? "" : $cidade->id, "cidade_nome"=>empty($cidade->nome) ? "" : $cidade->nome, "cidade_uf"=>empty($cidade->uf) ? "" : $cidade->uf));
	}

	function produtos(){
		$this->load->model("ProdutosModel", "produtos");

		$this->load->library("pagination");
		$cidade = $this->input->get("c");
		$pesquisa = $this->input->get("p");
		$pag = (int) $this->input->get("pag");
		$maximo = 10;
		$inicio = ($pag == null) ? 0 : $pag;
		if($inicio > 0)
			$inicio = $maximo * ($inicio -1);

		$config['base_url'] = "/lista/produtos"."?p=$pesquisa".(!empty($cidade) ? "&c=$cidade" : "");
		$config['per_page'] = $maximo;
		$config['first_link'] = '<<';
		$config['last_link'] = '>>';
		$config['next_link'] = '>';
		$config['prev_link'] = '<';   
		$config['full_tag_open'] 	= '<ul class="pagination">';
		$config['full_tag_close'] 	= '</ul>';
		$config['num_tag_open'] 	= '<li class="waves-effect">';
		$config['num_tag_close'] 	= '</li>';
		$config['cur_tag_open'] 	= '<li class="active"><a>';
		$config['cur_tag_close'] 	= '</a></li>';
		$config['next_tag_open'] 	= '<li class="waves-effect">';
		$config['next_tagl_close'] 	= '</li>';
		$config['prev_tag_open'] 	= '<li class="waves-effect">';
		$config['prev_tagl_close'] 	= '</li>';
		$config['first_tag_open'] 	= '<li class="waves-effect">Primeira';
		$config['first_tagl_close'] = '</li>';
		$config['last_tag_open'] 	= '<li class="waves-effect">Última';
		$config['last_tagl_close'] 	= '</li>';
		$config["prev_link"] = "<i class='material-icons'>chevron_left</i>";
		$config["next_link"] = "<i class='material-icons'>chevron_right</i>";
		$config['use_page_numbers'] = TRUE;
		$config['enable_query_strings'] = TRUE;
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = "pag";
		// $config['reuse_query_string'] = TRUE;//aproveitar outros GET existentes
		// $config['uri_segment'] = 2;
		$config['num_links'] = 5;

		$config['total_rows'] = $this->produtos->getListaProdutos($pesquisa, $cidade)->num_rows();
		$this->pagination->initialize($config);

		$produtos = $this->produtos->getListaProdutos($pesquisa, $cidade, $inicio, $maximo)->result();
		$this->load->view("site-produtos", array("produtos"=>$produtos, "paginacao"=>$this->pagination->create_links(), "pesquisa"=>$pesquisa));
	}

	function servicos(){
		$this->load->model("ServicosModel", "servicos");

		$this->load->library("pagination");
		$cidade = $this->input->get("c");
		$pesquisa = $this->input->get("p");
		$pag = (int) $this->input->get("pag");
		$maximo = 10;
		$inicio = ($pag == null) ? 0 : $pag;
		if($inicio > 0)
			$inicio = $maximo * ($inicio -1);

		$config['base_url'] = "/lista/servicos"."?p=$pesquisa".(!empty($cidade) ? "&c=$cidade" : "");
		$config['per_page'] = $maximo;
		$config['first_link'] = '<<';
		$config['last_link'] = '>>';
		$config['next_link'] = '>';
		$config['prev_link'] = '<';   
		$config['full_tag_open'] 	= '<ul class="pagination">';
		$config['full_tag_close'] 	= '</ul>';
		$config['num_tag_open'] 	= '<li class="waves-effect">';
		$config['num_tag_close'] 	= '</li>';
		$config['cur_tag_open'] 	= '<li class="active"><a>';
		$config['cur_tag_close'] 	= '</a></li>';
		$config['next_tag_open'] 	= '<li class="waves-effect">';
		$config['next_tagl_close'] 	= '</li>';
		$config['prev_tag_open'] 	= '<li class="waves-effect">';
		$config['prev_tagl_close'] 	= '</li>';
		$config['first_tag_open'] 	= '<li class="waves-effect">Primeira';
		$config['first_tagl_close'] = '</li>';
		$config['last_tag_open'] 	= '<li class="waves-effect">Última';
		$config['last_tagl_close'] 	= '</li>';
		$config["prev_link"] = "<i class='material-icons'>chevron_left</i>";
		$config["next_link"] = "<i class='material-icons'>chevron_right</i>";
		$config['use_page_numbers'] = TRUE;
		$config['enable_query_strings'] = TRUE;
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = "pag";
		// $config['reuse_query_string'] = TRUE;//aproveitar outros GET existentes
		// $config['uri_segment'] = 2;
		$config['num_links'] = 5;

		$config['total_rows'] = $this->servicos->getListaServicos($pesquisa, $cidade)->num_rows();
		$this->pagination->initialize($config);

		$servicos = $this->servicos->getListaServicos($pesquisa, $cidade, $inicio, $maximo)->result();
		$this->load->view("site-servicos", array("servicos"=>$servicos, "paginacao"=>$this->pagination->create_links(), "pesquisa"=>$pesquisa));
	}

	function artigos(){
		$this->load->model("ArtigosModel", "artigos");

		$this->load->library("pagination");
		$cidade = $this->input->get("c");
		$pesquisa = $this->input->get("p");
		$pag = (int) $this->input->get("pag");
		$maximo = 1;
		$inicio = ($pag == null) ? 0 : $pag;
		if($inicio > 0)
			$inicio = $maximo * ($inicio -1);

		$config['base_url'] = "/lista/artigos"."?p=$pesquisa".(!empty($cidade) ? "&c=$cidade" : "");
		$config['per_page'] = $maximo;
		$config['first_link'] = '<<';
		$config['last_link'] = '>>';
		$config['next_link'] = '>';
		$config['prev_link'] = '<';   
		$config['full_tag_open'] 	= '<ul class="pagination">';
		$config['full_tag_close'] 	= '</ul>';
		$config['num_tag_open'] 	= '<li class="waves-effect">';
		$config['num_tag_close'] 	= '</li>';
		$config['cur_tag_open'] 	= '<li class="active"><a>';
		$config['cur_tag_close'] 	= '</a></li>';
		$config['next_tag_open'] 	= '<li class="waves-effect">';
		$config['next_tagl_close'] 	= '</li>';
		$config['prev_tag_open'] 	= '<li class="waves-effect">';
		$config['prev_tagl_close'] 	= '</li>';
		$config['first_tag_open'] 	= '<li class="waves-effect">Primeira';
		$config['first_tagl_close'] = '</li>';
		$config['last_tag_open'] 	= '<li class="waves-effect">Última';
		$config['last_tagl_close'] 	= '</li>';
		$config["prev_link"] = "<i class='material-icons'>chevron_left</i>";
		$config["next_link"] = "<i class='material-icons'>chevron_right</i>";
		$config['use_page_numbers'] = TRUE;
		$config['enable_query_strings'] = TRUE;
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = "pag";
		// $config['reuse_query_string'] = TRUE;//aproveitar outros GET existentes
		// $config['uri_segment'] = 2;
		$config['num_links'] = 5;

		$config['total_rows'] = $this->artigos->getListaArtigos($pesquisa, $cidade)->num_rows();
		$this->pagination->initialize($config);

		$artigos = $this->artigos->getListaArtigos($pesquisa, $cidade, $inicio, $maximo)->result();
		$this->load->view("site-artigos", array("artigos"=>$artigos, "paginacao"=>$this->pagination->create_links(), "pesquisa"=>$pesquisa));
	}

	function eventos(){
		$this->load->model("EventosModel", "eventos");

		$this->load->library("pagination");
		$cidade = $this->input->get("c");
		$pesquisa = $this->input->get("p");
		$pag = (int) $this->input->get("pag");
		$maximo = 1;
		$inicio = ($pag == null) ? 0 : $pag;
		if($inicio > 0)
			$inicio = $maximo * ($inicio -1);

		$config['base_url'] = "/lista/eventos"."?p=$pesquisa".(!empty($cidade) ? "&c=$cidade" : "");
		$config['per_page'] = $maximo;
		$config['first_link'] = '<<';
		$config['last_link'] = '>>';
		$config['next_link'] = '>';
		$config['prev_link'] = '<';   
		$config['full_tag_open'] 	= '<ul class="pagination">';
		$config['full_tag_close'] 	= '</ul>';
		$config['num_tag_open'] 	= '<li class="waves-effect">';
		$config['num_tag_close'] 	= '</li>';
		$config['cur_tag_open'] 	= '<li class="active"><a>';
		$config['cur_tag_close'] 	= '</a></li>';
		$config['next_tag_open'] 	= '<li class="waves-effect">';
		$config['next_tagl_close'] 	= '</li>';
		$config['prev_tag_open'] 	= '<li class="waves-effect">';
		$config['prev_tagl_close'] 	= '</li>';
		$config['first_tag_open'] 	= '<li class="waves-effect">Primeira';
		$config['first_tagl_close'] = '</li>';
		$config['last_tag_open'] 	= '<li class="waves-effect">Última';
		$config['last_tagl_close'] 	= '</li>';
		$config["prev_link"] = "<i class='material-icons'>chevron_left</i>";
		$config["next_link"] = "<i class='material-icons'>chevron_right</i>";
		$config['use_page_numbers'] = TRUE;
		$config['enable_query_strings'] = TRUE;
		$config['page_query_string'] = TRUE;
		$config['query_string_segment'] = "pag";
		// $config['reuse_query_string'] = TRUE;//aproveitar outros GET existentes
		// $config['uri_segment'] = 2;
		$config['num_links'] = 5;

		$config['total_rows'] = $this->eventos->getListaEventos($pesquisa, $cidade)->num_rows();
		$this->pagination->initialize($config);

		$eventos = $this->eventos->getListaEventos($pesquisa, $cidade, $inicio, $maximo)->result();
		$this->load->view("site-eventos", array("eventos"=>$eventos, "paginacao"=>$this->pagination->create_links(), "pesquisa"=>$pesquisa));
	}
}