<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perfil extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->usuarios->logado(true);
	}

	function index(){
		$usuario = $this->usuarios->perfil()->first_row();
		$this->load->model("PlanosModel", "planos");
		$planos = $this->planos->getValores()->result();
		function validar_cpf($cpf){
			$cpf = preg_replace('/[^0-9]/', '', (string) $cpf);
			// Valida tamanho
			if (strlen($cpf) != 11)
				return false;
			// Calcula e confere primeiro dígito verificador
			for ($i = 0, $j = 10, $soma = 0; $i < 9; $i++, $j--)
				$soma += $cpf{$i} * $j;
			$resto = $soma % 11;
			if ($cpf{9} != ($resto < 2 ? 0 : 11 - $resto))
				return false;
			// Calcula e confere segundo dígito verificador
			for ($i = 0, $j = 11, $soma = 0; $i < 10; $i++, $j--)
				$soma += $cpf{$i} * $j;
			$resto = $soma % 11;
			return $cpf{10} == ($resto < 2 ? 0 : 11 - $resto);
		}

		function validar_cnpj($cnpj){
			$cnpj = preg_replace('/[^0-9]/', '', (string) $cnpj);
			// Valida tamanho
			if (strlen($cnpj) != 14)
				return false;
			// Valida primeiro dígito verificador
			for ($i = 0, $j = 5, $soma = 0; $i < 12; $i++)
			{
				$soma += $cnpj{$i} * $j;
				$j = ($j == 2) ? 9 : $j - 1;
			}
			$resto = $soma % 11;
			if ($cnpj{12} != ($resto < 2 ? 0 : 11 - $resto))
				return false;
			// Valida segundo dígito verificador
			for ($i = 0, $j = 6, $soma = 0; $i < 13; $i++)
			{
				$soma += $cnpj{$i} * $j;
				$j = ($j == 2) ? 9 : $j - 1;
			}
			$resto = $soma % 11;
			return $cnpj{13} == ($resto < 2 ? 0 : 11 - $resto);
		}


		$tipo = $this->input->post("tipo_documento");
		$this->form_validation->set_rules("nome", "Nome", "required");
		if($tipo == "cpf"){
			$this->form_validation->set_rules("cpfcnpj", "CPF", "required|validar_cpf", array("validar_cpf"=>"CPF inválido."));
		}else if($tipo == "cnpj"){
			$this->form_validation->set_rules("cpfcnpj", "CNPJ", "required|validar_cnpj", array("validar_cnpj"=>"CNPJ inválido."));
		}
		$this->form_validation->set_rules("id_cidade", "Cidade", "required");
		$this->form_validation->set_rules("email", "E-mail", "required|valid_email");
		$this->form_validation->set_rules("ramo_principal", "Ramo Principal", "required");
		$this->form_validation->set_rules("anuncios_principais", "Anúncios Principais", "nl2br|max_length[100]");

		if($this->form_validation->run()){
			$this->usuarios->atualizar_perfil();
		}

		$this->load->view("interno-perfil", array("usuario"=>$usuario, "planos"=>$planos));
	}

	function atualizar_foto(){
		$this->usuarios->atualizar_foto();
	}
	function atualizar_banner(){
		$this->usuarios->atualizar_banner();
	}

	function alterar_senha(){

		$this->form_validation->set_rules("senha_antiga", "Senha", "required");
		$this->form_validation->set_rules("nova_senha", "Nova Senha", "required");
		$this->form_validation->set_rules("confirmacao_senha", "Confirmação de Senha", "required|matches[nova_senha]");
		if($this->form_validation->run()){
			$this->usuarios->alterar_senha();
		}

		$this->load->view("alterar-senha");
	}
}