<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produtos extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->usuarios->logado(true);
		$this->load->model("AvesModel", "aves");
		$this->load->model("ProdutosModel", "produtos");
	}

	function index(){
		$limites = (object) $this->usuarios->anuncios_usuario();

		if($limites->total_anuncios > $limites->anuncios_permitidos){
			$this->session->set_flashdata("toast", "Materialize.toast('<p>Você possui mais anúncios do que seu plano comporta.</p><p><b>&nbsp;Alguns anúncios estão desativados.</b></p>', 15000, 'blue-grey');");
			$this->load->model("AvesModel", "aves");
			$this->aves->desativar();
		}
		$this->load->library("pagination");
		$pag = (int) $this->uri->segment(2);
		$maximo = 16;
		$inicio = ($pag == null) ? 0 : $pag;
		if($inicio > 0)
			$inicio = $maximo * ($inicio -1);

		$config['base_url'] = "/produtos";
		$config['per_page'] = $maximo;
		$config['first_link'] = '<<';
		$config['last_link'] = '>>';
		$config['next_link'] = '>';
		$config['prev_link'] = '<';   
		$config['full_tag_open'] 	= '<ul class="pagination">';
		$config['full_tag_close'] 	= '</ul>';
		$config['num_tag_open'] 	= '<li class="waves-effect">';
		$config['num_tag_close'] 	= '</li>';
		$config['cur_tag_open'] 	= '<li class="active"><a>';
		$config['cur_tag_close'] 	= '</a></li>';
		$config['next_tag_open'] 	= '<li class="waves-effect">';
		$config['next_tagl_close'] 	= '</li>';
		$config['prev_tag_open'] 	= '<li class="waves-effect">';
		$config['prev_tagl_close'] 	= '</li>';
		$config['first_tag_open'] 	= '<li class="waves-effect">Primeira';
		$config['first_tagl_close'] = '</li>';
		$config['last_tag_open'] 	= '<li class="waves-effect">Última';
		$config['last_tagl_close'] 	= '</li>';
		$config["prev_link"] = "<i class='material-icons'>chevron_left</i>";
		$config["next_link"] = "<i class='material-icons'>chevron_right</i>";
		$config['use_page_numbers'] = TRUE;
		$config['enable_query_strings'] = TRUE;
		$config['page_query_string'] = FALSE;
		$config['uri_segment'] = 2;
		$config['num_links'] = 3;

		$config['total_rows'] = $this->produtos->getProdutos()->num_rows();
		$this->pagination->initialize($config);

		$produtos = $this->produtos->getProdutos(false, $inicio, $maximo);
		$produtos = $produtos->result();
		$this->load->view("produtos", array("produtos"=>$produtos, "paginacao"=>$this->pagination->create_links(), "limites"=>$limites));
	}

	function cadastrar(){
		$limites = (object) $this->usuarios->anuncios_usuario();

		if($limites->anuncios_permitidos - $limites->total_anuncios < 1){
			$this->session->set_flashdata("toast", "Materialize.toast('<p>Seu plano não comporta mais anúncios.</p><p>Você pode alterá-lo &nbsp;<b><a class=\'white-text\' href=\'/solicitacoes/planos\'>aqui</a></b>.</p>', 15000, 'white-text blue-grey darken-5');");
			redirect("/produtos");
			exit();
		}

		

		$this->form_validation->set_rules("img_principal", "Imagem", "max_length[60]");
		$this->form_validation->set_rules("nome", "Nome", "required|max_length[60]");
		$this->form_validation->set_rules("frase", "Frase", "max_length[100]");
		$this->form_validation->set_rules("entrega", "Entrega", "required|max_length[10]");
		$this->form_validation->set_rules("entrega_obs", "Obs. de Entrega", "max_length[200]");
		$this->form_validation->set_rules("aceita_troca", "Aceita Troca", "required|max_length[10]");
		$this->form_validation->set_rules("parcela", "Parcela", "max_length[100]");
		$this->form_validation->set_rules("outras_opcoes", "Outras Opções", "max_length[500]");
		$this->form_validation->set_rules("link_externo", "Link Externo", "max_length[100]");
		$this->form_validation->set_rules("link_comercial", "Link Comercial", "max_length[100]");
		$this->form_validation->set_rules("valor", "Valor", "max_length[10]");
		$this->form_validation->set_rules("valor_mostrar", "Mostrar Valor", "");
		$this->form_validation->set_rules("cod_referencia", "Código de Referência", "max_length[30]");

		$this->form_validation->set_rules("ps_tipo_prodserv", "Tipo", "");
		$this->form_validation->set_rules("p_tipo_prod", "Estado", "max_length[1]");
		$this->form_validation->set_rules("p_quantidade", "Quantidade", "max_length[60]");
		

		if($this->form_validation->run()){
			$this->produtos->cadastrar();
		}else{
			$post = $this->input->post("nome");
			if(!empty($post)){
				$this->session->set_flashdata("toast", "Materialize.toast('Revise as informações.', 10000, 'red');");
			}
		}

		$this->load->view("produtos-cadastrar", array(
			"tipos"=>$this->produtos->getTiposProdutos()->result(),
		));
	}

	function editar(){
		$id = (int) $this->uri->segment(3);
		if(empty($id)){
			$this->session->set_flashdata("toast", "Materialize.toast('Nenhum produto encontrado.', 5000, 'red');");
			redirect("/produtos");
		}
		$produto = $this->produtos->getProduto($id);
		if($produto->num_rows() == 0){
			$this->session->set_flashdata("toast", "Materialize.toast('Nenhum produto encontrado.', 7000, 'red');");
			redirect("/produtos");
		}
		$limites = $this->usuarios->anuncios_usuario();

		$this->form_validation->set_rules("img_principal", "Imagem", "max_length[60]");
		$this->form_validation->set_rules("nome", "Nome", "required|max_length[60]");
		$this->form_validation->set_rules("frase", "Frase", "max_length[100]");
		$this->form_validation->set_rules("entrega", "Entrega", "required|max_length[10]");
		$this->form_validation->set_rules("entrega_obs", "Obs. de Entrega", "max_length[200]");
		$this->form_validation->set_rules("aceita_troca", "Aceita Troca", "required|max_length[10]");
		$this->form_validation->set_rules("parcela", "Parcela", "max_length[100]");
		$this->form_validation->set_rules("outras_opcoes", "Outras Opções", "max_length[500]");
		$this->form_validation->set_rules("link_externo", "Link Externo", "max_length[100]");
		$this->form_validation->set_rules("link_comercial", "Link Comercial", "max_length[100]");
		$this->form_validation->set_rules("valor", "Valor", "max_length[10]");
		$this->form_validation->set_rules("valor_mostrar", "Mostrar Valor", "");
		$this->form_validation->set_rules("cod_referencia", "Código de Referência", "max_length[30]");

		$this->form_validation->set_rules("ps_tipo_prodserv", "Mostrar Valor", "");
		$this->form_validation->set_rules("p_tipo_prod", "Estado", "max_length[1]");
		$this->form_validation->set_rules("p_quantidade", "Quantidade", "max_length[60]");

		if($this->form_validation->run()){
			$this->produtos->editar($id);
		}else{
			$post = $this->input->post("nome");
			if(!empty($post)){
				$this->session->set_flashdata("toast", "Materialize.toast('Revise as informações.', 10000, 'red');");
			}
		}

		$this->load->view("produtos-editar", array(
			"tipos"=>$this->produtos->getTiposProdutos()->result(),
			"produto"=>$produto->first_row(),
		));
	}

	function alterar_situacao(){
		$this->form_validation->set_rules("id", "ID", "required|is_numeric");
		$this->form_validation->set_rules("situacao", "Situação", "required|max_length[1]");

		if($this->form_validation->run()){
			$this->produtos->alterar_situacao();
		}
	}
}