<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Recuperar_senha extends CI_Controller{
	function __construct(){
		parent::__construct();
	}

	function index(){

		$this->form_validation->set_rules("login", "Login", "required");

		if($this->form_validation->run()){
			$this->load->model("UsuariosModel", "usuarios");
			$this->usuarios->recuperarSenha();
		}

		$this->load->view("recuperar-senha");
	}
}