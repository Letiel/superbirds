<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Select extends CI_Controller{
	function __construct(){
		parent::__construct();
	}

	function cidades(){
		$this->load->model("CidadesModel", "cidades");
		$cidades = $this->cidades->getCidadesSelect()->result();
		echo json_encode(array("results"=>$cidades));
	}

	function anuncios_usuario(){//função para retornar quantidade de anúncios que o usuário tem disponível
		echo json_encode($this->usuarios->anuncios_usuario());
	}
}