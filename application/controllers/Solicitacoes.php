<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Solicitacoes extends CI_Controller{
	function __construct(){
		parent::__construct();
		$this->usuarios->logado(true);
		$this->load->model("SolicitacoesModel", "solicitacoes");
	}

	function index(){
		$this->load->library("pagination");
		$pag = (int) $this->uri->segment(2);
		$maximo = 10;
		$inicio = ($pag == null) ? 0 : $pag;
		if($inicio > 0)
			$inicio = $maximo * ($inicio -1);

		$config['base_url'] = "/solicitacoes";
		$config['per_page'] = $maximo;  
		$config['full_tag_open'] 	= '<ul class="pagination">';
		$config['full_tag_close'] 	= '</ul>';
		$config['num_tag_open'] 	= '<li class="waves-effect">';
		$config['num_tag_close'] 	= '</li>';
		$config['cur_tag_open'] 	= '<li class="active"><a>';
		$config['cur_tag_close'] 	= '</a></li>';
		$config['next_tag_open'] 	= '<li class="waves-effect">';
		$config['next_tagl_close'] 	= '</li>';
		$config['prev_tag_open'] 	= '<li class="waves-effect">';
		$config['prev_tagl_close'] 	= '</li>';
		$config['first_tag_open'] 	= '<li class="waves-effect">';
		$config['first_tagl_close'] = '</li>';
		$config['last_tag_open'] 	= '<li class="waves-effect">';
		$config['last_tagl_close'] 	= '</li>';
		$config["prev_link"] = "<i class='material-icons'>chevron_left</i>";
		$config["next_link"] = "<i class='material-icons'>chevron_right</i>";

		$config["first_link"] = "<i class='material-icons'>first_page</i>";
		$config["last_link"] = "<i class='material-icons'>last_page</i>";

		$config['use_page_numbers'] = TRUE;
		$config['enable_query_strings'] = TRUE;
		$config['page_query_string'] = FALSE;
		$config['uri_segment'] = 2;
		$config['num_links'] = 3;

		$config['total_rows'] = $this->solicitacoes->getSolicitacoes()->num_rows();
		$this->pagination->initialize($config);

		$this->load->view("solicitacoes", array("paginacao"=>$this->pagination->create_links(), "solicitacoes"=>$this->solicitacoes->getSolicitacoes($inicio, $maximo)->result()));
	}

	function nova(){
		$this->load->view("solicitacoes-nova");
	}

	function planos(){
		$this->load->model("PlanosModel", "planos");

		$this->form_validation->set_rules("id_tab_valores", "Plano", "required|is_numeric");
		if($this->form_validation->run()){
			$this->solicitacoes->solicitar("PL");
		}

		$this->load->view("solicitacoes-planos", array("planos"=>$this->planos->getValores()->result()));
	}

	function artigos(){
		$this->load->model("ArtigosModel", "artigos");

		$this->form_validation->set_rules("solicitacao", "Descrição", "required|nl2br");
		if($this->form_validation->run()){
			$this->artigos->solicitar();
		}

		$this->load->view("solicitacoes-artigos", array("planos"=>$this->artigos->getTipos()->result()));
	}

	function eventos(){
		$this->load->model("EventosModel", "eventos");

		$this->form_validation->set_rules("solicitacao", "Descrição", "required|nl2br");
		if($this->form_validation->run()){
			$this->eventos->solicitar();
		}

		$this->load->view("solicitacoes-eventos");
	}

	function anuncios_destaque(){
		$this->form_validation->set_rules("id_tab_valores", "Plano", "required|is_numeric");
		if($this->form_validation->run()){
			$this->solicitacoes->solicitar_ad();
		}

		$this->load->view("solicitacoes-anuncios-destaque", array("planos"=>$this->solicitacoes->getTiposAds()->result()));
	}
}