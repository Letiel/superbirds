<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subdominio extends CI_Controller{
	function __construct(){
		parent::__construct();
	}

	function index(){
		$this->load->model("UsuariosModel", "usuarios");
		$sub = $this->uri->segment(2);

		$criador = $this->usuarios->getCriadorSub($sub);

		if($criador->num_rows() > 0){
			$criador = $criador->first_row();

			$this->load->model("AvesModel", "aves");
			$this->load->model("ProdutosModel", "produtos");
			$this->load->model("ServicosModel", "servicos");
			$this->load->model("ArtigosModel", "artigos");
			$this->load->model("EventosModel", "eventos");

			$aves = $this->aves->getAvesCriador($criador->id, 3)->result();
			$produtos = $this->produtos->getProdutosCriador($criador->id, 3)->result();
			$servicos = $this->servicos->getServicosCriador($criador->id, 3)->result();
			$artigos = $this->artigos->getArtigosCriador($criador->id, 3)->result();
			$eventos = $this->eventos->getEventosCriador($criador->id, 3)->result();
			$this->load->view("site-ver-criador", array("criador"=>$criador, "aves"=>$aves, "produtos"=>$produtos, "servicos"=>$servicos, "artigos"=>$artigos, "eventos"=>$eventos));
		}else{
			echo "Página não encontrada";
		}
	}

	function getDados(){ //para pegar os dados como title, description, etc...
		$this->load->model("UsuariosModel", "usuarios");
		$sub = $this->uri->segment(3);

		$criador = $this->usuarios->getCriadorSub2($sub);

		if($criador->num_rows() > 0){
			$criador = $criador->first_row();
			$criador->encontrou = true;
		}else{
			$criador->encontrou = false;
		}
		echo json_encode($criador);			
	}
}