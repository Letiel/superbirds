<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ver extends CI_Controller{
	function __construct(){
		parent::__construct();
	}

	function ave(){
		$this->load->model("AvesModel", "aves");
		$this->load->model("ProdutosModel", "produtos");

		$ave = (integer)$this->uri->segment(3);
		if(empty($ave) || !is_numeric($ave)){
			$this->session->set_flashdata("toast", "Materialize.toast('Ave não encontrada.', 10000, 'yellow');");
			redirect("/");
		}

		$votos = $this->aves->getVotosAve($ave)->first_row();
		$ave = $this->aves->getAveSite($ave);

		if($ave->num_rows() == 0){
			$this->session->set_flashdata("toast", "Materialize.toast('Ave não encontrada.', 10000, 'yellow');");
			redirect("/");
		}

		$ave = $ave->first_row();
		$outras = $this->aves->getAvesCriador($ave->id_usuario, 10)->result();
		$produtos = $this->produtos->getProdutosCriador($ave->id_usuario, 10)->result();

		$medias = $this->aves->getMediaPrecos($ave)->first_row();

		$this->load->view("site-ver-ave", array("ave"=>$ave, "outras"=>$outras, "produtos"=>$produtos, "votos"=>$votos, "medias"=>$medias));
		$this->load->model("CurtidasModel", "curtidas");
	}

	function produto(){
		$this->load->model("AvesModel", "aves");
		$this->load->model("ProdutosModel", "produtos");

		$produto = (integer)$this->uri->segment(3);
		if(empty($produto) || !is_numeric($produto)){
			$this->session->set_flashdata("toast", "Materialize.toast('Produto não encontrado.', 10000, 'yellow');");
			redirect("/");
		}

		$produto = $this->produtos->getProdutoSite($produto);

		if($produto->num_rows() == 0){
			$this->session->set_flashdata("toast", "Materialize.toast('Produto não encontrado.', 10000, 'yellow');");
			redirect("/");
		}

		$produto = $produto->first_row();
		$aves = $this->aves->getAvesCriador($produto->id_usuario, 10)->result();
		$produtos = $this->produtos->getProdutosCriador($produto->id_usuario, 10)->result();

		$this->load->view("site-ver-produto", array("produto"=>$produto, "aves"=>$aves, "produtos"=>$produtos));
	}

	function servico(){
		$this->load->model("AvesModel", "aves");
		$this->load->model("ProdutosModel", "produtos");
		$this->load->model("ServicosModel", "servicos");

		$servico = (integer)$this->uri->segment(3);
		if(empty($servico) || !is_numeric($servico)){
			$this->session->set_flashdata("toast", "Materialize.toast('Serviço não encontrado.', 10000, 'yellow');");
			redirect("/");
		}

		$servico = $this->servicos->getServicoSite($servico);

		if($servico->num_rows() == 0){
			$this->session->set_flashdata("toast", "Materialize.toast('Serviço não encontrado.', 10000, 'yellow');");
			redirect("/");
		}

		$servico = $servico->first_row();
		$aves = $this->aves->getAvesCriador($servico->id_usuario, 10)->result();
		$produtos = $this->produtos->getProdutosCriador($servico->id_usuario, 10)->result();

		$this->load->view("site-ver-servico", array("servico"=>$servico, "aves"=>$aves, "produtos"=>$produtos));
	}

	function artigo(){
		$this->load->model("AvesModel", "aves");
		$this->load->model("ProdutosModel", "produtos");
		$this->load->model("ServicosModel", "servicos");
		$this->load->model("ArtigosModel", "artigos");

		$artigo = (integer)$this->uri->segment(3);
		if(empty($artigo) || !is_numeric($artigo)){
			$this->session->set_flashdata("toast", "Materialize.toast('Artigo não encontrado.', 10000, 'yellow');");
			redirect("/");
		}

		$artigo = $this->artigos->getArtigoSite($artigo);

		if($artigo->num_rows() == 0){
			$this->session->set_flashdata("toast", "Materialize.toast('Artigo não encontrado.', 10000, 'yellow');");
			redirect("/");
		}

		$artigo = $artigo->first_row();
		$aves = $this->aves->getAvesCriador($artigo->id_usuario, 10)->result();
		$produtos = $this->produtos->getProdutosCriador($artigo->id_usuario, 10)->result();

		$this->load->view("site-ver-artigo", array("artigo"=>$artigo, "aves"=>$aves, "produtos"=>$produtos));
	}

	function evento(){
		$this->load->model("AvesModel", "aves");
		$this->load->model("ProdutosModel", "produtos");
		$this->load->model("ServicosModel", "servicos");
		$this->load->model("ArtigosModel", "artigos");
		$this->load->model("EventosModel", "eventos");

		$evento = (integer)$this->uri->segment(3);
		if(empty($evento) || !is_numeric($evento)){
			$this->session->set_flashdata("toast", "Materialize.toast('Serviço não encontrado.', 10000, 'yellow');");
			redirect("/");
		}

		$evento = $this->eventos->getEventoSite($evento);

		if($evento->num_rows() == 0){
			$this->session->set_flashdata("toast", "Materialize.toast('Evento não encontrado.', 10000, 'yellow');");
			redirect("/");
		}

		$evento = $evento->first_row();
		$aves = $this->aves->getAvesCriador($evento->id_usuario, 10)->result();
		$produtos = $this->produtos->getProdutosCriador($evento->id_usuario, 10)->result();

		$this->load->view("site-ver-evento", array("evento"=>$evento, "aves"=>$aves, "produtos"=>$produtos));
	}

	function visitar(){
		$post = $this->input->post();
		if(!empty($post["id"]) && is_numeric($post["id"])){
			switch ($post["tipo"]) {
				case 'ave':
					$this->load->model("AvesModel", "aves");
					$this->aves->visitar($post["id"]);
					break;
				case 'produto':
					$this->load->model("ProdutosModel", "produtos");
					$this->produtos->visitar($post["id"]);
					break;
				case 'servico':
					$this->load->model("ServicosModel", "servicos");
					$this->servicos->visitar($post["id"]);
					break;
				case 'artigo':
					$this->load->model("ArtigosModel", "artigos");
					$this->artigos->visitar($post["id"]);
					break;
				case 'evento':
					$this->load->model("EventosModel", "eventos");
					$this->eventos->visitar($post["id"]);
					break;
				case 'usuario':
					$this->load->model("UsuariosModel", "usuarios");
					$this->usuarios->visitar($post["id"]);
					break;
			}
		}
	}
}