<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ArtigosModel extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	function getTipos(){
		$this->db->start_cache();
		$this->db->where("tab_valores.tipo_conta", "AR");
		$this->db->where("tab_valores.situacao", "L");
		$planos = $this->db->get("tab_valores");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $planos;
	}

	function getArtigos($inicio = null, $maximo = null){
		$this->db->start_cache();
		$this->db->limit($maximo, $inicio);
		$this->db->where("artigos.situacao = 'L' OR artigos.situacao = 'S'");
		$this->db->where("artigos.id_usuario_destino", $this->session->userdata("id"));

		$this->db->where("tipos_gerais.tipo", "tipos_artigos");
		$this->db->select("artigos.*, COUNT(curtidas.id) as curtidas");

		$this->db->join("tipos_gerais", "tipos_gerais.id = artigos.tipo_artigo");

		$this->db->join("curtidas", "(curtidas.id_elemento = artigos.id AND curtidas.tipo = 'artigos')", "left");
		$this->db->where("(curtidas.tipo = 'artigos' OR curtidas.tipo IS NULL)");
		$this->db->group_by("artigos.id");
		$this->db->order_by("artigos.id", "DESC");
		$this->db->select("artigos.*, tipos_gerais.nome as tipo");
		$artigos = $this->db->get("artigos");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $artigos;
	}

	function solicitar(){
		$post = $this->input->post();

		$post["id_usuario_solicitante"] = $this->session->userdata("id");
		$post["situacao"] = "S";
		$post["id_tab_valores"] = 0;
		$post["tipo_solicitacao"] = "AR";

		$this->db->start_cache();
		$insert = $this->db->insert("solicitacoes", $post);
		$this->db->stop_cache();
		$this->db->flush_cache();
		if($insert){
			$this->session->set_flashdata("toast", "Materialize.toast('Solicitado com sucesso.', 5000, 'blue');Materialize.toast('Aguarde retorno em seu e-mail.', 10000, 'blue');");
			redirect("/solicitacoes");
		}else{
			$this->session->set_flashdata("toast", "Materialize.toast('Erro ao realizar solicitação.', 5000, 'red');");
		}
	}

	// -------------------------- SITE -------------------------

	function getListaArtigos($pesquisa = null, $cidade = null, $inicio = null, $maximo = null){
		$this->db->start_cache();
		$this->db->limit($maximo, $inicio);
		if(!empty($pesquisa)){
			$this->db->where("(usuarios.nome LIKE '%$pesquisa%' OR cidades.nome LIKE '%$pesquisa%' OR estados.nome LIKE '%$pesquisa%' OR estados.uf LIKE '%$pesquisa%' OR artigos.nome LIKE '%$pesquisa%')");
		}

		if(!empty($cidade)){
			$this->db->where("cidades.id", $cidade);
		}

		$this->db->where("(usuarios.situacao = 'L' OR usuarios.situacao IS NULL)");
		$this->db->where("(usuarios.denunciado = 'N' OR usuarios.denunciado = 'L' OR usuarios.denunciado = 'D' OR usuarios.denunciado IS NULL)");

		$this->db->where("artigos.mostra_superbirds", true);
		$this->db->where("artigos.situacao", "L");
		$this->db->where("(artigos.denunciado = 'N' OR artigos.denunciado = 'L' OR artigos.denunciado = 'D')");
		
		$this->db->join("usuarios", "usuarios.id = artigos.id_usuario_destino", "LEFT");
		$this->db->join("cidades", "usuarios.id_cidade = cidades.id", "LEFT");
		$this->db->join("estados", "cidades.uf = estados.uf", "LEFT");

		$this->db->order_by("artigos.data_criacao DESC");

		$this->db->select("artigos.*, COUNT(curtidas.id) as curtidas");

		$this->db->join("curtidas", "(curtidas.id_elemento = artigos.id AND curtidas.tipo = 'artigos')", "left");
		$this->db->where("(curtidas.tipo = 'artigos' OR curtidas.tipo IS NULL)");
		$this->db->group_by("artigos.id");
		
		$aves = $this->db->get("artigos");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $aves;
	}
	
	function getArtigosCriador($criador, $maximo = null, $inicio = null){
		$this->db->start_cache();
		$this->db->limit($maximo, $inicio);
		$this->db->where("artigos.id_usuario_destino", $criador);

		$this->db->where("(usuarios.situacao = 'L' OR usuarios.situacao IS NULL)");
		$this->db->where("(usuarios.denunciado = 'N' OR usuarios.denunciado = 'L' OR usuarios.denunciado = 'D' OR usuarios.denunciado IS NULL)");

		// $this->db->where("artigos.mostra_superbirds", true);
		$this->db->where("artigos.situacao", "L");
		$this->db->where("(artigos.denunciado = 'N' OR artigos.denunciado = 'L' OR artigos.denunciado = 'D')");
		
		$this->db->join("usuarios", "usuarios.id = artigos.id_usuario_destino", "LEFT");
		$this->db->join("cidades", "usuarios.id_cidade = cidades.id", "LEFT");
		$this->db->join("estados", "cidades.uf = estados.uf", "LEFT");

		$this->db->order_by("artigos.data_criacao DESC");

		$this->db->select("artigos.*, COUNT(curtidas.id) as curtidas");

		$this->db->join("curtidas", "(curtidas.id_elemento = artigos.id AND curtidas.tipo = 'artigos')", "left");
		$this->db->where("(curtidas.tipo = 'artigos' OR curtidas.tipo IS NULL)");
		$this->db->group_by("artigos.id");
		
		$aves = $this->db->get("artigos");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $aves;
	}

	function getArtigoSite($id, $mostra_superbirds = true){
		$this->db->flush_cache();
		$this->db->start_cache();
		$this->db->where("artigos.mostra_superbirds", $mostra_superbirds);
		$this->db->where("artigos.situacao", "L");
		$this->db->where("(artigos.id_usuario_destino = $id OR artigos.id_usuario_destino = '' OR artigos.id_usuario_destino IS NULL)");
		$this->db->where("(artigos.denunciado = 'N' OR artigos.denunciado = 'L' OR artigos.denunciado = 'D')");

		$this->db->join("usuarios", "usuarios.id = artigos.id_usuario_destino", "left");
		$this->db->join("tipos_gerais", "tipos_gerais.id = artigos.tipo_artigo", "left");
		$this->db->select("artigos.*, usuarios.nome as nome_usuario, usuarios.id as id_usuario, tipos_gerais.nome as tipo, COUNT(curtidas.id) as curtidas");

		$this->db->join("curtidas", "(curtidas.id_elemento = artigos.id AND curtidas.tipo = 'artigos')", "left");
		$this->db->where("(curtidas.tipo = 'artigos' OR curtidas.tipo IS NULL)");
		$this->db->group_by("artigos.id");
		$artigos = $this->db->get("artigos");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $artigos;
	}

	function alterar_situacao(){
		$post = $this->input->post();
		if($post["situacao"] == "C" || $post["situacao"] == "V" || $post["situacao"] == "L" || $post["situacao"] == "S" || $post["situacao"] == "E"){
			$limites = (object) $this->usuarios->anuncios_usuario();
			if($limites->total_artigos >= $limites->artigos_permitidos && $post["situacao"] == "L"){
				$this->session->set_flashdata("toast", "Materialize.toast('Limite de Artigos Ativos Atingido!', 10000, 'red');");
				exit();
			}else{
				$this->db->flush_cache();
				$this->db->start_cache();
				$this->db->where("artigos.id", $post["id"]);
				$this->db->where("artigos.id_usuario_destino", $this->session->userdata("id"));
				$this->db->select("artigos.situacao");
				$artigo = $this->db->get("artigos")->first_row();
				$this->db->stop_cache();
				$this->db->flush_cache();

				if($artigo->situacao == "V" || $artigo->situacao == "C" || $artigo->situacao == "E"){
					$this->session->set_flashdata("toast", "Materialize.toast('Não é possível alterar o estado deste artigo!', 10000, 'red');");
				}else{
					$this->db->start_cache();
					$this->db->where("artigos.id", $post["id"]);
					$this->db->where("artigos.id_usuario_destino", $this->session->userdata("id"));
					$this->db->update("artigos", array("situacao"=>$post["situacao"]));
					$this->db->stop_cache();
					$this->db->flush_cache();
					if($post["situacao"] == "E")
						$this->session->set_flashdata("toast", "Materialize.toast('Artigo Removido com Sucesso!', 5000, 'blue');");
					else
						$this->session->set_flashdata("toast", "Materialize.toast('Artigo Atualizado com Sucesso!', 5000, 'blue');");
				}

			}
		}
	}

	function visitar($id){
		$visitas = $this->session->userdata("visitas_artigos");
		if(empty($visitas[$id])){
			$this->db->flush_cache();
			$this->db->start_cache();
			$update = $this->db->query("UPDATE artigos SET contador = contador+1 WHERE artigos.id = $id");
			$this->db->stop_cache();
			$this->db->flush_cache();
			if($update){
				$visitas[$id] = true;
				$this->session->set_userdata("visitas_artigos", $visitas);
			}
		}
	}
}