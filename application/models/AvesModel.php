<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

class AvesModel extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	function getAves($destaque = false, $inicio = null, $maximo = null){
		$this->db->start_cache();
		if($destaque)
			$this->db->where("anuncios.destaque", 1);
		$this->db->limit($maximo, $inicio);
		$this->db->where("anuncios.tipo_anuncio", "A");
		$this->db->where("anuncios.id_usuario", $this->session->userdata("id"));
		$this->db->join("aves_classes", "aves_classes.id = anuncios.id_aves_classes", "left");
		$this->db->join("aves_cores", "aves_cores.id = anuncios.id_aves_cores", "left");
		$this->db->select("anuncios.id, anuncios.nome, anuncios.destaque, anuncios.img_principal, aves_classes.nome as classe, aves_cores.cor, aves_cores.subcor, anuncios.situacao, anuncios.curtidas, anuncios.contador, COUNT(curtidas.id) as curtidas");
		$this->db->join("curtidas", "(curtidas.id_elemento = anuncios.id AND curtidas.tipo = 'anuncios')", "left");
		$this->db->where("(curtidas.tipo = 'anuncios' OR curtidas.tipo IS NULL)");
		$this->db->group_by("anuncios.id");
		$this->db->order_by("anuncios.id", "DESC");
		$anuncios = $this->db->get("anuncios");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $anuncios;
	}

	function getAve($id){
		$this->db->start_cache();
		$this->db->where("anuncios.id", $id);
		$this->db->where("anuncios.tipo_anuncio", "A");
		$this->db->where("anuncios.id_usuario", $this->session->userdata("id"));
		$this->db->join("aves_classes", "aves_classes.id = anuncios.id_aves_classes", "left");
		$this->db->join("aves_cores", "aves_cores.id = anuncios.id_aves_cores", "left");

		$this->db->join("curtidas", "(curtidas.id_elemento = anuncios.id AND curtidas.tipo = 'anuncios')", "left");
		$this->db->where("(curtidas.tipo = 'anuncios' OR curtidas.tipo IS NULL)");
		$this->db->group_by("anuncios.id");

		$this->db->select("anuncios.*, aves_classes.nome as classe, aves_cores.cor, aves_cores.subcor, COUNT(curtidas.id) as curtidas");
		$anuncios = $this->db->get("anuncios");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $anuncios;
	}

	function getClasses(){
		$this->db->start_cache();
		$this->db->order_by("nome", "asc");
		$this->db->select("id, nome");
		$classes = $this->db->get('aves_classes');
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $classes;
	}

	function getMutacoes(){
		$this->db->start_cache();
		$this->db->order_by("nome", "asc");
		$this->db->select("id, nome");
		$mutacoes = $this->db->get('aves_mutacoes');
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $mutacoes;
	}

	function getCores(){
		$this->db->start_cache();
		$this->db->order_by("nome", "asc");
		$this->db->select("id, nome, cor, subcor");
		$cores = $this->db->get('aves_cores');
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $cores;
	}

	function getUnidades(){
		$this->db->start_cache();
		$this->db->where("tipos_gerais.tipo", "unidade");
		$this->db->order_by("nome", "asc");
		$this->db->select("id, nome");
		$unidades = $this->db->get('tipos_gerais');
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $unidades;
	}

	function cadastrar(){
		$post = $this->input->post();
		$post["id_usuario"] = $this->session->userdata("id");
		$post["tipo_anuncio"] = "A";
		$post["destaque"] = false;
		$post["a_responsabilidade_info"] = date("d/m/Y H:i:s")." - ".$_SERVER['REMOTE_ADDR'];
		$post["situacao"] = "L";
		$post["denunciado"] = "N";
		$post["contador"] = 0;
		$post["curtidas"] = 0;

		$post["a_avenda"] = $post["a_avenda"] == "a_avenda" ? true : false;
		$post["valor_mostrar"] = $post["valor_mostrar"] == "valor_mostrar" ? true : false;
		$post["a_confirmaresponsabilidade"] = $post["a_confirmaresponsabilidade"] == "a_confirmaresponsabilidade" ? true : false;

		if($this->db->insert("anuncios", $post)){
			$this->session->set_flashdata("toast", "Materialize.toast('Cadastrado com Sucesso!', 10000, 'blue');");
		}else{
			$this->session->set_flashdata("toast", "Materialize.toast('Ocorreu um erro interno ao cadastrar.', 10000, 'red');");
		}
			redirect("/aves");
	}

	function editar($id){
		$post = $this->input->post();
		$post["id_usuario"] = $this->session->userdata("id");
		$post["a_responsabilidade_info"] = date("d/m/Y H:i:s")." - ".$_SERVER['REMOTE_ADDR'];

		$post["a_avenda"] = $post["a_avenda"] == "a_avenda" ? true : false;
		$post["valor_mostrar"] = $post["valor_mostrar"] == "valor_mostrar" ? true : false;
		$post["a_confirmaresponsabilidade"] = $post["a_confirmaresponsabilidade"] == "a_confirmaresponsabilidade" ? true : false;
		$post["data_modificacao"] = date("Y-m-d H:i:s");

		$this->db->flush_cache();
		$this->db->start_cache();
		$this->db->where("anuncios.id", $id);
		$this->db->where("anuncios.tipo_anuncio", "A");
		$anuncio = $this->db->get("anuncios")->first_row();
		$this->db->stop_cache();
		$this->db->flush_cache();

		if(empty($post["img_1"])){
			if(!empty($anuncio->img_1))
				unlink("img/".$anuncio->img_1);
		}
		if(empty($post["img_2"])){
			if(!empty($anuncio->img_2))
				unlink("img/".$anuncio->img_2);
		}
		if(empty($post["img_3"])){
			if(!empty($anuncio->img_3))
				unlink("img/".$anuncio->img_3);
		}
		if(empty($post["img_4"])){
			if(!empty($anuncio->img_4))
				unlink("img/".$anuncio->img_4);
		}
		if(empty($post["img_5"])){
			if(!empty($anuncio->img_5))
				unlink("img/".$anuncio->img_5);
		}

		$this->db->start_cache();
		$this->db->where("anuncios.id", $id);
		$this->db->where("anuncios.tipo_anuncio", "A");
		$update = $this->db->update("anuncios", $post);
		$this->db->stop_cache();
		$this->db->flush_cache();
		if($update){
			$this->session->set_flashdata("toast", "Materialize.toast('Alterado com Sucesso!', 10000, 'blue');");
		}else{
			$this->session->set_flashdata("toast", "Materialize.toast('Ocorreu um erro interno ao alterar.', 10000, 'red');");
		}
		redirect("/aves");
	}

	function desativar($id = null){
		$limites = (object) $this->usuarios->anuncios_usuario();
		if($limites->total_anuncios > $limites->anuncios_permitidos){//existem anúncios a mais do que permitido
			$total_desativar = $limites->total_anuncios - $limites->anuncios_permitidos;
			$this->db->start_cache();
			if($id == null)
				$this->db->where("anuncios.id_usuario", $this->session->userdata("id"));
			else{
				$this->db->where("anuncios.id_usuario", $id);
			}
			$this->db->where("anuncios.situacao = \"L\"");
			$this->db->order_by("anuncios.id", "ASC");
			$this->db->limit($total_desativar);
			$this->db->select("id");
			$anuncios = $this->db->get("anuncios")->result();
			$this->db->stop_cache();
			$this->db->flush_cache();

			$update = array("situacao"=>"S");
			foreach($anuncios as $anuncio){
				$this->db->start_cache();
				$this->db->where("anuncios.id", $anuncio->id);
				$this->db->update("anuncios", $update);
				$this->db->stop_cache();
				$this->db->flush_cache();
			}
		}

		if($limites->total_anuncios_destaque > $limites->anuncios_destaque_permitidos){
			$total_desativar = $limites->total_anuncios_destaque - $limites->anuncios_destaque_permitidos;

			$this->db->flush_cache();
			$this->db->start_cache();
			$this->db->where("anuncios.destaque", "1");
			$this->db->limit($total_desativar);
			$this->db->order_by("anuncios.data_criacao", "DESC");
			$this->db->select("anuncios.id");
			$anuncios = $this->db->get("anuncios")->result();
			$this->db->stop_cache();
			$this->db->flush_cache();

			foreach($anuncios as $anuncio){
				$this->db->flush_cache();
				$this->db->start_cache();
				$this->db->where("anuncios.id", $anuncio->id);
				$this->db->update("anuncios", array("destaque"=>"0"));
				$this->db->stop_cache();
				$this->db->flush_cache();
			}
		}
	}

	function alterar_situacao(){
		$post = $this->input->post();
		if($post["situacao"] == "C" || $post["situacao"] == "V" || $post["situacao"] == "L" || $post["situacao"] == "S"){
			$limites = (object) $this->usuarios->anuncios_usuario();
			if($limites->total_anuncios >= $limites->anuncios_permitidos && $post["situacao"] == "L"){
				$this->session->set_flashdata("toast", "Materialize.toast('Limite de Anúncios Ativos Atingido!', 10000, 'red');");
				exit();
			}else{

				$this->db->flush_cache();
				$this->db->start_cache();
				$this->db->where("anuncios.id", $post["id"]);
				$this->db->where("anuncios.id_usuario", $this->session->userdata("id"));
				$this->db->select("anuncios.situacao");
				$anuncio = $this->db->get("anuncios")->first_row();
				$this->db->stop_cache();
				$this->db->flush_cache();

				if($anuncio->situacao == "V" || $anuncio->situacao == "C"){
					$this->session->set_flashdata("toast", "Materialize.toast('Não é ṕossível alterar o estado deste anúncio!', 10000, 'red');");
				}else{
					$this->db->start_cache();
					$this->db->where("anuncios.id", $post["id"]);
					$this->db->where("anuncios.id_usuario", $this->session->userdata("id"));
					$this->db->where("anuncios.tipo_anuncio", "A");
					$this->db->update("anuncios", array("situacao"=>$post["situacao"]));
					$this->db->stop_cache();
					$this->db->flush_cache();
					$this->session->set_flashdata("toast", "Materialize.toast('Anúncio Atualizado com Sucesso!', 5000, 'blue');");
				}

			}
		}
	}

	function getListaAves($pesquisa = null, $cidade = null, $inicio = null, $maximo = null){
		$this->db->start_cache();
		$this->db->limit($maximo, $inicio);
		if(!empty($pesquisa)){
			$this->db->where("(usuarios.nome LIKE '%$pesquisa%' OR cidades.nome LIKE '%$pesquisa%' OR estados.nome LIKE '%$pesquisa%' OR estados.uf LIKE '%$pesquisa%' OR anuncios.nome LIKE '%$pesquisa%' OR aves_classes.nome LIKE '%$pesquisa%' OR aves_classes.nome_cientifico LIKE '%$pesquisa%' OR aves_classes.sinonimos LIKE '%$pesquisa%' OR aves_cores.nome LIKE '%$pesquisa%' OR aves_cores.cor LIKE '%$pesquisa%' OR aves_cores.subcor LIKE '%$pesquisa%' OR aves_mutacoes.nome LIKE '%$pesquisa%')");
		}

		if(!empty($cidade)){
			$this->db->where("cidades.id", $cidade);
		}

		$classe = $this->input->get("classe");

		if(!empty($classe)){
			$this->db->where("anuncios.id_aves_classes", $classe);
		}
		
		$mutacao = $this->input->get("mutacao");
		if(!empty($mutacao)){
			$this->db->where("anuncios.id_aves_mutacoes", $mutacao);
		}
		$cor = $this->input->get("cor");
		if(!empty($cor)){
			$this->db->where("anuncios.id_aves_cores", $cor);
		}
		$unidade = $this->input->get("unidade");
		if(!empty($unidade)){
			$this->db->where("anuncios.id_tipo_unidade", $unidade);
		}

		$this->db->where("usuarios.situacao", "L");
		$this->db->where("(usuarios.denunciado = 'N' OR usuarios.denunciado = 'L' OR usuarios.denunciado = 'D')");

		$this->db->where("anuncios.tipo_anuncio", "A");
		$this->db->where("anuncios.situacao", "L");
		$this->db->where("(anuncios.denunciado = 'N' OR anuncios.denunciado = 'L' OR anuncios.denunciado = 'D')");
		$this->db->where("anuncios.a_confirmaresponsabilidade", "1");
		$this->db->where("tipos_gerais.tipo", "unidade");

		$this->db->join("aves_classes", ", aves_classes.id = anuncios.id_aves_classes", "LEFT");
		$this->db->join("aves_mutacoes", "aves_mutacoes.id = anuncios.id_aves_mutacoes", "LEFT");
		$this->db->join("aves_cores", "aves_cores.id = anuncios.id_aves_cores", "LEFT");
		$this->db->join("tipos_gerais", "tipos_gerais.id = anuncios.id_tipo_unidade", "LEFT");

		$this->db->join("usuarios", "usuarios.id = anuncios.id_usuario");
		$this->db->join("cidades", "usuarios.id_cidade = cidades.id");
		$this->db->join("estados", "cidades.uf = estados.uf");

		$this->db->order_by("anuncios.destaque DESC, data_criacao DESC");

		$this->db->join("curtidas", "(curtidas.id_elemento = anuncios.id AND curtidas.tipo = 'anuncios')", "left");
		$this->db->where("(curtidas.tipo = 'anuncios' OR curtidas.tipo IS NULL)");
		$this->db->group_by("anuncios.id");

		$this->db->select("anuncios.*, aves_classes.nome as classe, aves_cores.cor, aves_cores.subcor, aves_mutacoes.nome as mutacao, tipos_gerais.nome as grupo, usuarios.nome as criador, usuarios.id as id_criador, COUNT(curtidas.id) as curtidas");
		
		$aves = $this->db->get("anuncios");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $aves;
	}

	function getAvesCriador($id, $maximo = null, $inicio = null){
		$this->db->start_cache();
		$this->db->limit($maximo, $inicio);
		$this->db->where("anuncios.tipo_anuncio", "A");
		$this->db->where("anuncios.id_usuario", $id);
		$this->db->where("anuncios.situacao", "L");
		$this->db->where("(usuarios.denunciado = 'N' || usuarios.denunciado = 'L' || usuarios.denunciado = 'D')");
		$this->db->where("anuncios.a_confirmaresponsabilidade", "1");

		$this->db->order_by("anuncios.destaque", "DESC");
		$this->db->order_by("anuncios.data_criacao", "DESC");
		$this->db->order_by("anuncios.data_modificacao", "DESC");
		$this->db->where("(anuncios.denunciado = 'L' OR anuncios.denunciado = 'N' OR anuncios.denunciado = 'D')");
		$this->db->join("usuarios", "usuarios.id = anuncios.id_usuario", 'LEFT');
		$this->db->join("aves_classes", "aves_classes.id = anuncios.id_aves_classes", 'LEFT');
		$this->db->join("aves_cores", "aves_cores.id = anuncios.id_aves_cores", 'LEFT');
		$this->db->join("aves_mutacoes", "aves_mutacoes.id = anuncios.id_aves_mutacoes", 'LEFT');
		$this->db->join("tipos_gerais", "tipos_gerais.id = aves_classes.id_aves_grupos", 'LEFT');

		$this->db->join("curtidas", "(curtidas.id_elemento = anuncios.id AND curtidas.tipo = 'anuncios')", "left");
		$this->db->where("(curtidas.tipo = 'anuncios' OR curtidas.tipo IS NULL)");
		$this->db->group_by("anuncios.id");
		$this->db->select("anuncios.*, aves_classes.nome as classe, aves_cores.cor, aves_cores.subcor, aves_mutacoes.nome as mutacao, tipos_gerais.nome as grupo, usuarios.nome as criador, usuarios.id as id_criador, COUNT(curtidas.id) as curtidas");
		$anuncios = $this->db->get("anuncios");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $anuncios;
	}

	function getAveSite($id){
		$this->db->start_cache();
		$this->db->where("anuncios.id", $id);
		$this->db->where("anuncios.tipo_anuncio", "A");
		$this->db->where("anuncios.situacao", "L");
		$this->db->where("anuncios.a_confirmaresponsabilidade", "1");
		$this->db->where("(usuarios.denunciado = 'N' || usuarios.denunciado = 'L' || usuarios.denunciado = 'D')");

		$this->db->join("aves_classes", "aves_classes.id = anuncios.id_aves_classes", "left");
		$this->db->join("aves_cores", "aves_cores.id = anuncios.id_aves_cores", "left");
		$this->db->join("usuarios", "usuarios.id = anuncios.id_usuario");
		$this->db->join("tipos_gerais", "tipos_gerais.id = anuncios.id_tipo_unidade");
		$this->db->join("cidades", "cidades.id = usuarios.id_cidade");

		$this->db->join("curtidas", "(curtidas.id_elemento = anuncios.id AND curtidas.tipo = 'anuncios')", "left");
		$this->db->where("(curtidas.tipo = 'anuncios' OR curtidas.tipo IS NULL)");
		$this->db->group_by("anuncios.id");

		$this->db->select("anuncios.*, aves_classes.nome as classe, aves_cores.cor, aves_cores.subcor, usuarios.nome as nome_usuario, cidades.nome as nome_cidade, cidades.uf, tipos_gerais.nome as tipo_unidade, COUNT(curtidas.id) as curtidas");
		$anuncios = $this->db->get("anuncios");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $anuncios;
	}

	function getVotosAve($ave){
		$this->db->flush_cache();
		$this->db->start_cache();

		$this->db->where("id_anuncio", $ave);

		$this->db->group_by("id_anuncio");

		$this->db->select("AVG(nota) as media, COUNT(nota) as total");

		$total = $this->db->get("avaliacoes_aves");

		$this->db->stop_cache();
		$this->db->flush_cache();

		return $total;
	}

	function visitar($id){
		$visitas = $this->session->userdata("visitas_aves");
		if(empty($visitas[$id])){
			$this->db->flush_cache();
			$this->db->start_cache();
			$update = $this->db->query("UPDATE anuncios SET contador = contador+1 WHERE anuncios.id = $id");
			$this->db->stop_cache();
			$this->db->flush_cache();
			if($update){
				$visitas[$id] = true;
				$this->session->set_userdata("visitas_aves", $visitas);
			}
		}
	}

	function destacar(){
		$id = $this->uri->segment(3);
		if(!empty($id) && is_numeric($id)){
			$this->load->model("UsuariosModel", "usuarios");
			$limites = (object)$this->usuarios->anuncios_usuario();

			$this->db->flush_cache();
			$this->db->start_cache();
			$this->db->where("anuncios.id", $id);
			$this->db->select("anuncios.destaque");
			$anuncio = $this->db->get("anuncios")->first_row();
			$this->db->stop_cache();
			$this->db->flush_cache();

			if($anuncio->destaque == "1"){
				$this->db->flush_cache();
				$this->db->start_cache();
				$this->db->where("id", $id);
				$this->db->update("anuncios", array("destaque"=>"0"));
				$this->db->stop_cache();
				$this->db->flush_cache();
			}else{
				if($limites->total_anuncios_destaque < $limites->anuncios_destaque_permitidos){
					$this->db->flush_cache();
					$this->db->start_cache();
					$this->db->where("id", $id);
					$this->db->update("anuncios", array("destaque"=>"1"));
					$this->db->stop_cache();
					$this->db->flush_cache();

					$this->session->set_flashdata("toast", "Materialize.toast('Anúncio destacado com sucesso', 5000, 'blue');");
				}else{
					$this->session->set_flashdata("toast", "Materialize.toast('Você não possui destaques disponíveis.', 5000, 'red');");			
				}
			}
		}
		redirect("/aves");
	}

	function getMediaPrecos($ave){
		$this->db->flush_cache();
		$this->db->start_cache();
		$this->db->where("anuncios.id_aves_classes", $ave->id_aves_classes);
		$this->db->where("anuncios.id_aves_mutacoes", $ave->id_aves_mutacoes);
		$this->db->where("anuncios.id_aves_cores", $ave->id_aves_cores);
		$this->db->where("anuncios.id_tipo_unidade", $ave->id_tipo_unidade);
		
		$this->db->where("anuncios.a_avenda", "1");

		$this->db->where("anuncios.valor != '1'");

		$this->db->where("anuncios.tipo_anuncio", "A");
		$this->db->where("anuncios.situacao", "L");
		$this->db->where("anuncios.a_confirmaresponsabilidade", "1");
		$this->db->where("(usuarios.denunciado = 'N' || usuarios.denunciado = 'L' || usuarios.denunciado = 'D')");

		$this->db->join("usuarios", "usuarios.id = anuncios.id_usuario");

		$this->db->select("AVG(valor) as media, MIN(valor) as min, MAX(valor) as max, COUNT(anuncios.id) as quantidade");
		$medias = $this->db->get("anuncios");
		$this->db->stop_cache();
		$this->db->flush_cache();
		// echo $this->db->last_query();
		// exit;
		return $medias;
	}
}