<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CidadesModel extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	function getCidades(){
		$this->db->start_cache();
		$cidades = $this->db->get("cidades");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $cidades;
	}

	function getCidade($id){
		$this->db->start_cache();
		$this->db->where("cidades.id", $id);
		$cidades = $this->db->get("cidades");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $cidades;
	}

	function getCidadesSelect(){
		$this->db->start_cache();
		$post = $this->input->get();
		$this->db->like("cidades.nome", $post["term"]);
		$this->db->or_like("cidades.uf", $post["term"]);
		$this->db->select("CONCAT(cidades.nome, ' - ', cidades.uf) as text, cidades.id");
		$cidades = $this->db->get("cidades");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $cidades;
	}
}