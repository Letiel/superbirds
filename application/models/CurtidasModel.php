<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CurtidasModel extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	function getCurtidas($id, $tipo = "anuncios"){
		$ip = $_SERVER["REMOTE_ADDR"];
		$this->db->flush_cache();
		$this->db->start_cache();
		$this->db->where("curtidas.id_elemento", $id);
		$this->db->where("curtidas.ip", $ip);
		$this->db->where("curtidas.tipo", $tipo);
		$curtidas = $this->db->get("curtidas");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $curtidas;
	}

	function getAnuncio($id){
		$this->db->flush_cache();
		$this->db->start_cache();
		$this->db->where("anuncios.id", $id);
		$this->db->where("anuncios.situacao", "L");
		$this->db->where("(anuncios.denunciado = 'N' OR anuncios.denunciado = 'L' OR anuncios.denunciado = 'D')");
		$this->db->where("(anuncios.a_confirmaresponsabilidade = 1 OR anuncios.tipo_anuncio != 'A')");
		$anuncio = $this->db->get("anuncios");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $anuncio;
	}

	function curtirAnuncio($anuncio){
		if($this->getCurtidas($anuncio->id)->result() == null){
			$array = array();
			$array["ip"] = $_SERVER["REMOTE_ADDR"];
			$array["id_elemento"] = $anuncio->id;
			$array["tipo"] = "anuncios";
			$this->db->flush_cache();
			$this->db->start_cache();
			$curtida = $this->db->insert("curtidas", $array);
			$this->db->stop_cache();
			$this->db->flush_cache();
			if($curtida){
				$this->session->set_flashdata("toast", "Materialize.toast('Anúncio curtido!', 5000, 'blue');");
			}else{
				$this->session->set_flashdata("toast", "Materialize.toast('Erro ao curtir anúncio.', 5000, 'red'); Materialize.toast('Tente novamente mais tarde.', 7000, 'red'); Materialize.toast('Ou&nbsp;<a href='/fale-conosco'>fale conosco</a>.', 7000, 'red lighten-3');");
			}
		}else{
			$this->session->set_flashdata("toast", "Materialize.toast('Você já curtiu esse anúncio.', 5000, 'blue');");
		}

		switch ($anuncio->tipo_anuncio) {
			case 'A':
				redirect("/ver/ave/".$anuncio->id);	
				break;
			case 'P':
				redirect("/ver/produto/".$anuncio->id);	
				break;
			case 'S':
				redirect("/ver/servico/".$anuncio->id);	
				break;
			default:
				redirect("/");	
				break;
		}
	}

	function curtirArtigo($artigo){
		if($this->getCurtidas($artigo, "artigos")->result() == null){
			$array = array();
			$array["ip"] = $_SERVER["REMOTE_ADDR"];
			$array["id_elemento"] = $artigo;
			$array["tipo"] = "artigos";
			$this->db->flush_cache();
			$this->db->start_cache();
			$curtida = $this->db->insert("curtidas", $array);
			$this->db->stop_cache();
			$this->db->flush_cache();
			if($curtida){
				$this->session->set_flashdata("toast", "Materialize.toast('Artigo curtido!', 5000, 'blue');");
			}else{
				$this->session->set_flashdata("toast", "Materialize.toast('Erro ao curtir artigo.', 5000, 'red'); Materialize.toast('Tente novamente mais tarde.', 7000, 'red'); Materialize.toast('Ou&nbsp;<a href='/fale-conosco'>fale conosco</a>.', 7000, 'red lighten-3');");
			}
		}else{
			$this->session->set_flashdata("toast", "Materialize.toast('Você já curtiu esse artigo.', 5000, 'blue');");
		}

		redirect("/ver/artigo/".$artigo);	
	}

	function curtirEvento($evento){
		if($this->getCurtidas($evento, "eventos")->result() == null){
			$array = array();
			$array["ip"] = $_SERVER["REMOTE_ADDR"];
			$array["id_elemento"] = $evento;
			$array["tipo"] = "eventos";
			$this->db->flush_cache();
			$this->db->start_cache();
			$curtida = $this->db->insert("curtidas", $array);
			$this->db->stop_cache();
			$this->db->flush_cache();
			if($curtida){
				$this->session->set_flashdata("toast", "Materialize.toast('Evento curtido!', 5000, 'blue');");
			}else{
				$this->session->set_flashdata("toast", "Materialize.toast('Erro ao curtir evento.', 5000, 'red'); Materialize.toast('Tente novamente mais tarde.', 7000, 'red'); Materialize.toast('Ou&nbsp;<a href='/fale-conosco'>fale conosco</a>.', 7000, 'red lighten-3');");
			}
		}else{
			$this->session->set_flashdata("toast", "Materialize.toast('Você já curtiu esse evento.', 5000, 'blue');");
		}

		redirect("/ver/evento/".$evento);	
	}

	function curtirUsuario($usuario){
		if($this->getCurtidas($usuario, "usuarios")->result() == null){
			$array = array();
			$array["ip"] = $_SERVER["REMOTE_ADDR"];
			$array["id_elemento"] = $usuario;
			$array["tipo"] = "usuarios";
			$this->db->flush_cache();
			$this->db->start_cache();
			$curtida = $this->db->insert("curtidas", $array);
			$this->db->stop_cache();
			$this->db->flush_cache();
			if($curtida){
				$this->session->set_flashdata("toast", "Materialize.toast('Criador curtido!', 5000, 'blue');");
			}else{
				$this->session->set_flashdata("toast", "Materialize.toast('Erro ao curtir criador.', 5000, 'red'); Materialize.toast('Tente novamente mais tarde.', 7000, 'red'); Materialize.toast('Ou&nbsp;<a href='/fale-conosco'>fale conosco</a>.', 7000, 'red lighten-3');");
			}
		}else{
			$this->session->set_flashdata("toast", "Materialize.toast('Você já curtiu esse criador.', 5000, 'blue');");
		}

		redirect("/criadores/ver/".$usuario);
	}

	function votar_anuncio(){
		$post = $this->input->post();

		$votos = $this->session->userdata("votos");

		$this->db->flush_cache();
		$this->db->start_cache();

		if(!empty($votos[$post["id_anuncio"]])){
			$array = array("nota"=>$post["voto"]);

			$this->db->where("ip", $_SERVER["REMOTE_ADDR"]);
			$this->db->where("id_anuncio", $post["id_anuncio"]);
			$this->db->update("avaliacoes_aves", $array);

			$this->session->set_flashdata("toast", "Materialize.toast('Voto atualizado com sucesso', 5000, 'blue');");
		}else{
			$array = array("nota"=>$post["voto"], "ip"=>$_SERVER["REMOTE_ADDR"], "id_anuncio"=>$post["id_anuncio"]);
			$this->db->insert("avaliacoes_aves", $array);
			$this->session->set_flashdata("toast", "Materialize.toast('Voto efetuado com sucesso', 5000, 'blue');");
			$votos[$post["id_anuncio"]] = $post["voto"];
			$this->session->set_userdata("votos", $votos);
		}
		$this->db->stop_cache();
		$this->db->flush_cache();
	}
}