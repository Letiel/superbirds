<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EventosModel extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	function getTipos(){
		$this->db->start_cache();
		$this->db->where("tab_valores.tipo_conta", "EV");
		$this->db->where("tab_valores.situacao", "L");
		$planos = $this->db->get("tab_valores");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $planos;
	}

	function getEventos($inicio = null, $maximo = null){
		$this->db->start_cache();
		$this->db->limit($maximo, $inicio);
		$this->db->where("eventos.situacao", "L");
		$this->db->where("eventos.id_usuario_destino", $this->session->userdata("id"));

		$this->db->select("eventos.*, COUNT(curtidas.id) as curtidas");

		$this->db->join("curtidas", "(curtidas.id_elemento = eventos.id AND curtidas.tipo = 'eventos')", "left");
		$this->db->group_by("eventos.id");

		$this->db->order_by("eventos.id", "DESC");
		$eventos = $this->db->get("eventos");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $eventos;
	}

	function solicitar(){
		$post = $this->input->post();

		$post["id_usuario_solicitante"] = $this->session->userdata("id");
		$post["situacao"] = "S";
		$post["id_tab_valores"] = 0;
		$post["tipo_solicitacao"] = "EV";

		$this->db->start_cache();
		$insert = $this->db->insert("solicitacoes", $post);
		$this->db->stop_cache();
		$this->db->flush_cache();
		if($insert){
			$this->session->set_flashdata("toast", "Materialize.toast('Solicitado com sucesso.', 5000, 'blue');Materialize.toast('Aguarde retorno em seu e-mail.', 10000, 'blue');");
			redirect("/solicitacoes");
		}else{
			$this->session->set_flashdata("toast", "Materialize.toast('Erro ao realizar solicitação.', 5000, 'red');");
		}
	}

	function getListaEventos($pesquisa = null, $cidade = null, $inicio = null, $maximo = null){
		$this->db->start_cache();
		$this->db->limit($maximo, $inicio);
		if(!empty($pesquisa)){
			$this->db->where("(usuarios.nome LIKE '%$pesquisa%' OR cidades.nome LIKE '%$pesquisa%' OR estados.nome LIKE '%$pesquisa%' OR estados.uf LIKE '%$pesquisa%' OR eventos.nome LIKE '%$pesquisa%')");
		}

		if(!empty($cidade)){
			$this->db->where("cidades.id", $cidade);
		}

		$this->db->where("data_evento >= CURRENT_DATE()-30");

		$this->db->where("(usuarios.situacao = 'L' OR usuarios.situacao IS NULL)");
		$this->db->where("(usuarios.denunciado = 'N' OR usuarios.denunciado = 'L' OR usuarios.denunciado = 'D' OR usuarios.denunciado IS NULL)");

		$this->db->where("eventos.mostra_superbirds", true);
		$this->db->where("eventos.situacao", "L");
		
		$this->db->join("usuarios", "usuarios.id = eventos.id_usuario_destino", "LEFT");
		$this->db->join("cidades", "usuarios.id_cidade = cidades.id", "LEFT");
		$this->db->join("estados", "cidades.uf = estados.uf", "LEFT");

		$this->db->order_by("eventos.data_criacao DESC");

		$this->db->select("eventos.*, COUNT(curtidas.id) as curtidas");

		$this->db->join("curtidas", "(curtidas.id_elemento = eventos.id AND curtidas.tipo = 'eventos')", "left");
		$this->db->where("(curtidas.tipo = 'eventos' OR curtidas.tipo IS NULL)");
		$this->db->group_by("eventos.id");

		
		$eventos = $this->db->get("eventos");
		$this->db->stop_cache();
		$this->db->flush_cache();

		return $eventos;
	}

	function getEventosCriador($criador, $maximo = null, $inicio = null){
		$this->db->start_cache();
		$this->db->limit($maximo, $inicio);
		$this->db->where("eventos.id_usuario_destino", $criador);

		$this->db->where("data_evento >= CURRENT_DATE()-30");

		$this->db->where("(usuarios.situacao = 'L' OR usuarios.situacao IS NULL)");
		$this->db->where("(usuarios.denunciado = 'N' OR usuarios.denunciado = 'L' OR usuarios.denunciado = 'D' OR usuarios.denunciado IS NULL)");

		// $this->db->where("eventos.mostra_superbirds", true);
		$this->db->where("eventos.situacao", "L");
		
		$this->db->join("usuarios", "usuarios.id = eventos.id_usuario_destino", "LEFT");
		$this->db->join("cidades", "usuarios.id_cidade = cidades.id", "LEFT");
		$this->db->join("estados", "cidades.uf = estados.uf", "LEFT");

		$this->db->order_by("eventos.data_criacao DESC");

		$this->db->select("eventos.*, COUNT(curtidas.id) as curtidas");

		$this->db->join("curtidas", "(curtidas.id_elemento = eventos.id AND curtidas.tipo = 'eventos')", "left");
		$this->db->where("(curtidas.tipo = 'eventos' OR curtidas.tipo IS NULL)");
		$this->db->group_by("eventos.id");

		
		$eventos = $this->db->get("eventos");
		$this->db->stop_cache();
		$this->db->flush_cache();

		return $eventos;
	}

	function getEventoSite($id){
		$this->db->flush_cache();
		$this->db->start_cache();
		$this->db->where("eventos.id", $id);
		$this->db->where("(usuarios.situacao = 'L' OR usuarios.situacao IS NULL)");
		$this->db->where("(usuarios.denunciado = 'N' OR usuarios.denunciado = 'L' OR usuarios.denunciado = 'D' OR usuarios.denunciado IS NULL)");
		$this->db->where("eventos.mostra_superbirds", true);
		$this->db->where("eventos.situacao", "L");

		$this->db->join("usuarios", "usuarios.id = eventos.id_usuario_destino", "left");
		$this->db->join("cidades", "cidades.id = eventos.id_cidade", "left");
		$this->db->join("tipos_gerais", "tipos_gerais.id = eventos.tipo_evento");
		$this->db->select("eventos.*, usuarios.nome as nome_usuario, usuarios.id as id_usuario, tipos_gerais.nome as tipo, COUNT(curtidas.id) as curtidas");

		$this->db->join("curtidas", "(curtidas.id_elemento = eventos.id AND curtidas.tipo = 'eventos')", "left");
		$this->db->group_by("eventos.id");

		$evento = $this->db->get("eventos");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $evento;
	}

	function visitar($id){
		$visitas = $this->session->userdata("visitas_eventos");
		if(empty($visitas[$id])){
			$this->db->flush_cache();
			$this->db->start_cache();
			$update = $this->db->query("UPDATE eventos SET contador = contador+1 WHERE eventos.id = $id");
			$this->db->stop_cache();
			$this->db->flush_cache();
			if($update){
				$visitas[$id] = true;
				$this->session->set_userdata("visitas_eventos", $visitas);
			}
		}
	}
}