<?php
defined('BASEPATH') OR exit('Acesso não permitido!');

class HomeModel extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	function getAvesDestaque($limit = null){
		$this->db->start_cache();
		$this->db->where("anuncios.tipo_anuncio", "A");
		$this->db->where("anuncios.destaque", "1");
		$this->db->where("anuncios.situacao", "L");
		$this->db->where("(usuarios.denunciado = 'N' || usuarios.denunciado = 'L' || usuarios.denunciado = 'D')");
		$this->db->where("anuncios.a_confirmaresponsabilidade", "1");
		$this->db->order_by("anuncios.data_criacao", "DESC");
		$this->db->order_by("anuncios.data_modificacao", "DESC");
		$this->db->limit($limit);
		$this->db->where("(anuncios.denunciado = 'L' OR anuncios.denunciado = 'N' OR anuncios.denunciado = 'D')");
		$this->db->join("usuarios", "usuarios.id = anuncios.id_usuario", 'LEFT');
		$this->db->join("aves_classes", "aves_classes.id = anuncios.id_aves_classes", 'LEFT');
		$this->db->join("aves_cores", "aves_cores.id = anuncios.id_aves_cores", 'LEFT');
		$this->db->join("aves_mutacoes", "aves_mutacoes.id = anuncios.id_aves_mutacoes", 'LEFT');
		$this->db->join("tipos_gerais", "tipos_gerais.id = aves_classes.id_aves_grupos", 'LEFT');

		$this->db->join("curtidas", "(curtidas.id_elemento = anuncios.id AND curtidas.tipo = 'anuncios')", "left");

		$this->db->select("anuncios.*, aves_classes.nome as classe, aves_cores.cor, aves_cores.subcor, aves_mutacoes.nome as mutacao, tipos_gerais.nome as grupo, usuarios.nome as criador, usuarios.id as id_criador, COUNT(curtidas.id) as curtidas");

		$this->db->group_by("anuncios.id");
		$anuncios = $this->db->get("anuncios");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $anuncios;
	}

	function getAvesFamosas($limit = null){
		$this->db->start_cache();
		$this->db->where("anuncios.tipo_anuncio", "A");
		$this->db->where("anuncios.situacao", "L");
		$this->db->where("(usuarios.denunciado = 'N' || usuarios.denunciado = 'L' || usuarios.denunciado = 'D')");
		$this->db->where("anuncios.a_confirmaresponsabilidade", "1");
		$this->db->limit($limit);
		$this->db->order_by("anuncios.contador", "DESC");
		$this->db->where("(anuncios.denunciado = 'L' OR anuncios.denunciado = 'N' OR anuncios.denunciado = 'D')");
		$this->db->join("usuarios", "usuarios.id = anuncios.id_usuario", 'LEFT');
		$this->db->join("aves_classes", "aves_classes.id = anuncios.id_aves_classes", 'LEFT');
		$this->db->join("aves_cores", "aves_cores.id = anuncios.id_aves_cores", 'LEFT');
		$this->db->join("aves_mutacoes", "aves_mutacoes.id = anuncios.id_aves_mutacoes", 'LEFT');
		$this->db->join("tipos_gerais", "tipos_gerais.id = aves_classes.id_aves_grupos", 'LEFT');

		$this->db->join("curtidas", "(curtidas.id_elemento = anuncios.id AND curtidas.tipo = 'anuncios')", "left");

		$this->db->select("anuncios.*, aves_classes.nome as classe, aves_cores.cor, aves_cores.subcor, aves_mutacoes.nome as mutacao, tipos_gerais.nome as grupo, usuarios.nome as criador, usuarios.id as id_criador, COUNT(curtidas.id) as curtidas");

		$this->db->group_by("anuncios.id");
		$anuncios = $this->db->get("anuncios");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $anuncios;
	}

	function getAvesCurtidas($limit = null){
		$this->db->start_cache();
		$this->db->where("anuncios.tipo_anuncio", "A");
		$this->db->where("anuncios.situacao", "L");
		$this->db->where("usuarios.situacao", "L");
		$this->db->where("(usuarios.denunciado = 'N' || usuarios.denunciado = 'L' || usuarios.denunciado = 'D')");
		$this->db->where("anuncios.a_confirmaresponsabilidade", "1");
		$this->db->limit($limit);
		$this->db->where("(anuncios.denunciado = 'L' OR anuncios.denunciado = 'N' OR anuncios.denunciado = 'D')");
		$this->db->join("usuarios", "usuarios.id = anuncios.id_usuario", 'LEFT');
		$this->db->join("aves_classes", "aves_classes.id = anuncios.id_aves_classes", 'LEFT');
		$this->db->join("aves_cores", "aves_cores.id = anuncios.id_aves_cores", 'LEFT');
		$this->db->join("aves_mutacoes", "aves_mutacoes.id = anuncios.id_aves_mutacoes", 'LEFT');
		$this->db->join("tipos_gerais", "tipos_gerais.id = aves_classes.id_aves_grupos", 'LEFT');

		$this->db->join("curtidas", "(curtidas.id_elemento = anuncios.id AND curtidas.tipo = 'anuncios')", "left");

		$this->db->select("anuncios.*, aves_classes.nome as classe, aves_cores.cor, aves_cores.subcor, aves_mutacoes.nome as mutacao, tipos_gerais.nome as grupo, usuarios.nome as criador, usuarios.id as id_criador, COUNT(curtidas.id) as curtidas");

		$this->db->order_by("COUNT(curtidas.id)", "DESC");
		$this->db->group_by("anuncios.id");

		$anuncios = $this->db->get("anuncios");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $anuncios;
	}

	function getTotalAnuncios(){
		$this->db->start_cache();
		$this->db->where("anuncios.situacao", "L");
		$this->db->where("(anuncios.denunciado = 'L' OR anuncios.denunciado = 'N' OR anuncios.denunciado = 'D')");
		$this->db->where("(usuarios.denunciado = 'L' OR usuarios.denunciado = 'N' OR usuarios.denunciado = 'D')");
		$this->db->join("usuarios", "usuarios.id = anuncios.id_usuario");
		$this->db->select("anuncios.id");
		$anuncios = $this->db->get("anuncios");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $anuncios;
	}

	function getProximosEventos($inicio = null, $maximo = null){
		$this->db->start_cache();
		$this->db->limit($maximo, $inicio);
		$this->db->where("data_evento >= CURRENT_DATE()-30");

		$this->db->where("(usuarios.situacao = 'L' OR usuarios.situacao IS NULL)");
		$this->db->where("(usuarios.denunciado = 'N' OR usuarios.denunciado = 'L' OR usuarios.denunciado = 'D' OR usuarios.denunciado IS NULL)");

		$this->db->where("eventos.mostra_superbirds", true);
		$this->db->where("eventos.situacao", "L");
		
		$this->db->join("usuarios", "usuarios.id = eventos.id_usuario_destino", "LEFT");
		$this->db->join("cidades", "usuarios.id_cidade = cidades.id", "LEFT");
		$this->db->join("estados", "cidades.uf = estados.uf", "LEFT");

		$this->db->order_by("eventos.data_evento ASC");

		$this->db->select("eventos.*, COUNT(curtidas.id) as curtidas");

		$this->db->join("curtidas", "(curtidas.id_elemento = eventos.id AND curtidas.tipo = 'eventos')", "left");
		$this->db->where("(curtidas.tipo = 'eventos' OR curtidas.tipo IS NULL)");
		$this->db->group_by("eventos.id");

		
		$eventos = $this->db->get("eventos");
		$this->db->stop_cache();
		$this->db->flush_cache();
		
		return $eventos;
	}
}
