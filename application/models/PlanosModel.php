<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PlanosModel extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	function getValores($tipo = "PL", $inicio = NULL, $offset = NULL){
		$this->db->start_cache();
		$this->db->where("tab_valores.tipo_conta", $tipo);
		$valores = $this->db->get("tab_valores");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $valores;
	}
}