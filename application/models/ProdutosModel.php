<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProdutosModel extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	function getProdutos($destaque = false, $inicio = null, $maximo = null){
		$this->db->start_cache();
		if($destaque)
			$this->db->where("anuncios.destaque", 1);
		$this->db->limit($maximo, $inicio);
		$this->db->where("anuncios.tipo_anuncio", "P");
		$this->db->where("anuncios.id_usuario", $this->session->userdata("id"));

		$this->db->join("tipos_gerais", "tipos_gerais.id = anuncios.ps_tipo_prodserv");

		$this->db->select("anuncios.*, COUNT(curtidas.id) as curtidas");

		$this->db->join("curtidas", "(curtidas.id_elemento = anuncios.id AND curtidas.tipo = 'anuncios')", "left");
		$this->db->where("(curtidas.tipo = 'anuncios' OR curtidas.tipo IS NULL)");
		$this->db->group_by("anuncios.id");

		$this->db->order_by("anuncios.id", "DESC");
		$anuncios = $this->db->get("anuncios");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $anuncios;
	}

	function getTiposProdutos(){
		$this->db->start_cache();
		$this->db->where("tipos_gerais.tipo", "tipos_produtos");
		$tipos = $this->db->get("tipos_gerais");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $tipos;
	}

	function cadastrar(){
		$post = $this->input->post();
		$post["id_usuario"] = $this->session->userdata("id");
		$post["tipo_anuncio"] = "P";
		$post["destaque"] = false;
		$post["situacao"] = "L";
		$post["denunciado"] = "N";
		$post["contador"] = 0;
		$post["curtidas"] = 0;

		$post["valor_mostrar"] = $post["valor_mostrar"] == "valor_mostrar" ? true : false;

		if($this->db->insert("anuncios", $post)){
			$this->session->set_flashdata("toast", "Materialize.toast('Cadastrado com Sucesso!', 10000, 'blue');");
		}else{
			$this->session->set_flashdata("toast", "Materialize.toast('Ocorreu um erro interno ao cadastrar.', 10000, 'red');");
		}
			redirect("/produtos");
	}

	function getProduto($id){
		$this->db->start_cache();
		$this->db->where("anuncios.id", $id);
		$this->db->where("anuncios.tipo_anuncio", "P");
		$this->db->where("anuncios.id_usuario", $this->session->userdata("id"));
		// $this->db->join("tipos_gerais", "tipos_gerais.id = anuncios.ps_tipo_prodserv", "left");
		// $this->db->join("aves_cores", "aves_cores.id = anuncios.id_aves_cores", "left");
		$this->db->select("anuncios.*, COUNT(curtidas.id) as curtidas");

		$this->db->join("curtidas", "(curtidas.id_elemento = anuncios.id AND curtidas.tipo = 'anuncios')", "left");
		$this->db->where("(curtidas.tipo = 'anuncios' OR curtidas.tipo IS NULL)");
		$this->db->group_by("anuncios.id");

		$anuncios = $this->db->get("anuncios");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $anuncios;
	}

	function editar($id){
		$post = $this->input->post();
		$post["id_usuario"] = $this->session->userdata("id");

		$post["valor_mostrar"] = $post["valor_mostrar"] == "valor_mostrar" ? true : false;

		$post["data_modificacao"] = date("Y-m-d H:i:s");

		$this->db->start_cache();
		$this->db->where("anuncios.id", $id);
		$this->db->where("anuncios.tipo_anuncio", "P");
		$update = $this->db->update("anuncios", $post);
		$this->db->stop_cache();
		$this->db->flush_cache();
		if($update){
			$this->session->set_flashdata("toast", "Materialize.toast('Alterado com Sucesso!', 10000, 'blue');");
		}else{
			$this->session->set_flashdata("toast", "Materialize.toast('Ocorreu um erro interno ao alterar.', 10000, 'red');");
		}
		redirect("/produtos");
	}

	function alterar_situacao(){
		$post = $this->input->post();
		if($post["situacao"] == "C" || $post["situacao"] == "V" || $post["situacao"] == "L" || $post["situacao"] == "S"){
			$limites = (object) $this->usuarios->anuncios_usuario();
			if($limites->total_anuncios >= $limites->anuncios_permitidos && $post["situacao"] == "L"){
				$this->session->set_flashdata("toast", "Materialize.toast('Limite de Anúncios Ativos Atingido!', 10000, 'red');");
				exit();
			}else{
				$this->db->flush_cache();
				$this->db->start_cache();
				$this->db->where("anuncios.id", $post["id"]);
				$this->db->where("anuncios.id_usuario", $this->session->userdata("id"));
				$this->db->select("anuncios.situacao");
				$anuncio = $this->db->get("anuncios")->first_row();
				$this->db->stop_cache();
				$this->db->flush_cache();

				if($anuncio->situacao == "V" || $anuncio->situacao == "C"){
					$this->session->set_flashdata("toast", "Materialize.toast('Não é ṕossível alterar o estado deste anúncio!', 10000, 'red');");
				}else{
					$this->db->start_cache();
					$this->db->where("anuncios.id", $post["id"]);
					$this->db->where("anuncios.id_usuario", $this->session->userdata("id"));
					$this->db->where("anuncios.tipo_anuncio", "P");
					$this->db->update("anuncios", array("situacao"=>$post["situacao"]));
					$this->db->stop_cache();
					$this->db->flush_cache();
					$this->session->set_flashdata("toast", "Materialize.toast('Anúncio Atualizado com Sucesso!', 5000, 'blue');");
				}
			}
		}
	}

	function getListaProdutos($pesquisa = NULL, $cidade = NULL, $inicio = NULL, $maximo = NULL){
		$this->db->start_cache();
		$this->db->limit($maximo, $inicio);
		$this->db->where("anuncios.tipo_anuncio", "P");
		$this->db->where("anuncios.situacao", "L");
		$this->db->where("(anuncios.denunciado = 'L' OR anuncios.denunciado = 'N' OR anuncios.denunciado = 'D')");

		$this->db->join("tipos_gerais", "tipos_gerais.id = anuncios.ps_tipo_prodserv");

		$this->db->join("curtidas", "(curtidas.id_elemento = anuncios.id AND curtidas.tipo = 'anuncios')", "left");
		$this->db->where("(curtidas.tipo = 'anuncios' OR curtidas.tipo IS NULL)");
		$this->db->group_by("anuncios.id");


		$this->db->select("anuncios.*, COUNT(curtidas.id) as curtidas");
		$this->db->order_by("anuncios.id", "DESC");
		$anuncios = $this->db->get("anuncios");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $anuncios;
	}

	function getProdutosCriador($id, $maximo = null, $inicio = null){
		$this->db->start_cache();
		$this->db->limit($maximo, $inicio);
		$this->db->where("anuncios.id_usuario", $id);
		$this->db->where("anuncios.tipo_anuncio", "P");
		$this->db->where("anuncios.situacao", "L");
		$this->db->where("(anuncios.denunciado = 'L' OR anuncios.denunciado = 'N' OR anuncios.denunciado = 'D')");

		$this->db->join("tipos_gerais", "tipos_gerais.id = anuncios.ps_tipo_prodserv");
		$this->db->join("usuarios", "usuarios.id = anuncios.id_usuario");

		$this->db->select("anuncios.*, usuarios.nome as nome_usuario, usuarios.id as id_usuario, COUNT(curtidas.id) as curtidas");

		$this->db->join("curtidas", "(curtidas.id_elemento = anuncios.id AND curtidas.tipo = 'anuncios')", "left");
		$this->db->where("(curtidas.tipo = 'anuncios' OR curtidas.tipo IS NULL)");
		$this->db->group_by("anuncios.id");

		$this->db->order_by("anuncios.id", "DESC");
		$anuncios = $this->db->get("anuncios");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $anuncios;
	}

	function getProdutoSite($id){
		$this->db->start_cache();
		$this->db->where("anuncios.id", $id);
		$this->db->where("anuncios.tipo_anuncio", "P");
		$this->db->where("anuncios.situacao", "L");
		$this->db->where("(usuarios.denunciado = 'N' || usuarios.denunciado = 'L' OR anuncios.denunciado = 'D' OR usuarios.denunciado IS NULL)");
		$this->db->where("(anuncios.denunciado = 'L' OR anuncios.denunciado = 'N' OR anuncios.denunciado = 'D')");

		// $this->db->join("aves_classes", "aves_classes.id = anuncios.id_aves_classes", "left");
		// $this->db->join("aves_cores", "aves_cores.id = anuncios.id_aves_cores", "left");
		$this->db->join("usuarios", "usuarios.id = anuncios.id_usuario");
		$this->db->join("cidades", "cidades.id = usuarios.id_cidade");
		$this->db->select("anuncios.*, usuarios.nome as nome_usuario, cidades.nome as nome_cidade, cidades.uf, COUNT(curtidas.id) as curtidas");

		$this->db->join("curtidas", "(curtidas.id_elemento = anuncios.id AND curtidas.tipo = 'anuncios')", "left");
		$this->db->where("(curtidas.tipo = 'anuncios' OR curtidas.tipo IS NULL)");
		$this->db->group_by("anuncios.id");

		$anuncios = $this->db->get("anuncios");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $anuncios;
	}

	function visitar($id){
		$visitas = $this->session->userdata("visitas_produtos");
		if(empty($visitas[$id])){
			$this->db->flush_cache();
			$this->db->start_cache();
			$update = $this->db->query("UPDATE anuncios SET contador = contador+1 WHERE anuncios.id = $id");
			$this->db->stop_cache();
			$this->db->flush_cache();
			if($update){
				$visitas[$id] = true;
				$this->session->set_userdata("visitas_produtos", $visitas);
			}
		}
	}
}