<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SolicitacoesModel extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	function getSolicitacoes($inicio = NULL, $offset = NULL){
		$this->db->start_cache();
		$this->db->limit($offset, $inicio);
		$this->db->order_by("id", "DESC");
		$this->db->where("solicitacoes.situacao != 'E'");
		$this->db->where("solicitacoes.id_usuario_solicitante", $this->session->userdata("id"));
		$this->db->join("tab_valores", "tab_valores.id = solicitacoes.id_tab_valores", "LEFT");
		$this->db->select("solicitacoes.*, tab_valores.nome as selecionado");
		$solicitacoes = $this->db->get("solicitacoes");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $solicitacoes;
	}

	function getTipos(){
		$this->db->start_cache();
		$this->db->where("tipos_gerais.tipo", "tipos_solicitacoes");
		$this->db->where("tipos_gerais.situacao", "L");
		$this->db->order_by("valor", "ASC");
		$tipos = $this->db->get("tipos_gerais");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $tipos;
	}

	function solicitar($tipo){
		$post = $this->input->post();
		$post["id_usuario_solicitante"] = $this->session->userdata("id");
		$post["tipo_solicitacao"] = $tipo;
		$post["situacao"] = "S";
		$post["data_criacao"] = date("Y-m-d H:i:s");
		if($this->db->insert("solicitacoes", $post)){
			$this->session->set_flashdata("toast", "Materialize.toast('Solicitado com Sucesso. Você receberá o retorno em seu e-mail.', 10000, 'blue');");
			redirect("/solicitacoes");
		}else{
			$this->session->set_flashdata("toast", "Materialize.toast('Ocorreu um erro interno ao realizar a solicitação.', 10000, 'red');");
		}
	}

	// function solicitar_artigo(){
	// 	$post = $this->input->post();
	// 	$post["id_usuario_solicitante"] = $this->session->userdata("id");
	// 	$post["tipo_solicitacao"] = "AR";
	// 	if($this->db->insert("solicitacoes", $post)){
	// 		$this->session->set_flashdata("toast", "Materialize.toast('Solicitado com Sucesso. Você receberá o retorno em seu e-mail.', 10000, 'blue');");
	// 		redirect("/solicitacoes");
	// 	}else{
	// 		$this->session->set_flashdata("toast", "Materialize.toast('Ocorreu um erro interno ao realizar a solicitação.', 10000, 'red');");
	// 	}
	// }

	// function solicitar_evento(){
	// 	$post = $this->input->post();
	// 	$post["id_usuario_solicitante"] = $this->session->userdata("id");
	// 	$post["tipo_solicitacao"] = "EV";
	// 	if($this->db->insert("solicitacoes", $post)){
	// 		$this->session->set_flashdata("toast", "Materialize.toast('Solicitado com Sucesso. Você receberá o retorno em seu e-mail.', 10000, 'blue');");
	// 		redirect("/solicitacoes");
	// 	}else{
	// 		$this->session->set_flashdata("toast", "Materialize.toast('Ocorreu um erro interno ao realizar a solicitação.', 10000, 'red');");
	// 	}
	// }

	function solicitar_ad(){
		$post = $this->input->post();
		$post["id_usuario_solicitante"] = $this->session->userdata("id");
		$post["tipo_solicitacao"] = "AD";
		$post["data_criacao"] = date("Y-m-d H:i:s");
		if($this->db->insert("solicitacoes", $post)){
			$this->session->set_flashdata("toast", "Materialize.toast('Solicitado com Sucesso. Você receberá o retorno em seu e-mail.', 10000, 'blue');");
			redirect("/solicitacoes");
		}else{
			$this->session->set_flashdata("toast", "Materialize.toast('Ocorreu um erro interno ao realizar a solicitação.', 10000, 'red');");
		}
	}

	function getTiposAds(){
		$this->db->start_cache();
		$this->db->where("tab_valores.tipo_conta", "AD");
		$this->db->where("tab_valores.situacao", "L");
		$planos = $this->db->get("tab_valores");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $planos;
	}
}