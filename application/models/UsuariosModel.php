<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UsuariosModel extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	function cadastrar(){
		$post = $this->input->post();
		unset($post["confirma_senha"]);
		$post["tipo_documento"] = strtoupper($post["tipo_documento"]);
		$post["data_nascimento"] = $post["data_nascimento_submit"];
		$post["email_codigo"] = uniqid("SuperBirds_");
		unset($post["data_nascimento_submit"]);
		// ENVIANDO E-MAIL DE CONFIRMAÇÃO

		$this->load->library('email');

		$config['protocol'] = 'sendmail';
		$config['smtp_host'] = 'localhost';
		// $config['smtp_user'] = 'letiel';
		// $config['smtp_pass'] = 'letiel';
		// $config['smtp_port'] = '465';

		$config['mailtype'] = 'html';
		$config['mailpath'] = '/usr/sbin/sendmail';
		$config['charset'] = 'utf-8';
		$config['wordwrap'] = TRUE;

		$this->email->initialize($config);

		$this->email->from('contato@superbirds.acessonaweb.com.br', 'SuperBirds');
		$this->email->to($post["email"]);

		$this->email->subject('Confirmação de E-mail');
		$this->email->message('<h3>Olá '.$post["nome"].'. Você realizou o cadastro no site <a href="'.base_url().'">SuperBirds</a>. </h3><h4>Por favor, confirme seu e-mail clicando <a href="'.base_url().'confirmar_email/'.$post["email_codigo"].'">aqui</a></h4>');

		// FIM ENVIANDO E-MAIL DE CONFIRMAÇÃO
		if(!$this->email->send(false)){
			$this->session->set_flashdata("toast", "Materialize.toast('Não foi possível enviar o e-mail de confirmação. Certifique-se de que o e-mail está correto.', 10000, 'red');Materialize.toast('O cadastro não foi realizado.', 20000, 'red');");
		}else{
			$post['superbirds_senha'] = sha1($post['superbirds_senha']);//hash da senha
			if($this->db->insert("usuarios", $post)){
				$this->session->set_flashdata("toast", "Materialize.toast('E-mail enviado com sucesso para $post[email]', 10000, 'blue');Materialize.toast('O e-mail pode demorar alguns minutos para chegar.', 20000, 'blue');Materialize.toast('Cheque também sua caixa de spam!', 30000, 'blue');Materialize.toast('Por favor, confirme seu e-mail.', 500000, 'blue');");
				$this->session->set_flashdata("retorno", "Por favor, confirme seu e-mail clicando no link que recebeu no endereço fornecido. O e-mail pode demorar alguns minutos para chegar em sua caixa de entrada. Cheque também sua caixa de spam.");
				redirect(base_url()."login");
			}else{
				$this->session->set_flashdata("toast", "Materialize.toast('Erro ao realizar cadastro.', 5000, 'red');");
			}
		}
	}

	function confirmar_email(){
		$codigo = $this->uri->segment(2);
		if(empty($codigo)){
			$this->session->set_flashdata("toast", "Materialize.toast('Usuário não encontrado', 10000, 'red');");
			redirect(base_url()."login");
		}else{
			$this->db->start_cache();
			$this->db->where("usuarios.email_codigo", $codigo);
			$this->db->where("usuarios.email_confirmado", false);
			$usuario = $this->db->get("usuarios");
			$this->db->stop_cache();
			$this->db->flush_cache();

			if($usuario->num_rows() > 0){
				$usuario = $usuario->first_row();
				$this->db->start_cache();
				$this->db->where("usuarios.id", $usuario->id);
				$update = $this->db->update("usuarios", array("email_confirmado"=>true, "email_codigo"=>null));
				$this->db->stop_cache();
				$this->db->flush_cache();
				if($update){
					$this->session->set_flashdata("toast", "Materialize.toast('Confirmado com sucesso!', 10000, 'blue');");
					$this->session->set_userdata("logado", true);
					$this->session->set_userdata("id", $usuario->id);
					$this->session->set_userdata("nome", $usuario->nome);
					$this->session->set_userdata("login", $usuario->superbirds_usuario);
					$this->session->set_userdata("tipo", $usuario->tipo);
					redirect(base_url()."interno");
				}else{
					$this->session->set_flashdata("toast", "Materialize.toast('Ocorreu um erro interno ao realizar confirmação de e-mail.', 10000, 'red');");
				}
			}else{
				$this->session->set_flashdata("toast", "Materialize.toast('Usuário não encontrado', 10000, 'red');");
				redirect(base_url()."login");
			}
		}
	}

	function login(){
		$post = $this->input->post();
		$this->db->start_cache();
		$this->db->like("superbirds_usuario", $post["login"]);
		$this->db->where("superbirds_senha", sha1($post["senha"]));
		$usuario = $this->db->get("usuarios");
		$this->db->stop_cache();
		$this->db->flush_cache();

		if($usuario->num_rows() > 0){
			$usuario = $usuario->first_row();
			if($usuario->situacao == "L" && $usuario->email_confirmado == 1){
				$this->session->set_userdata("logado", true);
				$this->session->set_userdata("id", $usuario->id);
				$this->session->set_userdata("nome", $usuario->nome);
				$this->session->set_userdata("login", $usuario->superbirds_usuario);
				$this->session->set_userdata("tipo", $usuario->tipo);

				if(!empty($usuario->denunciado_motivo))
					$this->session->set_flashdata("toast", "Materialize.toast('Bem vindo.', 10000, 'blue');Materialize.toast('<b>Atenção! Você recebeu uma denúncia.</b>', 15000, 'red');Materialize.toast('<b>".$usuario->denunciado_motivo."<b>', 25000, 'red');");
				else
					$this->session->set_flashdata("toast", "Materialize.toast('Bem vindo.', 5000, 'blue');");
				redirect(base_url()."interno");
			}else if($usuario->email_confirmado == 0){
				$this->session->set_flashdata('retorno', "E-mail não confirmado.");
			}else if($usuario->situacao == "B"){
				$this->session->set_flashdata('retorno', "Usuário bloqueado.");
			}
		}else{
			$this->session->set_flashdata('retorno', "Usuário e senha não combinam!");
		}
	}

	function logado($redirect = false){
		$retorno = true;
		if(!$this->session->userdata("logado")){
			$retorno = false;
		}

		if(empty($this->session->userdata("id"))){
			$retorno = false;
		}

		if(empty($this->session->userdata("login"))){
			$retorno = false;
		}

		if(empty($this->session->userdata("tipo"))){
			$retorno = false;
		}

		if(empty($this->session->userdata("nome"))){
			$retorno = false;
		}
		if($redirect && !$retorno){
			$this->session->set_flashdata("toast", "Materialize.toast('Sua sessão expirou.', 15000, 'blue');");
			redirect(base_url()."login");
		}
		return $retorno;
	}

	function perfil(){
		$this->db->start_cache();
		$this->db->where("usuarios.id", $this->session->userdata("id"));
		$this->db->join("cidades", "cidades.id = usuarios.id_cidade");
		$this->db->select("usuarios.*, cidades.nome as nome_cidade, cidades.uf");
		$usuario = $this->db->get("usuarios");
		$this->db->stop_cache();
		$this->db->flush_cache();
		if($usuario->num_rows() == 0){
			$this->session->set_flashdata("toast", "Materialize.toast('Ocorreu um erro de autenticação.', 10000, 'red');");
			redirect(base_url()."logout");
			return false;
		}
		return $usuario;
	}

	function atualizar_perfil(){
		$post = $this->input->post();
		$post["tipo_documento"] = strtoupper($post["tipo_documento"]);
		$post["data_nascimento"] = $post["data_nascimento_submit"];
		$post["data_modificacao"] = date("Y-m-d H:i:s");

		@$post["mostra_cpfcnpj"] = $post["mostra_cpfcnpj"] == "mostra_cpfcnpj" ? true : false;
		@$post["mostra_razaosocial"] = $post["mostra_razaosocial"] == "mostra_razaosocial" ? true : false;
		@$post["mostra_endereco"] = $post["mostra_endereco"] == "mostra_endereco" ? true : false;
		@$post["mostra_fone"] = $post["mostra_fone"] == "mostra_fone" ? true : false;
		@$post["mostra_cel"] = $post["mostra_cel"] == "mostra_cel" ? true : false;
		@$post["mostra_whats"] = $post["mostra_whats"] == "mostra_whats" ? true : false;
		@$post["usu_mostra_listaclubes"] = $post["usu_mostra_listaclubes"] == "usu_mostra_listaclubes" ? true : false;

		unset($post["data_nascimento_submit"]);
		$this->db->where("usuarios.id", $this->session->userdata("id"));
		if($this->db->update("usuarios", $post)){
			$this->session->set_flashdata("toast", "Materialize.toast('Atualizado com sucesso.', 5000, 'blue');");
			redirect(base_url()."perfil");
		}else{
			$this->session->set_flashdata("toast", "Materialize.toast('Erro ao atualizar cadastro.', 5000, 'red');");
		}
	}

	function atualizar_foto(){
		$post = $this->input->post();
		$post["foto"];
		$post["id"];
		$this->db->start_cache();
		$this->db->where("usuarios.id", $post["id"]);
		$this->db->update("usuarios", array("img_principal"=>$post["foto"]));
		$this->db->stop_cache();
		$this->db->flush_cache();
		if($post["foto"] == ""){
			unlink("img/".$post["foto"]);
		}
	}

	function atualizar_banner(){
		$post = $this->input->post();
		$post["foto"];
		$post["id"];
		$this->db->start_cache();
		$this->db->where("usuarios.id", $post["id"]);
		$this->db->update("usuarios", array("img_banner"=>$post["foto"]));
		$this->db->stop_cache();
		$this->db->flush_cache();
		if($post["foto"] == ""){
			unlink("img/".$post["foto"]);
		}
	}

	function getAnunciosDestaque(){
		$this->db->start_cache();
		$this->db->where("anuncios.id_usuario", $this->session->userdata("id"));
		$this->db->where("anuncios.destaque", "1");
		
		$this->db->select("anuncios.id");
		$anuncios = $this->db->get("anuncios");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $anuncios;
	}

	function anuncios_usuario(){//retorna a quantidade de anúncios que o usuário tem disponível
		$anuncios_permitidos = 0;
		$anuncios_destaque_permitidos = 0;
		$this->db->start_cache();
		$this->db->where("usuarios.id", $this->session->userdata("id"));
		$this->db->where("tab_valores.tipo_conta", "PL");
		$this->db->where("tab_valores.situacao", "L");

		$this->db->where("usuarios.plano_vencimento >= '".date("Y-m-d")."'");

		$this->db->join("tab_valores", "tab_valores.id = usuarios.id_plano");
		$this->db->select("tab_valores.num_anuncios, tab_valores.num_anuncios_destaque");
		$plano = $this->db->get("usuarios");
		$this->db->stop_cache();
		$this->db->flush_cache();

		if($plano->num_rows() > 0){
			$plano = $plano->first_row();
			$anuncios_permitidos += $plano->num_anuncios;
			$anuncios_destaque_permitidos += $plano->num_anuncios_destaque;
		}


		$this->db->flush_cache();
		$this->db->start_cache();
		$this->db->where("ad_vencimento >= '".date("Y-m-d")."'");
		$this->db->where("tipo_conta", "AD");
		$this->db->where("id_usuario", $this->session->userdata("id"));
		$this->db->where("situacao", "P");

		$this->db->select("ad_quantidade");
		$ads = $this->db->get("usuarios_contas");
		$this->db->stop_cache();
		$this->db->flush_cache();

		$ads = $ads->result();
		foreach($ads as $ad){
			$anuncios_destaque_permitidos += $ad->ad_quantidade;
		}


		$this->db->start_cache();
		$this->db->where("anuncios.id_usuario", $this->session->userdata("id"));
		$this->db->where("anuncios.situacao = \"L\" OR anuncios.situacao = \"V\"");
		$total_anuncios = $this->db->get("anuncios")->num_rows();
		$this->db->stop_cache();
		$this->db->flush_cache();

		$this->load->model("AvesModel", "aves");
		$total_anuncios = $this->aves->getAves()->num_rows();
		$total_anuncios_destaque = $this->getAnunciosDestaque()->num_rows();

		$this->db->start_cache();
		$this->db->where("usuarios_contas.id_usuario", $this->session->userdata("id"));
		$this->db->where("usuarios_contas.tipo_conta", "EV");
		$this->db->where("usuarios_contas.situacao", "P");
		$this->db->where("usuarios_contas.data_vencimento >= '".date("Y-m-d")."'");
		$this->db->select("COUNT(id) AS total");
		$eventos_permitidos = $this->db->get("usuarios_contas")->first_row()->total;//total de eventos disponíveis
		$this->db->stop_cache();
		$this->db->flush_cache();

		$this->db->start_cache();
		$this->db->where("eventos.id_usuario_destino", $this->session->userdata("id"));
		$this->db->where("eventos.situacao = \"L\"");
		$total_eventos = $this->db->get("eventos")->num_rows();
		$this->db->stop_cache();
		$this->db->flush_cache();

		$this->db->start_cache();
		$this->db->where("usuarios_contas.id_usuario", $this->session->userdata("id"));
		$this->db->where("usuarios_contas.tipo_conta", "AR");
		$this->db->where("usuarios_contas.situacao", "P");
		$this->db->where("usuarios_contas.data_vencimento >= '".date("Y-m-d")."'");
		$this->db->select("COUNT(id) AS total");
		$artigos_permitidos = $this->db->get("usuarios_contas")->first_row()->total;//total de eventos disponíveis
		$this->db->stop_cache();
		$this->db->flush_cache();

		$this->db->start_cache();
		$this->db->where("artigos.id_usuario_destino", $this->session->userdata("id"));
		$this->db->where("artigos.situacao = \"L\"");
		$total_artigos = $this->db->get("artigos")->num_rows();
		$this->db->stop_cache();
		$this->db->flush_cache();

		$retorno = array(
			"anuncios_permitidos"=>$anuncios_permitidos, 
			"anuncios_destaque_permitidos"=>$anuncios_destaque_permitidos,
			"total_anuncios"=>$total_anuncios,
			"eventos_permitidos"=>$eventos_permitidos,
			"total_eventos"=>$total_eventos,
			"artigos_permitidos"=>$artigos_permitidos,
			"total_artigos"=>$total_artigos,
			"total_anuncios_destaque"=>$total_anuncios_destaque,
		);

		return $retorno;
	}

	function alterar_senha(){
		$post = $this->input->post();

		$this->db->flush_cache();
		$this->db->start_cache();
		$this->db->where("usuarios.id", $this->session->userdata("id"));
		$this->db->select("usuarios.id, usuarios.superbirds_senha");
		$usuario = $this->db->get("usuarios")->first_row();
		$this->db->stop_cache();
		$this->db->flush_cache();

		if($usuario->superbirds_senha == sha1($post["senha_antiga"])){
			$this->db->flush_cache();
			$this->db->start_cache();
			$this->db->where("usuarios.id", $usuario->id);
			$this->db->update("usuarios", array("superbirds_senha"=>sha1($post["nova_senha"])));
			$this->db->stop_cache();
			$this->db->flush_cache();

			$this->session->set_flashdata("toast", "Materialize.toast('Senha alterada com sucesso.', 10000, 'blue');");
			redirect("/interno");
		}else{
			$this->session->set_flashdata("retorno_senha", "A senha fornecida não confere!");
		}
	}

	// --------------------------SITE----------------------------------
	function getCriadores($pesquisa = null, $cidade = null, $inicio = null, $maximo = null){
		$this->db->start_cache();
		$this->db->limit($maximo, $inicio);
		if(!empty($pesquisa)){
			$this->db->where("(usuarios.nome LIKE '%$pesquisa%' OR cidades.nome LIKE '%$pesquisa%' OR estados.nome LIKE '%$pesquisa%' OR estados.uf LIKE '%$pesquisa%')");
		}

		if(!empty($cidade)){
			$this->db->where("cidades.id", $cidade);
		}

		$this->db->order_by("usuarios.data_criacao", "DESC");
		$this->db->where("usuarios.tipo", "USU");
		$this->db->where("usuarios.situacao", "L");
		$this->db->where("(usuarios.denunciado = 'N' || usuarios.denunciado = 'L' || usuarios.denunciado = 'D')");

		$this->db->where("(anuncios.tipo_anuncio = 'A' || anuncios.tipo_anuncio IS NULL || anuncios.tipo_anuncio = '')");
		// $this->db->where("(anuncios.situacao = 'L' || anuncios.situacao IS NULL || anuncios.situacao = '')");
		$this->db->where("(anuncios.denunciado = 'N' || anuncios.denunciado = 'L' || anuncios.denunciado = 'D' || anuncios.denunciado IS NULL || anuncios.denunciado = '')");

		$this->db->where("(anuncios.a_confirmaresponsabilidade = 1 OR anuncios.a_confirmaresponsabilidade IS NULL)");

		$this->db->join("anuncios", "(anuncios.id_usuario = usuarios.id AND anuncios.situacao = 'L')", "LEFT");
		$this->db->join("cidades", "cidades.id = usuarios.id_cidade", "LEFT");
		$this->db->join("estados", "cidades.uf = estados.uf", "LEFT");

		$this->db->select("usuarios.*, COUNT(anuncios.id) as total_aves");
		$this->db->group_by("usuarios.id");
		$criadores = $this->db->get("usuarios");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $criadores;
	}

	function getTotalAvesCriador($criador){
		$this->db->flush_cache();
		$this->db->start_cache();
		$this->db->where("anuncios.id_usuario", $criador);
		$this->db->where("(anuncios.tipo_anuncio = 'A' || anuncios.tipo_anuncio IS NULL)");
		$this->db->where("(anuncios.situacao = 'L' || anuncios.situacao IS NULL)");
		$this->db->where("(anuncios.denunciado = 'N' || anuncios.denunciado = 'L' || anuncios.denunciado = 'D' || anuncios.denunciado IS NULL)");
		$this->db->where("(anuncios.a_confirmaresponsabilidade = 1 OR anuncios.a_confirmaresponsabilidade IS NULL)");

		$this->db->select("COUNT(anuncios.id) as total_aves");
		$total = $this->db->get("anuncios");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $total;
	}

	function getClubes($pesquisa = null, $cidade = null, $inicio = null, $maximo = null){
		$this->db->start_cache();
		$this->db->limit($maximo, $inicio);
		if(!empty($pesquisa)){
			$this->db->where("(usuarios.nome LIKE '%$pesquisa%' OR cidades.nome LIKE '%$pesquisa%' OR estados.nome LIKE '%$pesquisa%' OR estados.uf LIKE '%$pesquisa%')");
		}

		if(!empty($cidade)){
			$this->db->where("cidades.id", $cidade);
		}

		$this->db->order_by("usuarios.data_criacao", "DESC");
		$this->db->where("usuarios.tipo", "CAG");
		$this->db->where("usuarios.situacao", "L");
		$this->db->where("(usuarios.denunciado = 'N' || usuarios.denunciado = 'L' || usuarios.denunciado = 'D')");

		$this->db->where("(anuncios.tipo_anuncio = 'A' || anuncios.tipo_anuncio IS NULL)");
		$this->db->where("(anuncios.situacao = 'L' || anuncios.situacao IS NULL)");
		$this->db->where("(anuncios.denunciado = 'N' || anuncios.denunciado = 'L' || anuncios.denunciado = 'D' || anuncios.denunciado IS NULL)");
		$this->db->join("anuncios", "anuncios.id_usuario = usuarios.id", "LEFT");
		$this->db->join("cidades", "cidades.id = usuarios.id_cidade", "LEFT");
		$this->db->join("estados", "cidades.uf = estados.uf", "LEFT");

		$this->db->select("usuarios.*, COUNT(anuncios.id) as total_aves");
		$this->db->group_by("usuarios.id");
		$criadores = $this->db->get("usuarios");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $criadores;
	}

	function getCriador($id){
		$this->db->start_cache();
		$this->db->where("usuarios.id", $id);
		$this->db->where("usuarios.situacao", "L");
		$this->db->where("(usuarios.denunciado = 'N' || usuarios.denunciado = 'L' || usuarios.denunciado = 'D')");

		$this->db->join("cidades", "cidades.id = usuarios.id_cidade");

		$this->db->where("(curtidas.tipo = 'usuarios' OR curtidas.tipo IS NULL)");
		$this->db->join("curtidas", "curtidas.id_elemento = usuarios.id", "left");

		$this->db->select("usuarios.*, cidades.nome as nome_cidade, cidades.uf, COUNT(curtidas.id) as curtidas");

		$this->db->group_by("usuarios.id");
		$usuario = $this->db->get("usuarios");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $usuario;
	}

	function getCriadorSub($sub){
		$this->db->start_cache();
		$this->db->where("usuarios.superbirds_subdominio", $sub);
		$this->db->or_where("usuarios.superbirds_subdominio", "http://".$sub);
		$this->db->or_where("usuarios.superbirds_subdominio", "https://".$sub);
		$this->db->or_where("usuarios.superbirds_subdominio", "https://www.".$sub);
		$this->db->or_where("usuarios.superbirds_subdominio", "www.".$sub);
		$this->db->where("usuarios.situacao", "L");
		$this->db->where("(usuarios.denunciado = 'N' || usuarios.denunciado = 'L' || usuarios.denunciado = 'D')");

		$this->db->join("cidades", "cidades.id = usuarios.id_cidade");

		$this->db->where("curtidas.tipo = 'usuarios' OR curtidas.tipo IS NULL");
		$this->db->join("curtidas", "curtidas.id_elemento = usuarios.id");

		$this->db->select("usuarios.*, cidades.nome as nome_cidade, cidades.uf, COUNT(curtidas.id) as curtidas");

		$this->db->group_by("usuarios.id");
		$usuario = $this->db->get("usuarios");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $usuario;
	}

	function getCriadorSub2($sub){ //tem 2 pois um vai enviar JSON e não deve trafegar algumas infos
		$this->db->start_cache();
		$this->db->where("usuarios.situacao", "L");

		$this->db->where("curtidas.tipo = 'usuarios' OR curtidas.tipo IS NULL");

		$this->db->where("(usuarios.denunciado = 'N' || usuarios.denunciado = 'L' || usuarios.denunciado = 'D')");
		$this->db->where("usuarios.superbirds_subdominio", $sub);
		$this->db->or_where("usuarios.superbirds_subdominio", "http://".$sub);
		$this->db->or_where("usuarios.superbirds_subdominio", "https://".$sub);
		$this->db->or_where("usuarios.superbirds_subdominio", "https://www.".$sub);
		$this->db->or_where("usuarios.superbirds_subdominio", "www.".$sub);
		$this->db->join("cidades", "cidades.id = usuarios.id_cidade");

		$this->db->join("curtidas", "curtidas.id_elemento = usuarios.id");

		$this->db->select("usuarios.nome, usuarios.id, usuarios.img_principal, usuarios.superbirds_email, usuarios.email_form_contato, usuarios.email, usuarios.frase, usuarios.img_banner, usuarios.tipo, usuarios.cag_tipo, usuarios.cag_sigla, usuarios.cag_anilha, cidades.nome as nome_cidade, cidades.uf, usuarios.denunciado, usuarios.site, COUNT(curtidas.id) as curtidas");
		$this->db->group_by("usuarios.id");
		$usuario = $this->db->get("usuarios");
		$this->db->stop_cache();
		$this->db->flush_cache();
		return $usuario;
	}

	function recuperarSenha(){
		$post = $this->input->post();

		$this->db->flush_cache();
		$this->db->start_cache();

		$this->db->or_where("usuarios.email", $post["login"]);
		$this->db->or_where("usuarios.superbirds_email", $post["login"]);
		$usuario = $this->db->get("usuarios");

		$this->db->stop_cache();
		$this->db->flush_cache();

		if($usuario->num_rows() > 0){
			$usuario = $usuario->first_row();

			$nova_senha = uniqid();
			$this->db->flush_cache();
			$this->db->start_cache();
			$this->db->where("usuarios.id", $usuario->id);
			$alterado = $this->db->update("usuarios", array("superbirds_senha"=>sha1($nova_senha)));
			$this->db->stop_cache();
			$this->db->flush_cache();

			$this->load->library('email');

			$config['charset'] = 'utf-8';
			$config['wordwrap'] = TRUE;
			$config['mailtype'] = "html";

			$this->email->initialize($config);

			$this->email->from('contato@superbirds.acessonaweb.com.br', 'SuperBirds');

			
			$this->email->to($usuario->email);
			
			if(!empty($usuario->superbirds_email))
				$this->email->cc($usuario->superbirds_email);

			$this->email->subject('Contato - SuperBirds');
			$this->email->message("
			<h3>Recuperação de senha.</h3>
			<h4>Olá. Sua senha foi recuperada no <a href='".base_url()."'>Superbirds</a>.</h4>
			<br>
			<p>Sua nova senha é: $nova_senha</p>
			<p>Você pode <a href='".base_url()."'>entrar</a> utilizando este endereço de e-mail e a nova senha fornecida.</p>
			");

			if($this->email->send()){
				$this->session->set_flashdata("toast", "Materialize.toast('Sua nova senha foi enviada para seu e-mail.', 50000, 'blue');Materialize.toast('Pode ocorrer uma demora no recebimento do e-mail Aguarde até 15 minutos.', 50000, 'blue');");
				print_r($usuario);
				// redirect("/login");
			}else{
				$this->session->set_flashdata("toast", "Materialize.toast('<p>Não foi possível enviar a nova senha para seu e-mail.', 70000, 'red');Materialize.toast('Envie um e-mail para&nbsp;<b>contato@superbirds.com.br</b>', 77000, 'red');");
			}
		}else{
			$this->session->set_flashdata("retorno", "Conta não encontrada.");
		}
	}

	function visitar($id){
		$visitas = $this->session->userdata("visitas_usuarios");
		if(empty($visitas[$id])){
			$this->db->flush_cache();
			$this->db->start_cache();
			$update = $this->db->query("UPDATE usuarios SET contador = contador+1 WHERE usuarios.id = $id");
			$this->db->stop_cache();
			$this->db->flush_cache();
			if($update){
				$visitas[$id] = true;
				$this->session->set_userdata("visitas_usuarios", $visitas);
			}
		}
	}
}