<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<title>Perfil - SuperBirds</title>
	<?php include 'inc/interno-head.php' ?>
	<link href="/css/select2.css" rel="stylesheet">

	<!-- CROP -->
	<link rel="stylesheet" href="/assets/css/imgpicker.css">
	<!-- CROP -->
</head>
<body class="blue-grey lighten-5">
	<?php include 'inc/interno-menu_lateral.php' ?>
	<?php include 'inc/interno-topo.php' ?>
	
	<main>
		<div class="container-fluid">

			<div class="card">
				<form method="post">
					<div class="card-content">
						<span class="card-title blue-grey-text">Alterar Senha</span>
						<hr>
						<div class="row">
							<div class="input-field col s12 m3">
								<label for="senha_antiga">Senha Antiga</label>
								<input type="text" name="senha_antiga" id="senha_antiga" value="<?php echo set_value("senha_antiga") ?>"/>
								<span class='red-text'><?php echo form_error('senha_antiga') ?></span>
								<span class='red-text'><?php echo $this->session->flashdata("retorno_senha") ?></span>
							</div>
							<div class="input-field col s12 m3">
								<label for="nova_senha">Nova Senha</label>
								<input type="text" name="nova_senha" id="nova_senha" value="<?php echo set_value("nova_senha") ?>"/>
								<span class='red-text'><?php echo form_error('nova_senha') ?></span>
							</div>
							<div class="input-field col s12 m3">
								<label for="confirmacao_senha">Confirmação da Senha</label>
								<input type="text" name="confirmacao_senha" id="confirmacao_senha" value="<?php echo set_value("confirmacao_senha") ?>"/>
								<span class='red-text'><?php echo form_error('confirmacao_senha') ?></span>
							</div>
						</div>
					</div>
					<div class="card-action">
						<div class="right-align">
							<button class="btn blue">Alterar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</main>

	<!-- Avatar Modal -->
	<div class="ip-modal" id="avatarModal">
	    <div class="ip-modal-dialog">
	        <div class="ip-modal-content">
	            <div class="ip-modal-header">
	                <a class="ip-close" title="Close">&times;</a>
	                <h4 class="ip-modal-title">Alterar Imagem</h4>
	            </div>
	            <div class="ip-modal-body">
	                <div class="btn blue ip-upload">Enviar <input type="file" name="file" class="ip-file"></div>
	                
	                <button type="button" class="btn blue lighten-1 ip-edit">Editar</button>
	                <button type="button" class="btn red ip-delete">Excluir</button>

	                <div class="alert ip-alert"></div>
	                <div class="ip-info">Para cortar esta imagem, arraste o mouse na região abaixo e clique em "Salvar Imagem"</div>
	                <div class="ip-preview"></div>
	                <div class="ip-rotate">
	                    <button type="button" class="btn ip-rotate-ccw" title="Rotate counter-clockwise"><i class="icon-ccw"></i></button>
	                    <button type="button" class="btn ip-rotate-cw" title="Rotate clockwise"><i class="icon-cw"></i></button>
	                </div>
	                <div class="ip-progress">
	                    <div class="text">Enviando</div>
	                    <div class="progress progress-striped active"><div class="progress-bar"></div></div>
	                </div>
	            </div>
	            <div class="ip-modal-footer">
	                <div class="ip-actions">
	                    <button type="button" class="btn blue ip-save">Salvar Imagem</button>
	                    <button type="button" class="btn grey ip-cancel">Cancelar</button>
	                </div>
	                <button type="button" class="btn red darken-5 ip-close">Fechar</button>
	            </div>
	        </div>
	    </div>
	</div>
	<!-- end Modal -->

	<!-- Header Modal -->
	<div class="ip-modal" id="headerModal">
	    <div class="ip-modal-dialog">
	        <div class="ip-modal-content">
	            <div class="ip-modal-header">
	                <a class="ip-close" title="Close">&times;</a>
	                <h4 class="ip-modal-title">Alterar Banner</h4>
	            </div>
	            <div class="ip-modal-body">
	                <div class="btn btn-primary ip-upload">Enviar <input type="file" name="file" class="ip-file"></div>
	                <!-- <button type="button" class="btn btn-primary ip-webcam">Webcam</button> -->
	                <button type="button" class="btn btn-info ip-edit">Editar</button>

	                <div class="alert ip-alert"></div>
	                <div class="ip-info">Para cortar esta imagem, arraste o mouse na região abaixo e clique em "Salvar Imagem"</div>
	                <div class="ip-preview"></div>
	                <div class="ip-rotate">
	                    <button type="button" class="btn btn-default ip-rotate-ccw" title="Rotate counter-clockwise"><i class="icon-ccw"></i></button>
	                    <button type="button" class="btn btn-default ip-rotate-cw" title="Rotate clockwise"><i class="icon-cw"></i></button>
	                </div>
	                <div class="ip-progress">
	                    <div class="text">Enviando</div>
	                    <div class="progress progress-striped active"><div class="progress-bar"></div></div>
	                </div>
	            </div>
	            <div class="ip-modal-footer">
	                <div class="ip-actions">
	                    <button type="button" class="btn btn-success ip-save">Salvar Imagem</button>
	                    <button type="button" class="btn btn-default ip-cancel">Cancelar</button>
	                </div>
	                <button type="button" class="btn btn-default ip-close">Fechar</button>
	            </div>
	        </div>
	    </div>
	</div>
	<!-- end Modal -->

	<?php include 'inc/interno-footer.php' ?>
	<?php include 'inc/interno-js.php' ?>
	<script type="text/javascript" src="/js/select2.full.min.js"></script>
	<script type="text/javascript" src="/js/i18n/pt-BR.js"></script>
	<script type="text/javascript" src="/js/jquery.mask.min.js"></script>

	<!-- CROP -->
	<!-- <script src="/assets/js/jquery-1.11.0.min.js"></script> -->
	<script src="/assets/js/jquery.Jcrop.min.js"></script>
	<script src="/assets/js/jquery.imgpicker.js"></script>
	<!-- CROP -->

	<script type="text/javascript">
		$(document).ready(function(){
			Materialize.updateTextFields();
			$('#ramo_principal').material_select();
			$('#cidade').select2({
				language: "pt-BR",
				placeholder: "Cidade",
				minimumInputLength: 3,
				ajax: {
				    url: '/select/cidades',
				    dataType: 'json',
				    delay: 250
				    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
			  	}
			});

			$('#avatarModal').imgPicker({
	            url: '/server/upload_avatar.php',
	            aspectRatio: 1,
	            data: {
	                uniqid: '<?php echo ($usuario->img_principal != null && $usuario->img_principal != "" ? $usuario->img_principal : uniqid("usuario_")) ?>',
	                id: <?php echo $usuario->id ?>
	            },
	            deleteComplete: function() {
	                $('#avatar2').attr('src', '//gravatar.com/avatar/0?d=mm&s=150');
	                $.post("/perfil/atualizar_foto", {
	                    foto: "",
	                    id: <?php echo $usuario->id ?>
	                }, function(result){

	                });
	                this.modal('hide');
	            },
	            uploadSuccess: function(image) {
	                // Calculate the default selection for the cropper
	                var select = (image.width > image.height) ?
	                [(image.width-image.height)/2, 0, image.height, image.height] :
	                [0, (image.height-image.width)/2, image.width, image.width];      
	                this.options.setSelect = select;
	            },
	            cropSuccess: function(image) {
	                $('#avatar2').attr('src', '//gravatar.com/avatar/0?d=mm&s=150');
	                $('#avatar2').attr('src', "/img/"+image.name);
	                $.post("/perfil/atualizar_foto", {
	                    foto: image.name,
	                    id: <?php echo $usuario->id ?>
	                }, function(result){
	                    location.reload();
	                });
	                this.modal('hide');
	            }
	        });

		});

		$('#headerModal').imgPicker({
		    url: '/server/upload_header.php',
		    aspectRatio: 998/400,
		    data: {
		        uniqid: '<?php echo uniqid("banner_") ?>',
		        id: <?php echo $usuario->id ?>
		    },
		    setSelect: [0, 0, 10, 10],
		    deleteComplete: function() {
		        $('.header img').attr('src', '');
		        this.modal('hide');
		        $.post("/perfil/atualizar_banner", {
		            foto: "",
		            id: <?php echo $usuario->id ?>
		        }, function(result){
		            location.reload();
		        });
		    },
		    cropSuccess: function(image) {
		        $('.header img').attr('src', '/img/' + image.url);

		        $.post("/perfil/atualizar_banner", {
		            foto: image.name,
		            id: <?php echo $usuario->id ?>
		        }, function(result){
		            location.reload();
		        });
		        this.modal('hide');
		    }
		});

		$(".remover_banner").click(function(){
			if(confirm("Tem certeza?")){
				$.post("/perfil/atualizar_banner", {
				    foto: "",
				    id: <?php echo $usuario->id ?>
				}, function(result){
				    location.reload();
				});
			}
		});

		<?php if($usuario->tipo_documento == "CNPJ"){ ?>
			$("#cpfcnpj").mask("99.999.999/9999-99");
		<?php }else{ ?>
			$("#cpfcnpj").mask("999.999.999-99");
		<?php } ?>
		$("#cep").mask("99999-999");
		$("#fone").mask("(99)99999 9999");
		$("#cel").mask("(99)99999 9999");
		$(".tipo_documento").change(function(){
			tipo = $(this).val();
			if(tipo == "cpf"){
				$("#cpfcnpj").mask("999.999.999-99");
				$("#cpfcnpj").parent().find("label").html("CPF");
			}else if(tipo == "cnpj"){
				$("#cpfcnpj").mask("99.999.999/9999-99");
				$("#cpfcnpj").parent().find("label").html("CNPJ");
			}
			Materialize.updateTextFields();
		});
		$('.datepicker').pickadate({
		    selectMonths: true, // Creates a dropdown to control month
		    selectYears: 100,
		    max: true,
		    min: [1900, 1, 1],
		    today: 'Hoje',
		    clear: 'Limpar',
		    close: 'Selecionar',
		    weekdaysLetter: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
		    weekdaysShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
		    weekdaysFull: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		    monthsShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
		    monthsFull: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
		    labelMonthSelect: 'Selecione o Mês',
	        labelYearSelect: 'Selecione o Ano',
	        labelMonthNext: 'Próximo Mês',
            labelMonthPrev: 'Mês Anterior',
            format: 'dd/mm/yyyy',
            formatSubmit: 'yyyy-mm-dd',
		    closeOnSelect: true // Close upon selecting a date,
		});
	</script>
</body>
</html>