<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<title>Incluir Ave - SuperBirds</title>
	<?php include 'inc/interno-head.php' ?>
	<link href="/css/select2.css" rel="stylesheet">

	<!-- CROP -->
	<link rel="stylesheet" href="/assets/css/imgpicker.css">
	<!-- CROP -->
</head>
<body class="blue-grey lighten-5">
	<?php include 'inc/interno-menu_lateral.php' ?>
	<?php include 'inc/interno-topo.php' ?>
	
	<main>
		<div class="container-fluid">
			<div class="card">
				<form method="post">
					<div class="card-content">
						<span class="card-title blue-grey-text">Incluir Ave - SuperBirds</span>
						<!-- <hr> -->
						
						<div class="row">
							<div class="col s12 m4 l3">
								<img src="<?php echo !empty(set_value('img_principal')) ? '/img/'.set_value('img_principal') : '//gravatar.com/avatar/0?d=mm&s=150' ?>" alt="Avatar" class="image" style="width:100%" id="avatar2" >
								<input value="<?php echo set_value('img_principal') ?>" type="hidden" name="img_principal" id="img_principal" style="display: none;" />
								<button type="button" class="btn blue btn-block blue-grey" data-ip-modal="#avatarModal">Foto</button>
								<span class='red-text'><?php echo form_error('img_principal') ?></span>
							</div>
							<div class="col s12 m8 l9">
								<div class="row">
									<div class="input-field col s12 m6">
										<label for="Nome">Nome</label>
										<input type="text" name="nome" id="nome" value="<?php echo set_value('nome') ?>"/>
										<span class='red-text'><?php echo form_error('nome') ?></span>
									</div>
									<div class="input-field col s12 m6">
										<label for="cod_referencia">Cód. Referência</label>
										<input type="text" name="cod_referencia" id="cod_referencia" value="<?php echo set_value('cod_referencia') ?>"/>
										<span class='red-text'><?php echo form_error('cod_referencia') ?></span>
									</div>
								</div>
								<div class="row">
									<div class="input-field col s12 m6">
										<select class='select' name="id_aves_classes" id="id_aves_classes">
											<option class="disabled" disabled selected="">Selecione um opção</option>
											<?php foreach ($classes as $classe): ?>
												<option <?php echo set_select('id_aves_classes', $classe->id); ?> value="<?php echo $classe->id ?>"><?php echo $classe->nome ?></option>
											<?php endforeach ?>
										</select>
										<label for="id_aves_classes">Classe</label>
										<span class='red-text'><?php echo form_error('id_aves_classes') ?></span>
									</div>
									<div class="input-field col s12 m6">
										<select class='select' name="id_aves_mutacoes" id="id_aves_mutacoes">
											<option class="disabled" disabled selected="">Selecione um opção</option>
											<?php foreach ($mutacoes as $mutacao): ?>
												<option <?php echo set_select('id_aves_mutacoes', $mutacao->id); ?> value="<?php echo $mutacao->id ?>"><?php echo $mutacao->nome ?></option>
											<?php endforeach ?>
										</select>
										<label for="id_aves_mutacoes">Mutação</label>
										<span class='red-text'><?php echo form_error('id_aves_mutacoes') ?></span>
									</div>
								</div>
								<div class="row">
									<div class="input-field col s12 m6">
										<select class='select' name="id_aves_cores" id="id_aves_cores">
											<option class="disabled" disabled selected="">Selecione um opção</option>
											<?php foreach ($cores as $cor): ?>
												<option <?php echo set_select('id_aves_cores', $cor->id); ?> value="<?php echo $cor->id ?>"><?php echo $cor->nome ?></option>
											<?php endforeach ?>
										</select>
										<label for="id_aves_cores">Cor</label>
										<span class='red-text'><?php echo form_error('id_aves_cores') ?></span>
									</div>
									<div class="input-field col s12 m6">
										<select class='select' name="id_tipo_unidade" id="id_tipo_unidade">
											<option class="disabled" disabled selected="">Selecione um opção</option>
											<?php foreach ($unidades as $unidade): ?>
												<option <?php echo set_select('id_tipo_unidade', $unidade->id); ?> value="<?php echo $unidade->id ?>"><?php echo $unidade->nome ?></option>
											<?php endforeach ?>
										</select>
										<label for="id_tipo_unidade">Quantidade</label>
										<span class='red-text'><?php echo form_error('id_tipo_unidade') ?></span>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="input-field col s12 m4">
								<label for="a_anilha">Anilha</label>
								<input type="text" name="a_anilha" id="a_anilha" value="<?php echo set_value('a_anilha') ?>"/>
								<span class='red-text'><?php echo form_error('a_anilha') ?></span>
							</div>
							<div class="input-field col s12 m4">
								<label for="a_anilhado_em">Anilhado em</label>
								<input type="text" class="ano" name="a_anilhado_em" id="a_anilhado_em" value="<?php echo set_value('a_anilhado_em') ?>"/>
								<span class='red-text'><?php echo form_error('a_anilhado_em') ?></span>
							</div>
							<div class="input-field col s12 m4">
								<label for="a_ibama">Ibama</label>
								<input type="text" name="a_ibama" id="a_ibama" value="<?php echo set_value('a_ibama') ?>"/>
								<span class='red-text'><?php echo form_error('a_ibama') ?></span>
							</div>
						</div>

						<div class="row">
							<div class="input-field col s12 m12">
								<label for="a_ibamaobs">Obs. Ibama</label>
								<textarea data-length="255" class="materialize-textarea" name="a_ibamaobs" id='a_ibamaobs'><?php echo set_value('a_ibamaobs') ?></textarea>
								<span class='red-text'><?php echo form_error('a_ibamaobs') ?></span>
							</div>
						</div>

						<div class="row">
							<div class="input-field col s12 m6">
								<select class='select' name="a_sexado" id="a_sexado">
									<option class="disabled" disabled selected>Selecione um opção</option>
									<option <?php echo set_select('a_sexado', 'NAOINFORMADO'); ?> value="NAOINFORMADO">Não Informado</option>
									<option <?php echo set_select('a_sexado', 'MACHO'); ?> value="MACHO">Macho</option>
									<option <?php echo set_select('a_sexado', 'FEMEA'); ?> value="FEMEA">Fêmea</option>
								</select>
								<label for="a_sexado">Sexo</label>
								<span class='red-text'><?php echo form_error('a_sexado') ?></span>
							</div>
							<div class="input-field col s12 m6">
								<select class='select' name="a_manso" id="a_manso">
									<option class="disabled" disabled selected="">Selecione um opção</option>
									<option <?php echo set_select('a_manso', 'NAOINFORMADO'); ?> value="NAOINFORMADO">Não Informado</option>
									<option <?php echo set_select('a_manso', 'SIM'); ?> value="SIM">Sim</option>
									<option <?php echo set_select('a_manso', 'NAO'); ?> value="NAO">Não</option>
								</select>
								<label for="a_manso">Manso</label>
								<span class='red-text'><?php echo form_error('a_manso') ?></span>
							</div>
						</div>

						<div class="row">
							<div class="input-field col s12 m12">
								<label for="frase">Frase</label>
								<input type="text" name="frase" id="frase" value="<?php echo set_value('frase') ?>"/>
								<span class='red-text'><?php echo form_error('frase') ?></span>
							</div>
						</div>

						<div class="row">
							<div class="input-field col s12 m12">
								<label for="descricao">Descrição</label>
								<textarea data-length="200" class="materialize-textarea" name="descricao" id='descricao'><?php echo set_value('descricao') ?></textarea>
								<span class='red-text'><?php echo form_error('descricao') ?></span>
							</div>
						</div>

						<div class="row">
							<div class="input-field col s12 m6">
								<label for="link_externo">Link Externo</label>
								<input type="text" name="link_externo" id="link_externo" value="<?php echo set_value('link_externo') ?>" />
								<span class='red-text'><?php echo form_error('link_externo') ?></span>
							</div>
							<div class="input-field col s12 m6">
								<label for="link_comercial">Link Comercial</label>
								<input type="text" name="link_comercial" id="link_comercial" value="<?php echo set_value('link_comercial') ?>" />
								<span class='red-text'><?php echo form_error('link_comercial') ?></span>
							</div>
						</div>

						<div class="row">
							<div class="col s12 m2 checkbox">
								<p>
									<input <?php echo set_checkbox('a_avenda', 'a_avenda'); ?> value="a_avenda" type="checkbox" id="a_avenda" name="a_avenda" />
									<label for="a_avenda">À Venda</label>
								</p>
								<span class='red-text'><?php echo form_error('a_avenda') ?></span>
							</div>
							<div class="input-field col s12 m3">
								<label for="valor">Valor(R$)</label>
								<input type="text" name="valor" id="valor" value="<?php echo set_value('valor') ?>" />
								<span class='red-text'><?php echo form_error('valor') ?></span>
							</div>
							<div class="col s12 m3 checkbox">
								<p>
									<input <?php echo set_checkbox('valor_mostrar', 'mostrar'); ?> value="mostrar" type="checkbox" id="valor_mostrar" name="valor_mostrar" />
									<label for="valor_mostrar">Mostrar Valor</label>
								</p>
								<span class='red-text'><?php echo form_error('valor_mostrar') ?></span>
							</div>
							<div class="input-field col s12 m4">
								<select class='select' name="entrega" id="entrega">
									<option class="disabled" disabled selected="">Selecione uma opção</option>
									<option <?php echo set_select('entrega', 'ACOMBINAR'); ?> value="ACOMBINAR">A Combinar</option>
									<option <?php echo set_select('entrega', 'SIM'); ?> value="SIM">Sim</option>
									<option <?php echo set_select('entrega', 'NAO'); ?> value="NAO">Não</option>
								</select>
								<label for="entrega">Entrega</label>
								<span class='red-text'><?php echo form_error('entrega') ?></span>
							</div>
						</div>

						<div class="row">
							<div class="input-field col s12 m12">
								<label for="entrega_obs">Obs. Entrega</label>
								<textarea data-length="200" class="materialize-textarea" name="entrega_obs" id='entrega_obs'><?php echo set_value('entrega_obs') ?></textarea>
								<span class='red-text'><?php echo form_error('entrega_obs') ?></span>
							</div>
						</div>

						<div class="row">
							<div class="input-field col s12 m4">
								<select class='select' name="aceita_troca" id="aceita_troca">
									<option class="disabled" disabled selected="">Selecione um opção</option>
									<option <?php echo set_select('aceita_troca', 'ACOMBINAR'); ?> value="ACOMBINAR">A Combinar</option>
									<option <?php echo set_select('aceita_troca', 'SIM'); ?> value="SIM">Sim</option>
									<option <?php echo set_select('aceita_troca', 'NAO'); ?> value="NAO">Não</option>
								</select>
								<label for="aceita_troca">Troca</label>
								<span class='red-text'><?php echo form_error('aceita_troca') ?></span>
							</div>
							<div class="input-field col s12 m8">
								<label for="parcela">Parcela</label>
								<input type="text" name="parcela" id="parcela" value="<?php echo set_value('parcela') ?>" />
								<span class='red-text'><?php echo form_error('parcela') ?></span>
							</div>
						</div>

						<div class="row">
							<div class="input-field col s12 m12">
								<label for="outras_opcoes">Outras Opções</label>
								<textarea data-length="200" class="materialize-textarea" name="outras_opcoes" id='outras_opcoes'><?php echo set_value('outras_opcoes') ?></textarea>
								<span class='red-text'><?php echo form_error('outras_opcoes') ?></span>
							</div>
						</div>

						<div class="row">
							<div class="col s12 m12 checkbox" style="padding-left: 2.5rem;">
								<p>
									<input type="checkbox" id="a_confirmaresponsabilidade" name="a_confirmaresponsabilidade" />
									<label for="a_confirmaresponsabilidade">LI, ENTENDI E CONCORDO COM OS <a class="modal-trigger" href="#termos_responsabilidade">TERMOS DE RESPONSABILIDADE</a></label>
								</p>
								<span class='red-text'><?php echo form_error('a_confirmaresponsabilidade') ?></span>
							</div>
						</div>

						<div class="row">
							<button class="btn  blue right">Incluir</button>
						</div>
					</div>

				</div>

			</form>
		</div>

	</div>
</main>

<!-- Avatar Modal -->
<div class="ip-modal" id="avatarModal">
	<div class="ip-modal-dialog">
		<div class="ip-modal-content">
			<div class="ip-modal-header">
				<a class="ip-close" title="Close">&times;</a>
				<h4 class="ip-modal-title">Alterar Imagem</h4>
			</div>
			<div class="ip-modal-body">
				<div class="btn blue ip-upload">Enviar <input type="file" name="file" class="ip-file"></div>

				<button type="button" class="btn blue lighten-1 ip-edit">Editar</button>
				<button type="button" class="btn red ip-delete">Excluir</button>

				<div class="alert ip-alert"></div>
				<div class="ip-info">Para cortar esta imagem, arraste o mouse na região abaixo e clique em "Salvar Imagem"</div>
				<div class="ip-preview"></div>
				<div class="ip-rotate">
					<button type="button" class="btn ip-rotate-ccw" title="Rotate counter-clockwise"><i class="icon-ccw"></i></button>
					<button type="button" class="btn ip-rotate-cw" title="Rotate clockwise"><i class="icon-cw"></i></button>
				</div>
				<div class="ip-progress">
					<div class="text">Enviando</div>
					<div class="progress progress-striped active"><div class="progress-bar"></div></div>
				</div>
			</div>
			<div class="ip-modal-footer">
				<div class="ip-actions">
					<button type="button" class="btn blue ip-save">Salvar Imagem</button>
					<button type="button" class="btn grey ip-cancel">Cancelar</button>
				</div>
				<button type="button" class="btn red darken-5 ip-close">Fechar</button>
			</div>
		</div>
	</div>
</div>
<!-- end Modal -->

<!-- Termos de Responsabilidade -->
<!-- Modal Structure -->
<div id="termos_responsabilidade" class="modal bottom-sheet">
	<div class="modal-content">
		<h4>Termos de Responsabilidade</h4>
		<p>1. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at nibh eget neque hendrerit vestibulum. Nam mattis et nisl vel vulputate. Suspendisse potenti. Vestibulum luctus sollicitudin nulla, in cursus massa pretium ac. Proin hendrerit convallis dui ut dictum. Duis placerat metus eu leo pretium, eu congue nunc rutrum. Integer ullamcorper est a elit iaculis, et auctor tellus dapibus. Pellentesque placerat ligula augue, nec egestas ligula blandit ultricies. In vehicula viverra feugiat. Duis non semper velit.</p>
		<p>2. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at nibh eget neque hendrerit vestibulum. Nam mattis et nisl vel vulputate. Suspendisse potenti. Vestibulum luctus sollicitudin nulla, in cursus massa pretium ac. Proin hendrerit convallis dui ut dictum. Duis placerat metus eu leo pretium, eu congue nunc rutrum. Integer ullamcorper est a elit iaculis, et auctor tellus dapibus. Pellentesque placerat ligula augue, nec egestas ligula blandit ultricies. In vehicula viverra feugiat. Duis non semper velit.</p>
		<p>3. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at nibh eget neque hendrerit vestibulum. Nam mattis et nisl vel vulputate. Suspendisse potenti. Vestibulum luctus sollicitudin nulla, in cursus massa pretium ac. Proin hendrerit convallis dui ut dictum. Duis placerat metus eu leo pretium, eu congue nunc rutrum. Integer ullamcorper est a elit iaculis, et auctor tellus dapibus. Pellentesque placerat ligula augue, nec egestas ligula blandit ultricies. In vehicula viverra feugiat. Duis non semper velit.</p>
		<p>4. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at nibh eget neque hendrerit vestibulum. Nam mattis et nisl vel vulputate. Suspendisse potenti. Vestibulum luctus sollicitudin nulla, in cursus massa pretium ac. Proin hendrerit convallis dui ut dictum. Duis placerat metus eu leo pretium, eu congue nunc rutrum. Integer ullamcorper est a elit iaculis, et auctor tellus dapibus. Pellentesque placerat ligula augue, nec egestas ligula blandit ultricies. In vehicula viverra feugiat. Duis non semper velit.</p>

	</div>
	<div class="modal-footer">
		<a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat">Fechar</a>
	</div>
</div>
<!-- Termos de Responsabilidade -->


<?php include 'inc/interno-footer.php' ?>
<?php include 'inc/interno-js.php' ?>
<script type="text/javascript" src="/js/select2.full.min.js"></script>
<script type="text/javascript" src="/js/i18n/pt-BR.js"></script>
<script type="text/javascript" src="/js/jquery.mask.min.js"></script>

<!-- CROP -->
<!-- <script src="/assets/js/jquery-1.11.0.min.js"></script> -->
<script src="/assets/js/jquery.Jcrop.min.js"></script>
<script src="/assets/js/jquery.imgpicker.js"></script>
<!-- CROP -->
<script type="text/javascript" src="/js/jquery.mask.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#valor').mask("# ##0.00", {reverse: true});
		Materialize.updateTextFields();
		$('#termos_responsabilidade').modal();
		$('.select').material_select();
		$('textarea').characterCounter();
		$('#avatarModal').imgPicker({
			url: '/server/upload_avatar.php',
			aspectRatio: 1,
			data: {
				uniqid: '<?php echo uniqid("ave_") ?>',
			},
			deleteComplete: function() {

			},
			uploadSuccess: function(image) {
	                // Calculate the default selection for the cropper
	                var select = (image.width > image.height) ?
	                [(image.width-image.height)/2, 0, image.height, image.height] :
	                [0, (image.height-image.width)/2, image.width, image.width];      
	                this.options.setSelect = select;
	            },
	            cropSuccess: function(image) {
	            	var n=Math.floor(Math.random()*11);
	            	var k = Math.floor(Math.random()* 1000000);
	            	var uniqid = String.fromCharCode(n)+k;//gerando caracteres aleatórios para concatenar na imagem... para evitar cache
	            	$('#avatar2').attr('src', '//gravatar.com/avatar/0?d=mm&s=150');
	            	$('#avatar2').attr('src', "/img/"+image.name+"?"+uniqid);
	                // ATRIBUIR PARA CAMPO HIDDEN O NOME DA IMG
	                $("#img_principal").val(image.name);
	                this.modal('hide');
	            }
	        });
	});
	$('.datepicker').pickadate({
		selectDays: false,
		    // selectMonths: true, // Creates a dropdown to control month
		    selectYears: 100,
		    max: true,
		    min: [1900, 1, 1],
		    today: 'Hoje',
		    clear: 'Limpar',
		    close: 'Selecionar',
		    weekdaysLetter: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
		    weekdaysShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
		    weekdaysFull: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		    monthsShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
		    monthsFull: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
		    labelMonthSelect: 'Selecione o Mês',
		    labelYearSelect: 'Selecione o Ano',
		    labelMonthNext: 'Próximo Mês',
		    labelMonthPrev: 'Mês Anterior',
		    format: 'yyyy',
		    formatSubmit: 'yyyy',
		    closeOnSelect: true // Close upon selecting a date,
		});
	</script>
</body>
</html>