<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<title>Aves - SuperBirds</title>
	<?php include 'inc/interno-head.php' ?>
	<style type="text/css">
		.destaque{
			box-shadow: 0px 0px 10px 0px green;
		}
	</style>
</head>
<body class="blue-grey lighten-5">
	<?php include 'inc/interno-menu_lateral.php' ?>
	<?php include 'inc/interno-topo.php' ?>
	<main>
		<div class="container-fluid">

			<div class="card">
				<div class="card-content">
					<div class="left">
						<p class="card-title blue-grey-text">Suas Aves - SuperBirds</p>
						<?php if ($limites->total_anuncios < $limites->anuncios_permitidos): ?>
							<p class="blue-grey-text">Você possui <b><?php echo $limites->anuncios_permitidos-$limites->total_anuncios ?></b> anúncios permitidos.</p>
						<?php else: ?>
							<p class='red-text'>Você não possui mais anúncios permitidos em seu plano. <a href="/solicitacoes">Solicite mais.</a></p>
						<?php endif ?>

						<?php if ($limites->total_anuncios_destaque < $limites->anuncios_destaque_permitidos): ?>
							<p class="blue-grey-text">Você possui <b><?php echo $limites->anuncios_destaque_permitidos - $limites->total_anuncios_destaque ?></b> destaques permitidos.</p>
						<?php else: ?>
							<p class='red-text'>Você não possui destaques permitidos em seu plano. <a href="/solicitacoes">Solicite mais.</a></p>
						<?php endif ?>
					</div>
					<div class="right">
						<a href='/aves/cadastrar' class="btn blue"><i class="fas fa-plus"></i> Incluir</a>
					</div>
					<div class="clearfix"></div>
					<hr>
					<div class="row">
						<?php if ($aves == null): ?>
							<h3 class="center-align blue-grey-text">Nenhuma Ave</h3>
						<?php endif ?>
						<?php foreach ($aves as $ave): ?>
							<div class="col s12 m3 <?php if($ave->destaque == '1') echo 'destaque' ?>">
						      <div class="card sticky-action <?php echo $ave->situacao == 'S' ? 'suspenso' : '' ?> <?php echo $ave->situacao == 'V' ? 'vendido' : '' ?> <?php echo $ave->situacao == 'C' ? 'concluido' : '' ?>">
						        <div class="card-image">
						          <img src="/img/<?php echo empty($ave->img_principal) ? 'pena.jpg' : $ave->img_principal."?".uniqid() ?>">
						        </div>
						        <div class="card-content">
						        	<span title="<?php echo $ave->nome ?>" class="card-title activator grey-text text-darken-4 truncate"><i class="material-icons right">more_vert</i><?php echo $ave->nome ?></span>
						        	<?php if (!empty($ave->classe)): ?>
						        		<p class='truncate'>Classe: <b title="<?php echo $ave->classe ?>"><?php echo $ave->classe ?></b></p>
						        	<?php endif ?>
						        	<?php if (!empty($ave->cor)): ?>
						        		<p class='truncate'>Cor: <b title="<?php echo $ave->cor ?>"><?php echo $ave->cor ?></b></p>
						        	<?php endif ?>
						        	<?php if (!empty($ave->subcor)): ?>
						        		<p class='truncate'>Sub-Cor: <b title="<?php echo $ave->subcor ?>"><?php echo $ave->subcor ?></b></p>
						        	<?php endif ?>
						        </div>
						        <div class="card-action blue-grey-text">
				                  <p><i class="fas fa-thumbs-up"></i> <?php echo $ave->curtidas ?>
				                  <i class="fas fa-eye"></i> <?php echo $ave->contador ?></p>
				                </div>
				                <div class="card-reveal">
				                	<span class="card-title grey-text text-darken-4 truncate" title="<?php echo $ave->nome ?>"><i class="material-icons right">close</i><?php echo $ave->nome ?></span>
				                	<a href="/aves/editar/<?php echo $ave->id ?>" class="btn waves-effect blue btn-block">Alterar</a>
				                	<hr />
				                	<?php if ($ave->destaque == "1"): ?>
				                		<a href="/aves/destacar/<?php echo $ave->id ?>" class="btn waves-effect blue-grey btn-block">Destaque</a>
				                	<?php else: ?>
				                		<a href="/aves/destacar/<?php echo $ave->id ?>" class="btn waves-effect blue btn-block">Destacar</a>
				                	<?php endif ?>
				                	<hr />
				                	<a class='dropdown-button btn btn-block green darken-3' href='#' data-activates='dropdown<?php echo $ave->id ?>'>Situação</a>
				                	<ul id='dropdown<?php echo $ave->id ?>' class='dropdown-content'>
				                		<li class="<?php echo $ave->situacao == "L" ? "blue-grey lighten-5" : "alterar_situacao" ?>"><a data-id="<?php echo $ave->id ?>" data-situacao="L" class='blue-text text-darken-5' href="#!">Ativado</a></li>
				                		<li class="<?php echo $ave->situacao == "S" ? "blue-grey lighten-5" : "alterar_situacao" ?>"><a data-id="<?php echo $ave->id ?>" data-situacao="S" class='red-text text-darken-5' href="#!">Suspenso</a></li>
				                		<li class="<?php echo $ave->situacao == "V" ? "blue-grey lighten-5" : "alterar_situacao" ?>"><a data-id="<?php echo $ave->id ?>" data-situacao="V" class='blue-grey-text' href="#!">Vendido</a></li>
				                		<li class="<?php echo $ave->situacao == "C" ? "blue-grey lighten-5" : "alterar_situacao" ?>"><a data-id="<?php echo $ave->id ?>" data-situacao="C" class='' href="#!">Concluído</a></li>
				                	</ul>
				                </div>
						      </div>
						    </div>
						<?php endforeach ?>
					</div>
					<div class="right-align">
						<?php echo $paginacao ?>
					</div>
				</div>
			</div>

		</div>
	</main>

	<?php include 'inc/interno-footer.php' ?>
	<?php include 'inc/interno-js.php' ?>

	<script type="text/javascript">
		$(document).ready(function(){
			$('.dropdown-button').dropdown({
				inDuration: 300,
				outDuration: 225,
				constrainWidth: false,
				hover: false,
				gutter: 0,
				belowOrigin: true,
				alignment: 'left',
				stopPropagation: false
			});

			$(".alterar_situacao").click(function(e){
				e.preventDefault();
				var situacao = $(this).find("a").attr("data-situacao");
				var id = $(this).find("a").attr("data-id");
				confirmacao = true;
				if(situacao == "V" || situacao == "C"){
					confirmacao = confirm("Tem certeza? Esta ação não pode ser desfeita.");
				}

				if(confirmacao)
					$.post("/aves/alterar_situacao", {
						situacao: situacao,
						id: id
					}, function(result){
						location.reload();
					});
			});
		});
	</script>
</body>
</html>