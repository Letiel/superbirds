<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<title>Cadastro - SuperBirds</title>
	<?php include 'inc/site-head.php'; ?>
	<link rel="stylesheet" type="text/css" href="/css/select2.css">
	<style type="text/css">
		html, body, main, .container-fluid, .row, .valign-wrapper{
			height: 100%;
		}
		.input-field input:focus + label {
		   color: #2196F3 !important;
		 }
		 /* label underline focus color */
		 .row .input-field input:focus {
		   border-bottom: 1px solid #2196F3 !important;
		   box-shadow: 0 1px 0 0 #2196F3 !important
		 }
	</style>
</head>
<body class="blue-grey lighten-5">
<?php include 'inc/site-topo.php' ?>
	<main>
		<div class="container-fluid">
			<div class="row">
				<div class=''>
					<div class="col s12">
						<div class="card">
							<form method="post">
								<div class="card-content">
									<span class="card-title">Cadastre-se no SuperBirds</span>
									<hr>
									<div class="row">
										<div class="input-field col s12 m3">
											<label for="Nome">Nome</label>
											<input type="text" name="nome" id="nome" value="<?php echo set_value('nome') ?>"/>
											<span class='red-text'><?php echo form_error('nome') ?></span>
										</div>
										<div class="col s6 m2" style="margin-top: 10px">
											<p>
												<input class="tipo_documento" name="tipo_documento" type="radio" value="cpf" id="cpf" checked />
												<label for="cpf">CPF</label>
										    </p>
										    <p>
										    	<input class="tipo_documento" name="tipo_documento" type="radio" value="cnpj" id="cnpj" />
										    	<label for="cnpj">CNPJ</label>
										    </p>
										</div>
										<div class="input-field col s6 m2">
											<label for="cpfcnpj">CPF</label>
											<input type="text" name="cpfcnpj" id="cpfcnpj" value="<?php echo set_value('cpfcnpj') ?>"/>
											<span class='red-text'><?php echo form_error('cpfcnpj') ?></span>
										</div>
										<div class="input-field col s12 m2">
											<label for="data_nascimento">Data Nasc.</label>
											<input type="text" name="data_nascimento" id="data_nascimento" value="<?php echo set_value('data_nascimento') ?>" class='datepicker'/>
											<span class='red-text'><?php echo form_error('data_nascimento') ?></span>
										</div>
										<div class="input-field col s12 m3">
											<label for="email">email</label>
											<input type="text" name="email" id="email" value="<?php echo set_value('email') ?>"/>
											<span class='red-text'><?php echo form_error('email') ?></span>
										</div>
									</div>
									<div class="row">
										<div class="input-field col s12 m6">
											<select name="id_cidade" id="cidade">
												<?php if(!empty(set_value("id_cidade"))){ ?>
													<option value="<?php echo set_value("id_cidade") ?>"><?php echo $this->session->flashdata("cidade") ?></option>
												<?php } ?>
											</select>
											<span class='red-text'><?php echo form_error('id_cidade') ?></span>
										</div>
										<div class="input-field col s12 m6">
											<label for="endereco">Endereço</label>
											<input type="text" name="endereco" id="endereco" value="<?php echo set_value('endereco') ?>"/>
											<span class='red-text'><?php echo form_error('endereco') ?></span>
										</div>
									</div>
									<div class="row">
										<div class="input-field col s12 m3">
											<label for="bairro">Bairro</label>
											<input type="text" name="bairro" id="bairro" value="<?php echo set_value('bairro') ?>"/>
											<span class='red-text'><?php echo form_error('bairro') ?></span>
										</div>
										<div class="input-field col s12 m3">
											<label for="complemento">Complemento</label>
											<input type="text" name="complemento" id="complemento" value="<?php echo set_value('complemento') ?>"/>
											<span class='red-text'><?php echo form_error('complemento') ?></span>
										</div>
										<div class="input-field col s12 m2">
											<label for="cep">CEP</label>
											<input type="text" name="cep" id="cep" value="<?php echo set_value('cep') ?>"/>
											<span class='red-text'><?php echo form_error('cep') ?></span>
										</div>
										<div class="input-field col s12 m2">
											<label for="fone">Telefone</label>
											<input type="text" name="fone" id="fone" value="<?php echo set_value('fone') ?>"/>
											<span class='red-text'><?php echo form_error('fone') ?></span>
										</div>
										<div class="input-field col s12 m2">
											<label for="cel">Celular</label>
											<input type="text" name="cel" id="cel" value="<?php echo set_value('cel') ?>"/>
											<span class='red-text'><?php echo form_error('cel') ?></span>
										</div>
									</div>
									<div class="row">
										<div class="input-field col s12 m6">
											<select name="ramo_principal" id="ramo_principal">
												<option value="C">Criador</option>
												<option value="P">Produtos</option>
												<option value="S">Serviços</option>
											</select>
											<label for="ramo_principal">Ramo Principal</label>
											<span class='red-text'><?php echo form_error('ramo_principal') ?></span>
										</div>

										<div class="input-field col s12 m6">
											<select name="id_plano" id="plano">
												<?php foreach ($planos as $plano): ?>
													<option value="<?php echo $plano->id ?>" <?php echo set_select('id_plano', $plano->id) ?>><?php echo $plano->nome ?></option>
												<?php endforeach ?>
											</select>
											<label for="plano">Plano</label>
											<span class='red-text'><?php echo form_error('id_plano') ?></span>
										</div>
									</div>
									<div class="row">
										<span class="card-title">Dados de Login</span>
										<div class="input-field col s12 m4">
											<label for="login">Login</label>
											<input type="text" name="superbirds_usuario" id="login" value="<?php echo set_value('superbirds_usuario') ?>"/>
											<span class='red-text'><?php echo form_error('superbirds_usuario') ?></span>
										</div>
										<div class="input-field col s12 m4">
											<label for="senha">Senha</label>
											<input type="password" name="superbirds_senha" id="senha" value="<?php echo set_value('superbirds_senha') ?>"/>
											<span class='red-text'><?php echo form_error('superbirds_senha') ?></span>
										</div>
										<div class="input-field col s12 m4">
											<label for="confirma_senha">Confirmação</label>
											<input type="password" name="confirma_senha" id="confirma_senha" value="<?php echo set_value('confirma_senha') ?>"/>
											<span class='red-text'><?php echo form_error('confirma_senha') ?></span>
										</div>
									</div>
								</div>
								<div class="card-action">
									<div class="right-align">
										<button class="btn blue">Cadastrar-se</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
	<?php include 'inc/site-footer.php' ?>
	<script type="text/javascript" src="/js/jquery.min.js"></script>
	<script type="text/javascript" src="/js/materialize.min.js"></script>
	<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
	<script type="text/javascript" src="/js/select2.full.min.js"></script>
	<script type="text/javascript" src="/js/i18n/pt-BR.js"></script>
	<script type="text/javascript" src="/js/jquery.mask.min.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$(".button-collapse").sideNav();
			Materialize.updateTextFields();
			Materialize.toast('Após o cadastro, você pode solicitar outro plano.', 40000, 'blue');
			Materialize.toast('Assim você poderá cadastrar mais anúncios', 50000, 'blue');
			$('#plano').material_select();
			$('#ramo_principal').material_select();
			$('#cidade').select2({
				language: "pt-BR",
				placeholder: "Cidade",
				minimumInputLength: 3,
				ajax: {
				    url: '/select/cidades',
				    dataType: 'json',
				    delay: 250
				    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
			  	}
			});
		});
		$("#cpfcnpj").mask("999.999.999-99");
		$("#cep").mask("99999-999");
		$("#fone").mask("(99)99999 9999");
		$("#cel").mask("(99)99999 9999");
		$(".tipo_documento").change(function(){
			tipo = $(this).val();
			if(tipo == "cpf"){
				$("#cpfcnpj").mask("999.999.999-99");
				$("#cpfcnpj").parent().find("label").html("CPF");
			}else if(tipo == "cnpj"){
				$("#cpfcnpj").mask("99.999.999/9999-99");
				$("#cpfcnpj").parent().find("label").html("CNPJ");
			}
			Materialize.updateTextFields();
		});
		$('.datepicker').pickadate({
		    selectMonths: true, // Creates a dropdown to control month
		    selectYears: 100,
		    max: true,
		    min: [1900, 1, 1],
		    today: 'Hoje',
		    clear: 'Limpar',
		    close: 'Selecionar',
		    weekdaysLetter: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
		    weekdaysShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
		    weekdaysFull: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		    monthsShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
		    monthsFull: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
		    labelMonthSelect: 'Selecione o Mês',
	        labelYearSelect: 'Selecione o Ano',
	        labelMonthNext: 'Próximo Mês',
            labelMonthPrev: 'Mês Anterior',
            format: 'dd/mm/yyyy',
            formatSubmit: 'yyyy-mm-dd',
		    closeOnSelect: true // Close upon selecting a date,
		  });
		// var ps2 = new PerfectScrollbar('ul');
		// $(".dropdown-button").dropdown();
		// $(".button-collapse").sideNav({
		// 	draggable: true
		// });
	</script>
</body>
</html>