<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<title>Eventos - SuperBirds</title>
	<?php include 'inc/interno-head.php' ?>
</head>
<body class="blue-grey lighten-5">
	<?php include 'inc/interno-menu_lateral.php' ?>
	<?php include 'inc/interno-topo.php' ?>
	<main>
		<div class="container-fluid">

			<div class="card">
				<div class="card-content">
					<div class="left">
						<p class="card-title blue-grey-text">Seus Eventos - SuperBirds</p>
					</div>
					<div class="right">
						<a href='/solicitacoes/eventos' class="btn blue"><i class="fas fa-plus"></i> Incluir</a>
					</div>
					<div class="clearfix"></div>
					<hr>
					<div class="row">
						<?php if ($eventos == NULL): ?>
							<h3 class="center-align blue-grey-text">Nenhum Evento</h3>
						<?php endif ?>
						<?php foreach ($eventos as $evento): ?>
							<div class="col s12 m3">
						      <div class="card sticky-action <?php echo $evento->situacao == 'S' ? 'suspenso' : '' ?> <?php echo $evento->situacao == 'V' ? 'vendido' : '' ?> <?php echo $evento->situacao == 'C' ? 'concluido' : '' ?>">
						        <div class="card-content">
						        	<span title="<?php echo $evento->titulo ?>" class="card-title activator grey-text text-darken-4 truncate"><i class="material-icons right">more_vert</i><?php echo $evento->titulo ?></span>						        	
						        </div>
						        <div class="card-action blue-grey-text">
				                  <p><i class="fas fa-thumbs-up"></i> <?php echo $evento->curtidas ?>
				                  <i class="fas fa-eye"></i> <?php echo $evento->contador ?></p>
				                </div>
				                <div class="card-reveal">
				                	<span class="card-title grey-text text-darken-4 truncate" title="<?php echo $evento->titulo ?>"><i class="material-icons right">close</i><?php echo $evento->titulo ?></span>
				                	<hr />
				                	<a class='dropdown-button btn btn-block green darken-3' href='#' data-activates='dropdown<?php echo $evento->id ?>'>Situação</a>
				                	<ul id='dropdown<?php echo $evento->id ?>' class='dropdown-content'>
				                		<li class="<?php echo $evento->situacao == "L" ? "blue-grey lighten-5" : "alterar_situacao" ?>"><a data-id="<?php echo $evento->id ?>" data-situacao="L" class='blue-text text-darken-5' href="#!">Ativado</a></li>
				                		<li class="<?php echo $evento->situacao == "S" ? "blue-grey lighten-5" : "alterar_situacao" ?>"><a data-id="<?php echo $evento->id ?>" data-situacao="S" class='red-text text-darken-5' href="#!">Suspenso</a></li>
				                		<li class="<?php echo $evento->situacao == "E" ? "blue-grey lighten-5" : "alterar_situacao" ?>"><a data-id="<?php echo $evento->id ?>" data-situacao="E" class='blue-grey-text' href="#!">Excluir</a></li>
				                	</ul>
				                </div>
						      </div>
						    </div>
						<?php endforeach ?>
					</div>
					<div class="right-align">
						<?php echo $paginacao ?>
					</div>
				</div>
			</div>

		</div>
	</main>

	<?php include 'inc/interno-footer.php' ?>
	<?php include 'inc/interno-js.php' ?>

	<script type="text/javascript">
		$(document).ready(function(){
			$('.dropdown-button').dropdown({
				inDuration: 300,
				outDuration: 225,
				constrainWidth: false,
				hover: false,
				gutter: 0,
				belowOrigin: true,
				alignment: 'left',
				stopPropagation: false
			});

			$(".alterar_situacao").click(function(e){
				e.preventDefault();
				var situacao = $(this).find("a").attr("data-situacao");
				var id = $(this).find("a").attr("data-id");
				confirmacao = true;
				if(situacao == "V" || situacao == "C"){
					confirmacao = confirm("Tem certeza? Esta ação não pode ser desfeita.");
				}

				if(confirmacao)
					$.post("/eventos/alterar_situacao", {
						situacao: situacao,
						id: id
					}, function(result){
						location.reload();
					});
			});
		});
	</script>
</body>
</html>