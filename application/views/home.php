<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <title>SuperBirds</title>
  <?php include 'inc/site-head.php'; ?>
  <link rel="stylesheet" type="text/css" href="/js/slick.css">
  <link rel="stylesheet" type="text/css" href="/js/slick-theme.css">
</head>
<body class="">

  <?php include 'inc/site-topo.php' ?>
  <main>


    <?php $background = 0; ?>

    <?php include 'inc/site-banner-superior.php' ?>
    
    <div class="row <?php (!empty($proximos_eventos)) ? "" : "grey lighten-5" ?>" style="margin-bottom: 0px;">
      <div class="col s12">
        <h3 class="flow-text center-align" style="font-size: 2em; margin: 30px 0"> 
          <i class="fas  fa-bullhorn" style="margin-right: 13px;"></i>
          <?php echo $total_anuncios ?> anúncios ativos no site
        </h3>
      </div>
    </div>

    <?php if (!empty($proximos_eventos)): ?>
      <div class="row grey lighten-5" style="    padding: 30px 30px 96px 30px;">
        <h1 class="center-align flow-text"><a href="/">Próximos Eventos</a></h1>
        <?php foreach ($proximos_eventos as $key => $evento): ?>
          <a href="/ver/evento/<?php echo $evento->id ?>">
            <div class="col xl4 l4 m4 s12">
              <div><i class="far fa-5x fa-calendar-alt left grey-text text-darken-2" style="margin-right: 25px;"></i></div>
              <h5 style="margin-top: 3px;" class="truncate grey-text text-darken-2"><?php echo $evento->titulo ?></h5>
              <h4 class="grey-text text-darken-2" style="margin-top: 0;"><?php echo date("d/m/Y", strtotime($evento->data_evento)) ?></h4>
            </div>
          </a>
        <?php endforeach ?>
      </div>
    <?php endif ?>

    <?php if (!empty($aves_destaque)): ?>
      <?php $background++ ?>

      <div class="row <?php echo !($background % 2) ? "grey lighten-5" : ""; ?>" style="padding: 20px 0;">
        <h1 class="center-align flow-text"><a href="/">Aves em Destaque</a></h1>
        <section class="slick-slider aves-destaque">
          <?php foreach ($aves_destaque as $ave): ?>      
            <div>
              <!-- <a href="/ver/ave/<?php echo $ave->id ?>"> -->

              <div class="card horizontal small">
                <div class="card-image">
                  <img src="<?php echo empty(!$ave->img_principal) ? '/img/'.$ave->img_principal : '/img/pena.jpg' ?>" title="<?php echo $ave->nome ?>" alt="<?php echo $ave->nome ?>">
                  <span class="card-title">
                    <span class="left">

                      <i class="fas fa-eye "></i>   <?php echo $ave->contador ?>
                    </span>
                    <span class="right">

                      <i class="far fa-thumbs-up "></i>   <?php echo $ave->curtidas ?>
                    </span>
                  </span>
                </div>
                <div class="card-stacked">
                  <div class="card-content">
                    <span class="card-title truncate"><?php echo $ave->nome ?></span>                  
                    <p class='truncate'>Criador: <a href="/criadores/ver/<?php echo $ave->id_criador ?>"><?php echo $ave->criador ?></a></p>
                    <p class='truncate'>Classe: <?php echo $ave->classe ?></p>
                    <p class="truncate">Mutação: <?php echo $ave->mutacao ?></p>
                    <a class="green-text text-darken-4 link" href="/ver/ave/<?php echo $ave->id ?>">Visitar Ave <i class="material-icons right">keyboard_arrow_right</i></a>  
                  </div>
                </div>
              </div>

              <!-- </a> -->
            </div>
          <?php endforeach ?>

        </section>
      </div>

    <?php endif ?>

    <?php if (!empty($aves_famosas)): ?>
      <?php $background++ ?>

      <div class="row <?php echo !($background % 2) ? "grey lighten-5" : ""; ?>" style="padding: 20px 0;">
        <h1 class="center-align flow-text"><a href="/">Aves Famosas</a></h1>
        <section class="slick-slider aves-destaque">
          <?php foreach ($aves_famosas as $ave): ?>      
            <div>
              <!-- <a href="/ver/ave/<?php echo $ave->id ?>"> -->

              <div class="card horizontal small">
                <div class="card-image">
                  <img src="<?php echo empty(!$ave->img_principal) ? '/img/'.$ave->img_principal : '/img/pena.jpg' ?>" title="<?php echo $ave->nome ?>" alt="<?php echo $ave->nome ?>">
                  <span class="card-title">
                    <span class="left">

                      <i class="fas fa-eye "></i>   <?php echo $ave->contador ?>
                    </span>
                    <span class="right">

                      <i class="far fa-thumbs-up "></i>   <?php echo $ave->curtidas ?>
                    </span>
                  </span>
                </div>
                <div class="card-stacked">
                  <div class="card-content">
                    <span class="card-title truncate"><?php echo $ave->nome ?></span>                  
                    <p class='truncate'>Criador: <a href="/criadores/ver/<?php echo $ave->id_criador ?>"><?php echo $ave->criador ?></a></p>
                    <p class='truncate'>Classe: <?php echo $ave->classe ?></p>
                    <p class="truncate">Mutação: <?php echo $ave->mutacao ?></p>
                    <a class="green-text text-darken-4 link" href="/ver/ave/<?php echo $ave->id ?>">Visitar Ave <i class="material-icons right">keyboard_arrow_right</i></a>  
                  </div>
                </div>
              </div>

              <!-- </a> -->
            </div>
          <?php endforeach ?>

        </section>
      </div>

    <?php endif ?> 

    <?php if (!empty($aves_curtidas)): ?>
      <?php $background++ ?>

      <div class="row <?php echo !($background % 2) ? "grey lighten-5" : ""; ?>" style="padding: 20px 0;">
        <h1 class="center-align flow-text"><a href="/">Aves Mais Curtidas</a></h1>
        <section class="slick-slider aves-destaque">
          <?php foreach ($aves_curtidas as $ave): ?>      
            <div>
              <!-- <a href="/ver/ave/<?php echo $ave->id ?>"> -->

              <div class="card horizontal small">
                <div class="card-image">
                  <img src="<?php echo empty(!$ave->img_principal) ? '/img/'.$ave->img_principal : '/img/pena.jpg' ?>" title="<?php echo $ave->nome ?>" alt="<?php echo $ave->nome ?>">
                  <span class="card-title">
                    <span class="left">

                      <i class="fas fa-eye "></i>   <?php echo $ave->contador ?>
                    </span>
                    <span class="right">

                      <i class="far fa-thumbs-up "></i>   <?php echo $ave->curtidas ?>
                    </span>
                  </span>
                </div>
                <div class="card-stacked">
                  <div class="card-content">
                    <span class="card-title truncate"><?php echo $ave->nome ?></span>                  
                    <p class='truncate'>Criador: <a href="/criadores/ver/<?php echo $ave->id_criador ?>"><?php echo $ave->criador ?></a></p>
                    <p class='truncate'>Classe: <?php echo $ave->classe ?></p>
                    <p class="truncate">Mutação: <?php echo $ave->mutacao ?></p>
                    <a class="green-text text-darken-4 link" href="/ver/ave/<?php echo $ave->id ?>">Visitar Ave <i class="material-icons right">keyboard_arrow_right</i></a>  
                  </div>
                </div>
              </div>

              <!-- </a> -->
            </div>
          <?php endforeach ?>

        </section>
      </div>

    <?php endif ?> 

  </main>

  <?php include 'inc/site-footer.php' ?>

  <script type="text/javascript" src="/js/jquery.min.js"></script>
  <script type="text/javascript" src="/js/materialize.min.js"></script>
  <script src="/js/perfect-scrollbar.min.js"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.12/css/all.css" integrity="sha384-G0fIWCsCzJIMAVNQPfjH08cyYaUtMwjJwqiRKxxE/rx96Uroj1BtIQ6MLJuheaO9" crossorigin="anonymous">
  <script src="/js/slick.min.js" type="text/javascript" charset="utf-8"></script>
  <script type="text/javascript">

   $(function(){
    $(".button-collapse").sideNav();
    $(window).scroll(function() {
     if($(document).scrollTop() > 40) {
      $( "#top-bird" ).css("height", "0");
      $( "#top-bird" ).css("padding", "0");
    }else{
      $( "#top-bird" ).css("height", "100px");
      $( "#top-bird" ).css("padding", "10px");
    }
    if($(document).scrollTop() < 40){
    }
  });

    $('.slider').slider({
      height: 400, 
    });

    $('.slick-slider').slick({
      infinite: true,
      slidesToShow: 2,
      slidesToScroll: 2,
      autoplay: true,
      autoplaySpeed: 3000,
      responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },{
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
    ]
  });
  });
</script>
<?php include("inc/site-js.php") ?>
</body>
</html>
