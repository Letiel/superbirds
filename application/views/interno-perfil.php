<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<title>Perfil - SuperBirds</title>
	<?php include 'inc/interno-head.php' ?>
	<link href="/css/select2.css" rel="stylesheet">

	<!-- CROP -->
	<link rel="stylesheet" href="/assets/css/imgpicker.css">
	<!-- CROP -->

	<style type="text/css">
		.header h2{
			display: none;
		}
		<?php if (empty($usuario->img_banner)): ?>
			.header h2{
				display: inherit !important;
			}
		<?php endif ?>
	</style>
</head>
<body class="blue-grey lighten-5">
	<?php include 'inc/interno-menu_lateral.php' ?>
	<?php include 'inc/interno-topo.php' ?>
	
	<main>
		<div class="container-fluid">

			<div class="card">
				<form method="post">
					<div class="card-content">
						<span class="card-title blue-grey-text"><?php echo $usuario->nome ?> - Perfil</span>
						<hr>
						<div class="row">
							<div class="input-field col s12 m3">
								<img src="<?php echo ($usuario->img_principal ? '/img/'.$usuario->img_principal.'?'.uniqid() : '//gravatar.com/avatar/0?d=mm&s=150') ?>" alt="Avatar" class="image" style="width:100%" id="avatar2" >

								<button type="button" class="btn blue btn-block blue-grey" data-ip-modal="#avatarModal">Imagem</button>
							</div>
							<div class="input-field col s12 m3">
								<label for="Nome">Nome</label>
								<input type="text" name="nome" id="nome" value="<?php echo $usuario->nome ?>"/>
								<span class='red-text'><?php echo form_error('nome') ?></span>
							</div>
							<div class="col s6 m2" style="margin-top: 10px">
								<p>
									<input class="tipo_documento" name="tipo_documento" type="radio" value="cpf" id="cpf" <?php echo $usuario->tipo_documento == "CPF" ? "checked" : "" ?> />
									<label for="cpf">CPF</label>
							    </p>
							    <p>
							    	<input class="tipo_documento" name="tipo_documento" type="radio" value="cnpj" id="cnpj" <?php echo $usuario->tipo_documento == "CNPJ" ? "checked" : "" ?> />
							    	<label for="cnpj">CNPJ</label>
							    </p>
							</div>
							<div class="input-field col s6 m2">
								<label for="cpfcnpj"><?php echo strtoupper($usuario->tipo_documento) ?></label>
								<input type="text" name="cpfcnpj" id="cpfcnpj" value="<?php echo $usuario->cpfcnpj ?>"/>
								<span class='red-text'><?php echo form_error('cpfcnpj') ?></span>
							</div>
							<div class="input-field col s12 m2">
								<label for="data_nascimento">Data Nasc.</label>
								<input type="text" name="data_nascimento" id="data_nascimento" value="<?php echo $usuario->data_nascimento != '0000-00-00' ? date('d/m/Y', strtotime($usuario->data_nascimento)) : '' ?>" class='datepicker'/>
								<span class='red-text'><?php echo form_error('data_nascimento') ?></span>
							</div>
							<div class="input-field col s12 m3">
								<label for="email">email</label>
								<input type="text" name="email" id="email" value="<?php echo $usuario->email ?>"/>
								<span class='red-text'><?php echo form_error('email') ?></span>
							</div>
							<div class="input-field col s12 m6" style="margin-bottom: 1.45rem;">
								<select name="id_cidade" id="cidade">
									<option selected value="<?php echo $usuario->id_cidade ?>"><?php echo $usuario->nome_cidade." - ".$usuario->uf ?></option>
								</select>
								<span class='red-text'><?php echo form_error('id_cidade') ?></span>
							</div>
							<div class="input-field col s12 m6">
								<label for="endereco">Endereço</label>
								<input type="text" name="endereco" id="endereco" value="<?php echo $usuario->endereco ?>"/>
								<span class='red-text'><?php echo form_error('endereco') ?></span>
							</div>
							<div class="input-field col s12 m3">
								<label for="bairro">Bairro</label>
								<input type="text" name="bairro" id="bairro" value="<?php echo $usuario->bairro ?>"/>
								<span class='red-text'><?php echo form_error('bairro') ?></span>
							</div>
							<div class="input-field col s12 m3">
								<label for="complemento">Complemento</label>
								<input type="text" name="complemento" id="complemento" value="<?php echo $usuario->complemento ?>"/>
								<span class='red-text'><?php echo form_error('complemento') ?></span>
							</div>
							<div class="input-field col s12 m2">
								<label for="cep">CEP</label>
								<input type="text" name="cep" id="cep" value="<?php echo $usuario->cep ?>"/>
								<span class='red-text'><?php echo form_error('cep') ?></span>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s12 m2">
								<label for="fone">Telefone</label>
								<input type="text" name="fone" id="fone" value="<?php echo $usuario->fone ?>"/>
								<span class='red-text'><?php echo form_error('fone') ?></span>
							</div>
							<div class="input-field col s12 m2">
								<label for="cel">Celular</label>
								<input type="text" name="cel" id="cel" value="<?php echo $usuario->cel ?>"/>
								<span class='red-text'><?php echo form_error('cel') ?></span>
							</div>
							<div class="input-field col s12 m2">
								<label for="whats">WhatsApp</label>
								<input type="text" name="whats" id="whats" value="<?php echo $usuario->whats ?>"/>
								<span class='red-text'><?php echo form_error('whats') ?></span>
							</div>
							<div class="input-field col s12 m6">
								<select name="ramo_principal" id="ramo_principal">
									<option <?php echo $usuario->ramo_principal == "C" ? "selected" : "" ?> value="C">Criador</option>
									<option <?php echo $usuario->ramo_principal == "P" ? "selected" : "" ?> value="P">Produtos</option>
									<option <?php echo $usuario->ramo_principal == "S" ? "selected" : "" ?> value="S">Serviços</option>
								</select>
								<label for="ramo_principal">Ramo Principal</label>
								<span class='red-text'><?php echo form_error('ramo_principal') ?></span>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s12">
								<textarea class="materialize-textarea" id="anuncios_principais" name="anuncios_principais" data-length="100" maxlength="100"><?php echo $usuario->anuncios_principais ?></textarea>
								<label for="anuncios_principais">Descreva em poucas palavras seus principais anúncios</label>
								<span class='red-text'><?php echo form_error('anuncios_principais') ?></span>
							</div>
							<div class="col s12 m3 checkbox">
								<p>
									<input <?php echo $usuario->mostra_cpfcnpj == true ? "checked" : "" ?> value="mostra_cpfcnpj" type="checkbox" id="mostra_cpfcnpj" name="mostra_cpfcnpj" />
									<label for="mostra_cpfcnpj">Mostrar CPF/CNPJ</label>
								</p>
								<span class='red-text'><?php echo form_error('mostra_cpfcnpj') ?></span>
							</div>
							<?php if ($usuario->tipo == "CAG"): ?>
								<div class="col s12 m3 checkbox">
									<p>
										<input <?php echo $usuario->mostra_razaosocial == true ? "checked" : "" ?> value="mostra_razaosocial" type="checkbox" id="mostra_razaosocial" name="mostra_razaosocial" />
										<label for="mostra_razaosocial">Mostrar Razão Social</label>
									</p>
									<span class='red-text'><?php echo form_error('mostra_razaosocial') ?></span>
								</div>
							<?php endif ?>
							<div class="col s12 m3 checkbox">
								<p>
									<input <?php echo $usuario->mostra_endereco == true ? "checked" : "" ?> value="mostra_endereco" type="checkbox" id="mostra_endereco" name="mostra_endereco" />
									<label for="mostra_endereco">Mostrar Endereço</label>
								</p>
								<span class='red-text'><?php echo form_error('mostra_endereco') ?></span>
							</div>
							<div class="col s12 m3 checkbox">
								<p>
									<input <?php echo $usuario->mostra_fone == true ? "checked" : "" ?> value="mostra_fone" type="checkbox" id="mostra_fone" name="mostra_fone" />
									<label for="mostra_fone">Mostrar Telefone</label>
								</p>
								<span class='red-text'><?php echo form_error('mostra_fone') ?></span>
							</div>
							<div class="col s12 m3 checkbox">
								<p>
									<input <?php echo $usuario->mostra_cel == true ? "checked" : "" ?> value="mostra_cel" type="checkbox" id="mostra_cel" name="mostra_cel" />
									<label for="mostra_cel">Mostrar Celular</label>
								</p>
								<span class='red-text'><?php echo form_error('mostra_cel') ?></span>
							</div>
							<div class="col s12 m3 checkbox">
								<p>
									<input <?php echo $usuario->mostra_whats == true ? "checked" : "" ?> value="mostra_whats" type="checkbox" id="mostra_whats" name="mostra_whats" />
									<label for="mostra_whats">Mostrar Whats</label>
								</p>
								<span class='red-text'><?php echo form_error('mostra_whats') ?></span>
							</div>
							<div class="col s12 m3 checkbox">
								<p>
									<input <?php echo $usuario->usu_mostra_listaclubes == true ? "checked" : "" ?> value="usu_mostra_listaclubes" type="checkbox" id="usu_mostra_listaclubes" name="usu_mostra_listaclubes" />
									<label for="usu_mostra_listaclubes">Mostrar Lista de Clubes</label>
								</p>
								<span class='red-text'><?php echo form_error('usu_mostra_listaclubes') ?></span>
							</div>
						</div>
					</div>
					<div class="card-action">
						<div class="right-align">
							<button class="btn blue">Atualizar</button>
						</div>
					</div>
				</form>
			</div>

			<div class="card">
				<div class="card-content">
					<span class="card-title blue-grey-text">Banner Destaque</span>
					<hr>
					<div class="header" >
						<div class="center-align">
							<h2 class="blue-grey-text">Banner Destaque</h2>
							<img class="responsive-img" src="<?= !empty($usuario->img_banner) ? '/img/'.$usuario->img_banner : '' ?>" />
						</div>
						<button type="button" class="btn btn-block blue edit-header" data-ip-modal="#headerModal" title="Alterar Banner">Alterar</button>
						<button class="btn btn-block red remover_banner">Remover</button>
					</div>
				</div>
			</div>
		</div>
	</main>

	<!-- Avatar Modal -->
	<div class="ip-modal" id="avatarModal">
	    <div class="ip-modal-dialog">
	        <div class="ip-modal-content">
	            <div class="ip-modal-header">
	                <a class="ip-close" title="Close">&times;</a>
	                <h4 class="ip-modal-title">Alterar Imagem</h4>
	            </div>
	            <div class="ip-modal-body">
	                <div class="btn blue ip-upload">Enviar <input type="file" name="file" class="ip-file"></div>
	                
	                <button type="button" class="btn blue lighten-1 ip-edit">Editar</button>
	                <button type="button" class="btn red ip-delete">Excluir</button>

	                <div class="alert ip-alert"></div>
	                <div class="ip-info">Para cortar esta imagem, arraste o mouse na região abaixo e clique em "Salvar Imagem"</div>
	                <div class="ip-preview"></div>
	                <div class="ip-rotate">
	                    <button type="button" class="btn ip-rotate-ccw" title="Rotate counter-clockwise"><i class="icon-ccw"></i></button>
	                    <button type="button" class="btn ip-rotate-cw" title="Rotate clockwise"><i class="icon-cw"></i></button>
	                </div>
	                <div class="ip-progress">
	                    <div class="text">Enviando</div>
	                    <div class="progress progress-striped active"><div class="progress-bar"></div></div>
	                </div>
	            </div>
	            <div class="ip-modal-footer">
	                <div class="ip-actions">
	                    <button type="button" class="btn blue ip-save">Salvar Imagem</button>
	                    <button type="button" class="btn grey ip-cancel">Cancelar</button>
	                </div>
	                <button type="button" class="btn red darken-5 ip-close">Fechar</button>
	            </div>
	        </div>
	    </div>
	</div>
	<!-- end Modal -->

	<!-- Header Modal -->
	<div class="ip-modal" id="headerModal">
	    <div class="ip-modal-dialog">
	        <div class="ip-modal-content">
	            <div class="ip-modal-header">
	                <a class="ip-close" title="Close">&times;</a>
	                <h4 class="ip-modal-title">Alterar Banner</h4>
	            </div>
	            <div class="ip-modal-body">
	                <div class="btn btn-primary ip-upload">Enviar <input type="file" name="file" class="ip-file"></div>
	                <!-- <button type="button" class="btn btn-primary ip-webcam">Webcam</button> -->
	                <button type="button" class="btn btn-info ip-edit">Editar</button>

	                <div class="alert ip-alert"></div>
	                <div class="ip-info">Para cortar esta imagem, arraste o mouse na região abaixo e clique em "Salvar Imagem"</div>
	                <div class="ip-preview"></div>
	                <div class="ip-rotate">
	                    <button type="button" class="btn btn-default ip-rotate-ccw" title="Rotate counter-clockwise"><i class="icon-ccw"></i></button>
	                    <button type="button" class="btn btn-default ip-rotate-cw" title="Rotate clockwise"><i class="icon-cw"></i></button>
	                </div>
	                <div class="ip-progress">
	                    <div class="text">Enviando</div>
	                    <div class="progress progress-striped active"><div class="progress-bar"></div></div>
	                </div>
	            </div>
	            <div class="ip-modal-footer">
	                <div class="ip-actions">
	                    <button type="button" class="btn btn-success ip-save">Salvar Imagem</button>
	                    <button type="button" class="btn btn-default ip-cancel">Cancelar</button>
	                </div>
	                <button type="button" class="btn btn-default ip-close">Fechar</button>
	            </div>
	        </div>
	    </div>
	</div>
	<!-- end Modal -->

	<?php include 'inc/interno-footer.php' ?>
	<?php include 'inc/interno-js.php' ?>
	<script type="text/javascript" src="/js/select2.full.min.js"></script>
	<script type="text/javascript" src="/js/i18n/pt-BR.js"></script>
	<script type="text/javascript" src="/js/jquery.mask.min.js"></script>

	<!-- CROP -->
	<!-- <script src="/assets/js/jquery-1.11.0.min.js"></script> -->
	<script src="/assets/js/jquery.Jcrop.min.js"></script>
	<script src="/assets/js/jquery.imgpicker.js"></script>
	<!-- CROP -->

	<script type="text/javascript">
		$(document).ready(function(){
			Materialize.updateTextFields();
			$('input#anuncios_principais').characterCounter();
			$('#ramo_principal').material_select();
			$('#cidade').select2({
				language: "pt-BR",
				placeholder: "Cidade",
				minimumInputLength: 3,
				ajax: {
				    url: '/select/cidades',
				    dataType: 'json',
				    delay: 250
				    // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
			  	}
			});

			$('#avatarModal').imgPicker({
	            url: '/server/upload_avatar.php',
	            aspectRatio: 1,
	            data: {
	                uniqid: '<?php echo ($usuario->img_principal != null && $usuario->img_principal != "" ? $usuario->img_principal : uniqid("usuario_")) ?>',
	                id: <?php echo $usuario->id ?>
	            },
	            deleteComplete: function() {
	                $('#avatar2').attr('src', '//gravatar.com/avatar/0?d=mm&s=150');
	                $.post("/perfil/atualizar_foto", {
	                    foto: "",
	                    id: <?php echo $usuario->id ?>
	                }, function(result){

	                });
	                this.modal('hide');
	            },
	            uploadSuccess: function(image) {
	                // Calculate the default selection for the cropper
	                var select = (image.width > image.height) ?
	                [(image.width-image.height)/2, 0, image.height, image.height] :
	                [0, (image.height-image.width)/2, image.width, image.width];      
	                this.options.setSelect = select;
	            },
	            cropSuccess: function(image) {
	                $('#avatar2').attr('src', '//gravatar.com/avatar/0?d=mm&s=150');
	                $('#avatar2').attr('src', "/img/"+image.name);
	                $.post("/perfil/atualizar_foto", {
	                    foto: image.name,
	                    id: <?php echo $usuario->id ?>
	                }, function(result){
	                    location.reload();
	                });
	                this.modal('hide');
	            }
	        });

		});

		$('#headerModal').imgPicker({
		    url: '/server/upload_header.php',
		    aspectRatio: 998/400,
		    data: {
		        uniqid: '<?php echo uniqid("banner_") ?>',
		        id: <?php echo $usuario->id ?>
		    },
		    setSelect: [0, 0, 10, 10],
		    deleteComplete: function() {
		        $('.header img').attr('src', '');
		        this.modal('hide');
		        $.post("/perfil/atualizar_banner", {
		            foto: "",
		            id: <?php echo $usuario->id ?>
		        }, function(result){
		            location.reload();
		        });
		    },
		    cropSuccess: function(image) {
		        $('.header img').attr('src', '/img/' + image.url);

		        $.post("/perfil/atualizar_banner", {
		            foto: image.name,
		            id: <?php echo $usuario->id ?>
		        }, function(result){
		            location.reload();
		        });
		        this.modal('hide');
		    }
		});

		$(".remover_banner").click(function(){
			if(confirm("Tem certeza?")){
				$.post("/perfil/atualizar_banner", {
				    foto: "",
				    id: <?php echo $usuario->id ?>
				}, function(result){
				    location.reload();
				});
			}
		});

		<?php if($usuario->tipo_documento == "CNPJ"){ ?>
			$("#cpfcnpj").mask("99.999.999/9999-99");
		<?php }else{ ?>
			$("#cpfcnpj").mask("999.999.999-99");
		<?php } ?>
		$("#cep").mask("99999-999");
		$("#fone").mask("(99)99999 9999");
		$("#cel").mask("(99)99999 9999");
		$("#whats").mask("(99)99999 9999");
		$(".tipo_documento").change(function(){
			tipo = $(this).val();
			if(tipo == "cpf"){
				$("#cpfcnpj").mask("999.999.999-99");
				$("#cpfcnpj").parent().find("label").html("CPF");
			}else if(tipo == "cnpj"){
				$("#cpfcnpj").mask("99.999.999/9999-99");
				$("#cpfcnpj").parent().find("label").html("CNPJ");
			}
			Materialize.updateTextFields();
		});
		$('.datepicker').pickadate({
		    selectMonths: true, // Creates a dropdown to control month
		    selectYears: 100,
		    max: true,
		    min: [1900, 1, 1],
		    today: 'Hoje',
		    clear: 'Limpar',
		    close: 'Selecionar',
		    weekdaysLetter: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'],
		    weekdaysShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
		    weekdaysFull: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
		    monthsShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
		    monthsFull: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
		    labelMonthSelect: 'Selecione o Mês',
	        labelYearSelect: 'Selecione o Ano',
	        labelMonthNext: 'Próximo Mês',
            labelMonthPrev: 'Mês Anterior',
            format: 'dd/mm/yyyy',
            formatSubmit: 'yyyy-mm-dd',
		    closeOnSelect: true // Close upon selecting a date,
		});
	</script>
</body>
</html>