<?php
defined('BASEPATH') OR exit('Acesso negado');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<title>SuperBirds - Interno</title>
	<?php include 'inc/interno-head.php' ?>
</head>
<body class="blue-grey lighten-5">
	<?php include 'inc/interno-menu_lateral.php' ?>
	<?php include 'inc/interno-topo.php' ?>
	
	<main>
		<div class="container-fluid">
			<div class="row">
				<div class="col xl4 l4 m12 s12">
					<div class="card card-icon">
						<div class="card-content-icon z-depth-1 green darken-2">
							<i class="fab fa-earlybirds fa-5x white-text"></i>
						</div>
						<div class="card-content">
							<span class="card-title">Aves</span>
							<h3 class="flow-text"><?php echo $aves ?></h3>
						</div>
						<div class="card-action">
							<a href="/aves" class="green-text text-darken-2">Acessar Aves</a>
						</div>
					</div>
				</div>
				<div class="col xl4 l4 m12 s12">
					<div class="card card-icon">
						<div class="card-content-icon z-depth-1 light-blue darken-3">
							<i class="fas fa-cubes fa-5x white-text"></i>
						</div>
						<div class="card-content">
							<span class="card-title">Produtos</span>
							<h3 class="flow-text"><?php echo $produtos ?></h3>
						</div>
						<div class="card-action">
							<a href="/produtos" class="light-blue-text text-darken-3">Acessar Produtos</a>
						</div>
					</div>
				</div>
				<div class="col xl4 l4 m12 s12">
					<div class="card card-icon">
						<div class="card-content-icon z-depth-1 orange darken-2">
							<i class="fas fa-wrench fa-5x white-text"></i>
						</div>
						<div class="card-content">
							<span class="card-title">Serviços</span>
							<h3 class="flow-text"><?php echo $servicos ?></h3>
						</div>
						<div class="card-action">
							<a href="/servicos" class="orange-text text-darken-2">Acessar Serviços</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
	<?php include 'inc/interno-footer.php' ?>
	<?php include 'inc/interno-js.php' ?>
</body>
</html>