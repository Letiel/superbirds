<!DOCTYPE html>
<html>
<head>
	<title>Criador</title>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="/css/materialize.min.css">
	<link href="/css/perfect-scrollbar.css" rel="stylesheet">
	<script src="/js/perfect-scrollbar.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<style type="text/css">
	    .no-margin{
	    	margin: 0 !important;
	    }

	    .item-menu a{
	    	font-size: 2.7rem;
	    }

	    nav ul{
	    	width: 100%;
	    	min-height: 60px;
	    	margin: 0 auto;
	    }

	    ul li{
	    	float: none;
	    	display: inline-flex;
	    }

	    ul li a {
	        position: relative;
	        color: #fff;
	        letter-spacing: -1px;
	        text-decoration: none;
	        font-weight: 300;
	        font-size: 2rem;
	        margin: 0 25px;
	    }

	    header{
	    	padding: 2rem;
	    }

	    nav{
	    	box-shadow: none !important;
	    	margin-top: 15px;
	    	width: 100%;
	    }

	    .nav-wrapper{
	    	position: relative;
	    	height: 100%;
	    	display: inline-block;
	    }

	    .circle{
	    	width: 140px;
	    }

	    h1 {
	        margin: .9rem 0 0 0;
	        padding: 0;
	        font-weight: 300;
	        font-size: 1.7rem;
	        line-height: 1.4;
	        -webkit-backface-visibility: hidden;
	        color: #ffffff;
	    }
	</style>
</head>
<body class="blue-grey lighten-5">
	<header class="indigo center-align z-depth-2">
		<img class="circle resposive-img" src="https://scontent.fpoa8-1.fna.fbcdn.net/v/t1.0-9/25507722_1652379044808264_4094503431840525549_n.jpg?oh=0539971ee802b6b7f3b855d73920272f&oe=5B1D0F2B">
		<h1>Letiel Kellermann</h1>
		<nav class="indigo">
  			<a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
		  <div class="nav-wrapper">
		    <ul class="hide-on-med-and-down">
		    	<li class='item-menu'><a href="/">Aves</a></li>
		    	<li class='item-menu'><a href="/">Matérias</a></li>
		    	<li class='item-menu'><a href="/">Contato</a></li>
		    </ul>
		    <ul class="side-nav left-align" id="mobile-demo">
                <li class='item-menu'><a href="/">Aves</a></li>
                <li class='item-menu'><a href="/">Matérias</a></li>
                <li class='item-menu'><a href="/">Contato</a></li>
            </ul>
		  </div>
		</nav>
	</header>
	<main>
		<div class="container-fluid">
			<!-- BANNERS -->
			<div class="row">
				<div class="carousel carousel-slider">
				    <div class="carousel-item green darken-1">
				    	<h2 class="white-text">Demonstração</h2>
				    	<p class="white-text">Tente arrastar para o lado</p>
				    </div>

				    <div class="carousel-item" href="#two!">
				    	<img src="http://nutriave.com.br/wp-content/uploads/2017/03/escolha-da-gaiola.jpg">
				    </div>

				    <div class="carousel-item blue darken-1">
				    	<h2 class="white-text">Demonstração</h2>
				    	<p class="white-text">Subtítulo de demonstração</p>
				    </div>
				    
				    <div class="carousel-item purple darken-1">
				    	<h2 class="white-text">Demonstração</h2>
				    	<p class="white-text">Subtítulo de demonstração</p>
				    </div>
				</div>
			</div>

			<div class="row">
				<div class="col s12">
		          <div class="card">
		            <div class="card-content">
		              <span class="card-title">Demonstração</span>
		              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
		              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
		              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
		              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		            </div>
		          </div>
		        </div>
			</div>

			<!-- PÁSSAROS -->
			<div class="row">
				<div class="col s12 m6 l3">
					<div class="card hoverable">
						<div class="card-content">
							<span class="card-title grey-text text-lighten-3 center-align"><i class="fab fa-earlybirds fa-5x"></i></span>
						  Conteúdo Demonstração
						</div>
						<div class="card-action center-align">
						  <a class="blue-text no-margin" href="#">Pássaro Demonstração</a>
						</div>
					</div>
				</div>
				<div class="col s12 m6 l3">
					<div class="card hoverable">
						<div class="card-content">
							<span class="card-title grey-text text-lighten-3 center-align"><i class="fab fa-earlybirds fa-5x"></i></span>
						  Conteúdo Demonstração
						</div>
						<div class="card-action center-align">
						  <a class="blue-text no-margin" href="#">Pássaro Demonstração</a>
						</div>
					</div>
				</div>
				<div class="col s12 m6 l3">
					<div class="card hoverable">
						<div class="card-content">
							<span class="card-title grey-text text-lighten-3 center-align"><i class="fab fa-earlybirds fa-5x"></i></span>
						  Conteúdo Demonstração
						</div>
						<div class="card-action center-align">
						  <a class="blue-text no-margin" href="#">Pássaro Demonstração</a>
						</div>
					</div>
				</div>
				<div class="col s12 m6 l3">
					<div class="card hoverable">
						<div class="card-content">
							<span class="card-title grey-text text-lighten-3 center-align"><i class="fab fa-earlybirds fa-5x"></i></span>
						  Conteúdo Demonstração
						</div>
						<div class="card-action center-align">
						  <a class="blue-text no-margin" href="#">Pássaro Demonstração</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>

	<script type="text/javascript" src="/js/jquery.min.js"></script>
	<script type="text/javascript" src="/js/materialize.min.js"></script>
	<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
	<script type="text/javascript">
		$(".button-collapse").sideNav({
			draggable: true
		});
		$('.carousel.carousel-slider').carousel({
			fullWidth: true,
			indicators: true
		});

		setInterval(function() {
			$('.carousel.carousel-slider').carousel('next');
		}, 5000);
	</script>
</body>
</html>