<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<title>Alterar Produto - SuperBirds</title>
	<?php include 'inc/interno-head.php' ?>
	<link href="/css/select2.css" rel="stylesheet">

	<!-- CROP -->
	<link rel="stylesheet" href="/assets/css/imgpicker.css">
	<!-- CROP -->
</head>
<body class="blue-grey lighten-5">
	<?php include 'inc/interno-menu_lateral.php' ?>
	<?php include 'inc/interno-topo.php' ?>
	
	<main>
		<div class="container-fluid">
			<div class="card">
				<form method="post">
					<div class="card-content">
						<span class="card-title blue-grey-text">Alterar Produto - SuperBirds</span>
						<!-- <hr> -->
						
						<div class="row">
							<div class="col s12 m4 l3">
								<img src="<?php echo !empty($produto->img_principal) ? '/img/'.$produto->img_principal : '//gravatar.com/avatar/0?d=mm&s=150' ?>" alt="Avatar" class="image" style="width:100%" id="avatar2" >
								<input value="<?php echo $produto->img_principal ?>" type="hidden" name="img_principal" id="img_principal" style="display: none;" />
								<button type="button" class="btn blue btn-block blue-grey" data-ip-modal="#avatarModal">Foto</button>
								<span class='red-text'><?php echo form_error('img_principal') ?></span>
							</div>
							<div class="col s12 m8 l9">
								<div class="row">
									<div class="input-field col s12 m6">
										<label for="Nome">Nome</label>
										<input type="text" name="nome" id="nome" value="<?php echo $produto->nome ?>"/>
										<span class='red-text'><?php echo form_error('nome') ?></span>
									</div>
									<div class="input-field col s12 m6">
										<label for="cod_referencia">Cód. Referência</label>
										<input type="text" name="cod_referencia" id="cod_referencia" value="<?php echo $produto->cod_referencia ?>"/>
										<span class='red-text'><?php echo form_error('cod_referencia') ?></span>
									</div>
								</div>
								<div class="row">
									<div class="input-field col s12 m6">
										<select class='select' name="ps_tipo_prodserv" id="ps_tipo_prodserv">
											<option class="disabled" disabled selected="">Selecione um opção</option>
											<?php foreach ($tipos as $tipo): ?>
												<option value="<?php echo $tipo->id ?>" <?php echo $tipo->id == $produto->ps_tipo_prodserv ? "selected" : "" ?>><?php echo $tipo->nome ?></option>
											<?php endforeach ?>
										</select>
										<label for="ps_tipo_prodserv">Tipo</label>
										<span class='red-text'><?php echo form_error('ps_tipo_prodserv') ?></span>
									</div>
									<div class="input-field col s12 m6">
										<select class='select' name="p_tipo_prod" id="p_tipo_prod">
											<option <?php echo $produto->p_tipo_prod == "N" ? "selected" : "" ?> value="N">Novo</option>
											<option <?php echo $produto->p_tipo_prod == "U" ? "selected" : "" ?> value="U">Usado</option>
										</select>
										<label for="p_tipo_prod">Estado</label>
										<span class='red-text'><?php echo form_error('p_tipo_prod') ?></span>
									</div>
									<div class="input-field col s12 m6">
										<input type="text" name="p_quantidade" id="p_quantidade" value="<?php echo $produto->p_quantidade ?>"/>
										<label for="p_quantidade">Quantidade</label>
										<span class='red-text'><?php echo form_error('p_quantidade') ?></span>
										<span class="blue-grey-text">Ex: 1 Kilo, 2 Unidades, etc.</span>
									</div>
								</div>
							</div>
						</div>

						<div class="row">
							<div class="input-field col s12 m12">
								<label for="frase">Frase</label>
								<input type="text" name="frase" id="frase" value="<?php echo $produto->frase ?>"/>
								<span class='red-text'><?php echo form_error('frase') ?></span>
							</div>
						</div>

						<div class="row">
							<div class="input-field col s12 m12">
								<label for="descricao">Descrição</label>
								<textarea data-length="200" class="materialize-textarea" name="descricao" id='descricao'><?php echo $produto->descricao ?></textarea>
								<span class='red-text'><?php echo form_error('descricao') ?></span>
							</div>
						</div>

						<div class="row">
							<div class="input-field col s12 m6">
								<label for="link_externo">Link Externo</label>
								<input type="text" name="link_externo" id="link_externo" value="<?php echo $produto->link_externo ?>" />
								<span class='red-text'><?php echo form_error('link_externo') ?></span>
							</div>
							<div class="input-field col s12 m6">
								<label for="link_comercial">Link Comercial</label>
								<input type="text" name="link_comercial" id="link_comercial" value="<?php echo $produto->link_comercial ?>" />
								<span class='red-text'><?php echo form_error('link_comercial') ?></span>
							</div>
						</div>

						<div class="row">
							<div class="input-field col s12 m3">
								<label for="valor">Valor(R$)</label>
								<input type="text" name="valor" id="valor" value="<?php echo $produto->valor ?>" />
								<span class='red-text'><?php echo form_error('valor') ?></span>
							</div>
							<div class="col s12 m3 checkbox">
								<p>
									<input <?php echo $produto->valor_mostrar == true ? "checked" : "" ?> value="valor_mostrar" type="checkbox" id="valor_mostrar" name="valor_mostrar" />
									<label for="valor_mostrar">Mostrar Valor</label>
								</p>
								<span class='red-text'><?php echo form_error('valor_mostrar') ?></span>
							</div>
							<div class="input-field col s12 m4">
								<select class='select' name="entrega" id="entrega">
									<option class="disabled" disabled selected="">Selecione uma opção</option>
									<option <?php echo $produto->entrega == "ACOMBINAR" ? "selected" : "" ?> value="ACOMBINAR">A Combinar</option>
									<option <?php echo $produto->entrega == "SIM" ? "selected" : "" ?> value="SIM">Sim</option>
									<option <?php echo $produto->entrega == "NAO" ? "selected" : "" ?> value="NAO">Não</option>
								</select>
								<label for="entrega">Entrega</label>
								<span class='red-text'><?php echo form_error('entrega') ?></span>
							</div>
						</div>

						<div class="row">
							<div class="input-field col s12 m12">
								<label for="entrega_obs">Obs. Entrega</label>
								<textarea data-length="200" class="materialize-textarea" name="entrega_obs" id='entrega_obs'><?php echo $produto->entrega_obs ?></textarea>
								<span class='red-text'><?php echo form_error('entrega_obs') ?></span>
							</div>
						</div>

						<div class="row">
							<div class="input-field col s12 m4">
								<select class='select' name="aceita_troca" id="aceita_troca">
									<option class="disabled" disabled selected="">Selecione um opção</option>
									<option <?php echo $produto->aceita_troca == "ACOMBINAR" ? "selected" : "" ?> value="ACOMBINAR">A Combinar</option>
									<option <?php echo $produto->aceita_troca == "SIM" ? "selected" : "" ?> value="SIM">Sim</option>
									<option <?php echo $produto->aceita_troca == "NAO" ? "selected" : "" ?> value="NAO">Não</option>
								</select>
								<label for="aceita_troca">Troca</label>
								<span class='red-text'><?php echo form_error('aceita_troca') ?></span>
							</div>
							<div class="input-field col s12 m8">
								<label for="parcela">Parcela</label>
								<input type="text" name="parcela" id="parcela" value="<?php echo $produto->parcela ?>" />
								<span class='red-text'><?php echo form_error('parcela') ?></span>
							</div>
						</div>

						<div class="row">
							<div class="input-field col s12 m12">
								<label for="outras_opcoes">Outras Opções</label>
								<textarea data-length="200" class="materialize-textarea" name="outras_opcoes" id='outras_opcoes'><?php echo $produto->outras_opcoes ?></textarea>
								<span class='red-text'><?php echo form_error('outras_opcoes') ?></span>
							</div>
						</div>
						<div class="row">
							<button class="btn  blue right">Alterar</button>
						</div>
					</div>

				</div>

			</form>
		</div>

	</div>
</main>

<!-- Avatar Modal -->
<div class="ip-modal" id="avatarModal">
	<div class="ip-modal-dialog">
		<div class="ip-modal-content">
			<div class="ip-modal-header">
				<a class="ip-close" title="Close">&times;</a>
				<h4 class="ip-modal-title">Alterar Imagem</h4>
			</div>
			<div class="ip-modal-body">
				<div class="btn blue ip-upload">Enviar <input type="file" name="file" class="ip-file"></div>

				<button type="button" class="btn blue lighten-1 ip-edit">Editar</button>
				<button type="button" class="btn red ip-delete">Excluir</button>

				<div class="alert ip-alert"></div>
				<div class="ip-info">Para cortar esta imagem, arraste o mouse na região abaixo e clique em "Salvar Imagem"</div>
				<div class="ip-preview"></div>
				<div class="ip-rotate">
					<button type="button" class="btn ip-rotate-ccw" title="Rotate counter-clockwise"><i class="icon-ccw"></i></button>
					<button type="button" class="btn ip-rotate-cw" title="Rotate clockwise"><i class="icon-cw"></i></button>
				</div>
				<div class="ip-progress">
					<div class="text">Enviando</div>
					<div class="progress progress-striped active"><div class="progress-bar"></div></div>
				</div>
			</div>
			<div class="ip-modal-footer">
				<div class="ip-actions">
					<button type="button" class="btn blue ip-save">Salvar Imagem</button>
					<button type="button" class="btn grey ip-cancel">Cancelar</button>
				</div>
				<button type="button" class="btn red darken-5 ip-close">Fechar</button>
			</div>
		</div>
	</div>
</div>
<!-- end Modal -->


<?php include 'inc/interno-footer.php' ?>
<?php include 'inc/interno-js.php' ?>
<script type="text/javascript" src="/js/select2.full.min.js"></script>
<script type="text/javascript" src="/js/i18n/pt-BR.js"></script>
<script type="text/javascript" src="/js/jquery.mask.min.js"></script>

<!-- CROP -->
<!-- <script src="/assets/js/jquery-1.11.0.min.js"></script> -->
<script src="/assets/js/jquery.Jcrop.min.js"></script>
<script src="/assets/js/jquery.imgpicker.js"></script>
<!-- CROP -->
<script type="text/javascript" src="/js/jquery.mask.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#valor').mask("# ##0.00", {reverse: true});
		Materialize.updateTextFields();
		$('.select').material_select();
		$('textarea').characterCounter();
		$('#avatarModal').imgPicker({
			url: '/server/upload_avatar.php',
			aspectRatio: 1,
			data: {
				uniqid: '<?php echo empty($produto->img_principal) ? uniqid() : $produto->img_principal ?>',
			},
			deleteComplete: function() {

			},
			uploadSuccess: function(image) {
	                // Calculate the default selection for the cropper
	                var select = (image.width > image.height) ?
	                [(image.width-image.height)/2, 0, image.height, image.height] :
	                [0, (image.height-image.width)/2, image.width, image.width];      
	                this.options.setSelect = select;
	            },
	            cropSuccess: function(image) {

	            	var n=Math.floor(Math.random()*11);
	            	var k = Math.floor(Math.random()* 1000000);
	            	var uniqid = String.fromCharCode(n)+k;//gerando caracteres aleatórios para concatenar na imagem... para evitar cache

	            	$('#avatar2').attr('src', '//gravatar.com/avatar/0?d=mm&s=150');
	            	$('#avatar2').attr('src', "/img/"+image.name+"?"+uniqid);
	            	Materialize.toast('Imagem alterada!', 5000, 'blue');
	                // ATRIBUIR PARA CAMPO HIDDEN O NOME DA IMG
	                $("#img_principal").val(image.name);
	                this.modal('hide');
	            }
	        });
	});
	
	</script>
</body>
</html>