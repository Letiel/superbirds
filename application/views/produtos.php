<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<title>Produtos - SuperBirds</title>
	<?php include 'inc/interno-head.php' ?>
</head>
<body class="blue-grey lighten-5">
	<?php include 'inc/interno-menu_lateral.php' ?>
	<?php include 'inc/interno-topo.php' ?>
	<main>
		<div class="container-fluid">

			<div class="card">
				<div class="card-content">
					<div class="left">
						<p class="card-title blue-grey-text">Seus Produtos - SuperBirds</p>
						<?php if ($limites->total_anuncios < $limites->anuncios_permitidos): ?>
							<p class="blue-grey-text">Você possui <b><?php echo $limites->anuncios_permitidos-$limites->total_anuncios ?></b> anúncios permitidos.</p>
						<?php else: ?>
							<p class='red-text'>Você não possui mais anúncios permitidos em seu plano. <a href="/solicitacoes/planos">Trocar Plano.</a></p>
						<?php endif ?>
					</div>
					<div class="right">
						<a href='/produtos/cadastrar' class="btn blue"><i class="fas fa-plus"></i> Incluir</a>
					</div>
					<div class="clearfix"></div>
					<hr>
					<div class="row">
						<?php if ($produtos == null): ?>
							<h3 class="center-align blue-grey-text">Nenhum Produto</h3>
						<?php endif ?>
						<?php foreach ($produtos as $produto): ?>
							<div class="col s12 m3">
						      <div class="card sticky-action <?php echo $produto->situacao == 'S' ? 'suspenso' : '' ?> <?php echo $produto->situacao == 'V' ? 'vendido' : '' ?> <?php echo $produto->situacao == 'C' ? 'concluido' : '' ?>">
						        <div class="card-image">
						          <img src="<?php echo empty($produto->img_principal) ? '//gravatar.com/avatar/0?d=mm&s=150' : '/img/'.$produto->img_principal."?".uniqid() ?>">
						        </div>
						        <div class="card-content">
						        	<span title="<?php echo $produto->nome ?>" class="card-title activator grey-text text-darken-4 truncate"><i class="material-icons right">more_vert</i><?php echo $produto->nome ?></span>
						        	<?php if (!empty($produto->p_quantidade)): ?>
						        		<p title="<?php echo $produto->p_quantidade ?>" class="grey-text text-darken-4 truncate">Quantidade: <?php echo $produto->p_quantidade ?></p>
						        	<?php endif ?>

						        	<?php if (!empty($produto->valor)): ?>
						        		<p title="<?php echo $produto->valor ?>" class="grey-text text-darken-4 truncate">Valor: R$ <?php echo $produto->valor ?></p>
						        	<?php endif ?>
						        	
						        </div>
						        <div class="card-action blue-grey-text">
				                  <p><i class="fas fa-thumbs-up"></i> <?php echo $produto->curtidas ?>
				                  <i class="fas fa-eye"></i> <?php echo $produto->contador ?></p>
				                </div>
				                <div class="card-reveal">
				                	<span class="card-title grey-text text-darken-4 truncate" title="<?php echo $produto->nome ?>"><i class="material-icons right">close</i><?php echo $produto->nome ?></span>
				                	<a href="/produtos/editar/<?php echo $produto->id ?>" class="btn waves-effect blue btn-block">Alterar</a>
				                	<hr />
				                	<a class='dropdown-button btn btn-block green darken-3' href='#' data-activates='dropdown<?php echo $produto->id ?>'>Situação</a>
				                	<ul id='dropdown<?php echo $produto->id ?>' class='dropdown-content'>
				                		<li class="<?php echo $produto->situacao == "L" ? "blue-grey lighten-5" : "alterar_situacao" ?>"><a data-id="<?php echo $produto->id ?>" data-situacao="L" class='blue-text text-darken-5' href="#!">Ativado</a></li>
				                		<li class="<?php echo $produto->situacao == "S" ? "blue-grey lighten-5" : "alterar_situacao" ?>"><a data-id="<?php echo $produto->id ?>" data-situacao="S" class='red-text text-darken-5' href="#!">Suspenso</a></li>
				                		<li class="<?php echo $produto->situacao == "V" ? "blue-grey lighten-5" : "alterar_situacao" ?>"><a data-id="<?php echo $produto->id ?>" data-situacao="V" class='blue-grey-text' href="#!">Vendido</a></li>
				                		<li class="<?php echo $produto->situacao == "C" ? "blue-grey lighten-5" : "alterar_situacao" ?>"><a data-id="<?php echo $produto->id ?>" data-situacao="C" class='' href="#!">Concluído</a></li>
				                	</ul>
				                </div>
						      </div>
						    </div>
						<?php endforeach ?>
					</div>
					<div class="right-align">
						<?php echo $paginacao ?>
					</div>
				</div>
			</div>

		</div>
	</main>

	<?php include 'inc/interno-footer.php' ?>
	<?php include 'inc/interno-js.php' ?>

	<script type="text/javascript">
		$(document).ready(function(){
			$('.dropdown-button').dropdown({
				inDuration: 300,
				outDuration: 225,
				constrainWidth: false,
				hover: false,
				gutter: 0,
				belowOrigin: true,
				alignment: 'left',
				stopPropagation: false
			});

			$(".alterar_situacao").click(function(e){
				e.preventDefault();
				var situacao = $(this).find("a").attr("data-situacao");
				var id = $(this).find("a").attr("data-id");
				confirmacao = true;
				if(situacao == "V" || situacao == "C"){
					confirmacao = confirm("Tem certeza? Esta ação não pode ser desfeita.");
				}

				if(confirmacao)
					$.post("/produtos/alterar_situacao", {
						situacao: situacao,
						id: id
					}, function(result){
						location.reload();
					});
			});
		});
	</script>
</body>
</html>