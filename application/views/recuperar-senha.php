<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<title>Recuperar Senha - SuperBirds</title>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="/css/materialize.min.css">
	<link href="/css/perfect-scrollbar.css" rel="stylesheet">
	<script src="/js/perfect-scrollbar.min.js"></script>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<style type="text/css">
		html, body, main, .container-fluid, .row, .valign-wrapper{
			height: 100%;
		}
		.input-field input:focus + label {
		   color: #ff9800 !important;
		 }
		 /* label underline focus color */
		 .row .input-field input:focus {
		   border-bottom: 1px solid #ff9800 !important;
		   box-shadow: 0 1px 0 0 #ff9800 !important
		 }
		 .btn-block{
		 	width: 100%;
		 }
	</style>
</head>
<body class="blue-grey lighten-5">
	<main>
		<div class="container-fluid">
			<div class="row">
				<div class='valign-wrapper'>
					<div class="col s12 m10 offset-m1 l6 offset-l3">
						<div class="card">
							<form method="post" action="/recuperar-senha">
								<div class="card-content">
									<span class="center" style="display: inherit;"><i class='fab fa-themeisle fa-5x'></i></span>
									<span class="card-title center orange-text"><b>SuperBirds</b></span>
									<span class="card-title center orange-text"><b>Recuperar Senha</b></span>
									<span class="center blue-text"><b>Informe seu e-mail</b></span>
									<div class="input-field">
										<label for="login">E-mail</label>
										<input autofocus type="text" name="login" id="login" value="<?php echo set_value('login') ?>" class='validate'>
										<span class='red-text'><?php echo form_error('login') ?></span>
									</div>
									<div class="center-align">
										<span class='red-text'><?php echo $this->session->flashdata("retorno") ?></span>
									</div>
								</div>
								<div class="card-action">
									<button type="submit" class='btn waves-effect waves-light btn-block btn-large orange white-text'><b>Recuperar</b></button>
									<a href="/interno" class='btn waves-effect waves-light btn-block blue white-text' style="margin-top: 10px;"><b>Entrar</b></a>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
	<footer>
		
	</footer>
	<script type="text/javascript" src="/js/jquery.min.js"></script>
	<script type="text/javascript" src="/js/materialize.min.js"></script>
	<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			Materialize.updateTextFields();
			<?php echo $this->session->flashdata("toast"); ?>
		});
		var ps2 = new PerfectScrollbar('ul');
		$(".dropdown-button").dropdown();
		$(".button-collapse").sideNav({
			draggable: true
		});
	</script>
</body>
</html>