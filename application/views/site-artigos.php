<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <title>Artigos - SuperBirds</title>
  <?php include 'inc/site-head.php'; ?>
  <link href="/css/select2.css" rel="stylesheet">
</head>
<body class="">

  <?php include 'inc/site-topo.php' ?>
  <main>
    <?php include 'inc/site-banner-superior.php' ?>

    <div class="container" style="padding-top: 40px;">
      <div class="row">
        <h3 class="text-flow center-align"><b>Artigos</b></h3>
      </div>

      <div class="row">
        <form method="get" class="col s12">
          <div class="row">

            <div class="input-field col s5">
              <input id="pesquisa" name="p" value="<?php echo $pesquisa ?>" type="text" class="validate" placeholder="Buscar Artigos">
              <label for="pesquisa">Pesquisar</label>
            </div>
            <div class="input-field col s5">
              <label for="cidade" style="top: -26px; font-size: 0.8rem;">Cidade</label>
              <select name="c" id="cidade"></select>
            </div>
            <div class="col s2">
              <button type="submit" class="btn-flat" style="margin-top: 0px; padding: 10px; height: auto;"><i class="material-icons left">search</i> Pesquisar</button>
            </div>
          </div>
        </form>
      </div>

      <div class="row">
        <?php if (!empty($pesquisa)): ?>
          <h5>Sua pesquisa: <b><?php echo $pesquisa ?></b></h5>
        <?php endif ?>
        <?php foreach ($artigos as $artigo): ?>
          <div class="col s12 m4">
            <div class="card">
              <div class="card-image">
                <img src="<?php echo empty(!$artigo->img_principal) ? '/img/'.$artigo->img_principal : '/img/pena.jpg' ?>">
              </div>
              <div class="card-content">
                <span class="card-title truncate"><?php echo $artigo->titulo ?></span>
                <p>
                  <?php echo substr(strip_tags($artigo->descricao), 0, 100);
                  echo strlen(strip_tags($artigo->descricao)) > 100 ? "..." : ""; ?>
                </p>
              </div>
              <div class="card-action">
                <a class="green-text text-darken-4 link" href="/ver/artigo/<?php echo $artigo->id ?>">Visitar Artigo <i class="material-icons right">keyboard_arrow_right</i></a> 
              </div>
            </div>
          </div>
        <?php endforeach ?>
        
      </div>
      <div class="row">
        <?php echo $paginacao ?>
      </div>


    </main>
    <?php include 'inc/site-footer.php' ?>

    <script type="text/javascript" src="/js/jquery.min.js"></script>
    <script type="text/javascript" src="/js/materialize.min.js"></script>
    <script src="/js/perfect-scrollbar.min.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
    <script type="text/javascript" src="/js/select2.full.min.js"></script>
    <script type="text/javascript" src="/js/i18n/pt-BR.js"></script>
    <script type="text/javascript">

     $(function(){
      $(".button-collapse").sideNav();
      $(window).scroll(function() {
       if($(document).scrollTop() > 40) {
        $( "#top-bird" ).css("height", "0");
        $( "#top-bird" ).css("padding", "0");
      }else{
        $( "#top-bird" ).css("height", "100px");
        $( "#top-bird" ).css("padding", "10px");
      }
      if($(document).scrollTop() < 40){
      }
    });

      $('.slider').slider();

      $(document).ready(function(){
        $('#cidade').select2({
          language: "pt-BR",
          placeholder: "Cidade",
          minimumInputLength: 3,
          ajax: {
            url: '/select/cidades',
            dataType: 'json',
            delay: 250
            // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
          }
        });
      });
    });
  </script>
  <?php include("inc/site-js.php") ?>
</body>
</html>
