<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <title>Aves - SuperBirds</title>
  <?php include 'inc/site-head.php'; ?>
  <link href="/css/select2.css" rel="stylesheet">
</head>
<body class="">

  <?php include 'inc/site-topo.php' ?>
  <main>
    <?php include 'inc/site-banner-superior.php' ?>
    
    <div class="container" style="padding-top: 40px;">
      <div class="row">
        <h3 class="text-flow center-align"><b>Aves</b></h3>
      </div>

      <div class="row">
        <form method="get" class="col s12">

          <div class="row">
            <div class="input-field col s6">
              <input id="pesquisa" name="p" value="<?php echo $pesquisa ?>" type="text" class="validate" placeholder="Buscar Aves">
              <label for="pesquisa">Pesquisar</label>
            </div>
            <div class="input-field col s6">
              <label for="cidade" style="top: -26px; font-size: 0.8rem;">Cidade</label>
              <select name="c" id="cidade"></select>
            </div>
          </div>
          <div class="row">              
            <div class="input-field col s6">
              <select name="classe" id="classes">
                <option value="">Selecione</option>
                <?php foreach ($classes as $classe): ?>
                  <option value="<?php echo $classe->id ?>" <?php echo $classe->id == $classe_ ? "selected" : "" ?>><?php echo $classe->nome ?></option>
                <?php endforeach ?>
              </select>
              <label for="classes">Classe</label>
            </div>
            <div class="input-field col s6">
              <select name="mutacao" id="mutacoes">
                <option value="">Selecione</option>
                <?php foreach ($mutacoes as $mutacao): ?>
                  <option value="<?php echo $mutacao->id ?>" <?php echo $mutacao->id == $mutacao_ ? "selected" : "" ?>><?php echo $mutacao->nome ?></option>
                <?php endforeach ?>
              </select>
              <label for="mutacoes">Mutação</label>
            </div>
          </div>
          <div class="row">              
            <div class="input-field col s6">
              <select name="cor" id="cores">
                <option value="">Selecione</option>
                <?php foreach ($cores as $cor): ?>
                  <option value="<?php echo $cor->id ?>" <?php echo $cor->id == $cor_ ? "selected" : "" ?>><?php echo $cor->cor.(empty($cor->subcor) ? "" : " - ".$cor->subcor) ?></option>
                <?php endforeach ?>
              </select>
              <label for="cores">Cor</label>
            </div>
            <div class="input-field col s6">
              <select name="unidade" id="unidades">
                <option value="">Selecione</option>
                <?php foreach ($unidades as $unidade): ?>
                  <option value="<?php echo $cor->id ?>" <?php echo $unidade->id == $unidade_ ? "selected" : "" ?>><?php echo $unidade->nome ?></option>
                <?php endforeach ?>
              </select>
              <label for="unidades">Unidade</label>
            </div>
          </div>
          <div class="row">              
            <div class="col s2 offset-s5">
              <button type="submit" class="btn-flat" style="margin-top: 0px; padding: 10px; height: auto;"><i class="material-icons left">search</i> Pesquisar</button>
            </div>
          </div>
        </form>
      </div>

      <div class="row">
        <?php if (!empty($pesquisa)): ?>
          <h5>Sua pesquisa: <b><?php echo $pesquisa ?></b></h5>
        <?php endif ?>
        <?php foreach ($aves as $ave): ?>
          <div class="col s12 m4">
            <div class="card">
              <div class="card-image">
                <img src="<?php echo empty(!$ave->img_principal) ? '/img/'.$ave->img_principal : '/img/pena.jpg' ?>">
              </div>
              <div class="card-content">
                <span class="card-title truncate"><?php echo $ave->nome ?></span>
                <p class='truncate'>Criador: <a href="/criadores/ver/<?php echo $ave->id_criador ?>"><?php echo $ave->criador ?></a></p>
                <p class='truncate'>Classe: <?php echo $ave->classe ?></p>
                <p class="truncate">Mutação: <?php echo $ave->mutacao ?></p>
                <div class="row">
                  <div class="col s6"><p class="blue-text center-align"><i class="fas fa-2x fa-eye"></i><br><?php echo $ave->contador ?></p></div>
                  <div class="col s6"><p class="blue-text center-align"><i class="far fa-2x fa-thumbs-up"></i><br><?php echo $ave->curtidas ?></p></div>
                </div>


              </div>
              <div class="card-action">
                <a class="green-text text-darken-4 link" href="/ver/ave/<?php echo $ave->id ?>">Visitar Ave <i class="material-icons right">keyboard_arrow_right</i></a> 
              </div>
            </div>      
          </div>
        <?php endforeach ?>
      </div>
      <div class="row center-align">
        <?php echo $paginacao ?>
      </div>
    </div>
  </main>
  <?php include 'inc/site-footer.php' ?>


  <script type="text/javascript" src="/js/jquery.min.js"></script>
  <script type="text/javascript" src="/js/materialize.min.js"></script>
  <script src="/js/perfect-scrollbar.min.js"></script>
  <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
  <script type="text/javascript" src="/js/select2.full.min.js"></script>
  <script type="text/javascript" src="/js/i18n/pt-BR.js"></script>
  <script type="text/javascript">

   $(function(){
    $(".button-collapse").sideNav();
    $(window).scroll(function() {
     console.log($(document).scrollTop());
     if($(document).scrollTop() > 40) {
      $( "#top-bird" ).css("height", "0");
      $( "#top-bird" ).css("padding", "0");
    }else{
      $( "#top-bird" ).css("height", "100px");
      $( "#top-bird" ).css("padding", "10px");
    }
    if($(document).scrollTop() < 40){
    }
  });

    $('.slider').slider();

    $(document).ready(function(){
      $('#classes').material_select();
      $('#mutacoes').material_select();
      $('#cores').material_select();
      $('#unidades').material_select();
      $('#cidade').select2({
        language: "pt-BR",
        placeholder: "Cidade",
        minimumInputLength: 3,
        ajax: {
          url: '/select/cidades',
          dataType: 'json',
          delay: 250
            // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
          }
        });
    });
  });
</script>
<?php include("inc/site-js.php") ?>
</body>
</html>
