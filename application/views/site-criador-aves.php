<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <title>Aves - SuperBirds</title>
	<?php include 'inc/site-head.php'; ?>
  <link href="/css/select2.css" rel="stylesheet">
</head>
<body class="">

  <?php include 'inc/site-topo.php' ?>
  <main>
    <?php include 'inc/site-banner-superior.php' ?>
    
        <div class="row">
          <hr>
          <h3 class="blue-grey-text center-align">Aves de <b><?php echo $criador->nome ?></b></h3>
        </div>
        <hr>
        <div class="container">
          <div class="row">
            
            <?php foreach ($aves as $ave): ?>
              <div class="col s12 m4">
                <div class="card">
                  <div class="card-image">
                    <img src="<?php echo empty(!$ave->img_principal) ? '/img/'.$ave->img_principal : '/img/pena.jpg' ?>">
                  </div>
                  <div class="card-content">
                    <span class="card-title truncate"><?php echo $ave->nome ?></span>
                    <p class='truncate'>Criador: <a href="/criadores/ver/<?php echo $ave->id_criador ?>"><?php echo $ave->criador ?></a></p>
                    <p class='truncate'>Classe: <?php echo $ave->classe ?></p>
                    <p class="truncate">Mutação: <?php echo $ave->mutacao ?></p>
                    <div class="row">
                      <div class="col s6"><p class="blue-text center-align"><i class="fas fa-2x fa-eye"></i><br><?php echo $ave->contador ?></p></div>
                      <div class="col s6"><p class="blue-text center-align"><i class="far fa-2x fa-thumbs-up"></i><br><?php echo $ave->curtidas ?></p></div>
                    </div>
                    
                    
                  </div>
                  <div class="card-action">
                    <a class="btn btn-block blue darken-2 waves-effect waves-light" href="/ver/ave/<?php echo $ave->id ?>">Visitar</a>
                  </div>
                </div>
              </div>
            <?php endforeach ?>
          </div>
          <?php echo $paginacao ?>
        </div>

	</main>
  <?php include 'inc/site-footer.php' ?>

	<script type="text/javascript" src="/js/jquery.min.js"></script>
	<script type="text/javascript" src="/js/materialize.min.js"></script>
	<script src="/js/perfect-scrollbar.min.js"></script>
	<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
  <script type="text/javascript" src="/js/select2.full.min.js"></script>
  <script type="text/javascript" src="/js/i18n/pt-BR.js"></script>
	<script type="text/javascript">

	$(function(){
    $(".button-collapse").sideNav();
		$(window).scroll(function() {
			console.log($(document).scrollTop());
			if($(document).scrollTop() > 40) {
				$( "#top-bird" ).css("height", "0");
				$( "#top-bird" ).css("padding", "0");
			}else{
				$( "#top-bird" ).css("height", "100px");
				$( "#top-bird" ).css("padding", "10px");
			}
			if($(document).scrollTop() < 40){
			}
		});

		$('.slider').slider();

    $(document).ready(function(){
      $('#cidade').select2({
        language: "pt-BR",
        placeholder: "Cidade",
        minimumInputLength: 3,
        ajax: {
            url: '/select/cidades',
            dataType: 'json',
            delay: 250
            // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
          }
      });
    });
	});
	</script>
  <?php include("inc/site-js.php") ?>
</body>
</html>
