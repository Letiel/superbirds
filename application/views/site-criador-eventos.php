<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <title>Eventos de <?php echo $criador->nome ?> - SuperBirds</title>
	<?php include 'inc/site-head.php'; ?>
  <link href="/css/select2.css" rel="stylesheet">
</head>
<body class="">

  <?php include 'inc/site-topo.php' ?>
  <main>
    <?php include 'inc/site-banner-superior.php' ?>
    
        <div class="row">
          <hr>
          <h3 class="blue-grey-text center-align">Eventos de <b><?php echo $criador->nome ?></b></h3>
        </div>
        <hr>
        <div class="container">
          <div class="row">
            <?php foreach ($eventos as $evento): ?>
              <div class="col s12 m12 l4">
                <div class="card">
                  <div class="card-image blue-grey lighten-3">
                    <p class="white-text center-align" style="font-size: 100px; padding: 10px 0; margin: 0;"><i class="far fa-calendar-alt"></i></p>
                  </div>
                  
                  <div class="card-content">
                    <span class="card-title truncate"><?php echo $evento->titulo ?></span>
                    <p>Data: <b><?php echo date("d/m/Y", strtotime($evento->data_evento)) ?></b></p>
                    <p>
                      <?php echo substr(strip_tags($evento->descricao), 0, 100);
                      echo strlen(strip_tags($evento->descricao)) > 100 ? "..." : ""; ?>
                    </p>
                  </div>
                  <div class="card-action">
                    <a class="btn btn-block blue darken-2 waves-effect waves-light" href="/ver/evento/<?php echo $evento->id ?>">Visitar</a>
                  </div>
                </div>
              </div>
            <?php endforeach ?>
          </div>
          <?php echo $paginacao ?>
        </div>

	</main>
  <?php include 'inc/site-footer.php' ?>

	<script type="text/javascript" src="/js/jquery.min.js"></script>
	<script type="text/javascript" src="/js/materialize.min.js"></script>
	<script src="/js/perfect-scrollbar.min.js"></script>
	<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
  <script type="text/javascript" src="/js/select2.full.min.js"></script>
  <script type="text/javascript" src="/js/i18n/pt-BR.js"></script>
	<script type="text/javascript">

	$(function(){
    $(".button-collapse").sideNav();
		$(window).scroll(function() {
			console.log($(document).scrollTop());
			if($(document).scrollTop() > 40) {
				$( "#top-bird" ).css("height", "0");
				$( "#top-bird" ).css("padding", "0");
			}else{
				$( "#top-bird" ).css("height", "100px");
				$( "#top-bird" ).css("padding", "10px");
			}
			if($(document).scrollTop() < 40){
			}
		});

		$('.slider').slider();

    $(document).ready(function(){
      $('#cidade').select2({
        language: "pt-BR",
        placeholder: "Cidade",
        minimumInputLength: 3,
        ajax: {
            url: '/select/cidades',
            dataType: 'json',
            delay: 250
            // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
          }
      });
    });
	});
	</script>
  <?php include("inc/site-js.php") ?>
</body>
</html>
