<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <title>Fale Conosco - SuperBirds</title>
	<?php include 'inc/site-head.php'; ?>
</head>
<body class="">
  	<?php include 'inc/site-topo.php' ?>
  	<main>
  		<div class="container" style="margin-top: 50px">
  			<div class="row">
  				<form class="col s12" method="post">
  					<div class="row">
  						<h3 class="header">Fale Conosco</h3>
  					</div>
  					<div class="row">
  						<div class="input-field col s12">
  							<i class="material-icons prefix">person</i>
  							<input id="nome" name="nome" type="text" class="validate" value="<?php echo set_value('nome') ?>">
  							<label for="nome">Seu Nome:</label>
  							<?php echo form_error("nome") ?>
  						</div>
					</div>
  					<div class="row">
  						<div class="input-field col s12">
  							<i class="material-icons prefix">email</i>
  							<input id="email" name="email" type="email" class="validate" value="<?php echo set_value('email') ?>">
  							<label for="email">Seu Email</label>
  							<?php echo form_error("email") ?>
  						</div>
  					</div>
  					<div class="row">
  						<div class="input-field col s12">
  							<i class="material-icons prefix">mode_edit</i>
  							<textarea id="descricao" name="descricao" class="materialize-textarea"><?php echo set_value('descricao') ?></textarea>
  							<label for="descricao">Mensagem de Contato</label>
  							<?php echo form_error("descricao") ?>
  						</div>
  					</div>
  					<div class="row">
  						<div class="col s12">
  							<button class="btn blue btn-block waves-effect waves-light" type="submit">Enviar <i class="material-icons right">send</i></button>
  						</div>
  					</div>
  				</form>
  			</div>
  		</div>
  	</main>
    <?php include 'inc/site-footer.php' ?>

	<script type="text/javascript" src="/js/jquery.min.js"></script>
	<script type="text/javascript" src="/js/materialize.min.js"></script>
	<script src="/js/perfect-scrollbar.min.js"></script>
	<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
	<script type="text/javascript">

	$(function(){
		$('textarea').trigger('autoresize');
    $(".button-collapse").sideNav();
		$(window).scroll(function() {
			if($(document).scrollTop() > 40) {
				$( "#top-bird" ).css("height", "0");
				$( "#top-bird" ).css("padding", "0");
			}else{
				$( "#top-bird" ).css("height", "100px");
				$( "#top-bird" ).css("padding", "10px");
			}
			if($(document).scrollTop() < 40){
			}
		});

		$('.slider').slider();
	});
	</script>
  	<?php include("inc/site-js.php") ?>
</body>
</html>
