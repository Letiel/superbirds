<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <title><?php echo $artigo->titulo ?> - SuperBirds</title>
	<?php include 'inc/site-head.php'; ?>
  <link href="/css/select2.css" rel="stylesheet">
  <style type="text/css">
    .fotos{
      height: 200px !important;
    }
  </style>
</head>
<body class="">
  <?php include 'inc/site-topo.php' ?>
  <main>
    <div class="row grey lighten-5" style="padding: 30px 0">
      <div class="container">
        <div class="row">
          <div class="col s10 offset-s1 m6 l3">
            <img class="responsive-img circle materialboxed" src="<?php echo empty(!$artigo->img_principal) ? '/img/'.$artigo->img_principal : '/img/pena.jpg' ?>" />
          </div>
          <div class="col s12 m6 l9">
            <h3 class="blue-grey-text truncate" style="margin-bottom: 0;">
              <?php echo $artigo->titulo ?>
            </h3>
            <?php if (!empty($artigo->tipo)): ?>
              <h6 class="blue-grey-text truncate">
                  Tipo de artigo: <?php echo $artigo->tipo ?>
              </h6>
            <?php endif ?>
            <?php if (!empty($artigo->nome_usuario)): ?>
              <h6 class="blue-grey-text truncate">
                  Por: <a href="/criadores/ver/<?php echo $artigo->id_usuario ?>"><?php echo $artigo->nome_usuario ?></a>
              </h6>
            <?php endif ?>
            <?php if (!empty($artigo->frase)): ?>
              <blockquote>
                <h5 class="blue-grey-text truncate text-lighten-2">
                    <i class="fas fa-2x fa-quote-left text-lighten-4"></i>
                    <span><?php echo $artigo->frase ?></span>
                </h5>
              </blockquote>
            <?php endif ?>
          </div>
          <div class="col s12">
            <div class="row">
              <div class="col s6">
                <p class="blue-text center-align right"><i class="fas fa-2x fa-eye"></i><br>Visitas: <?php echo $artigo->contador ?></p>
              </div>
              <div class="col s6">
                <a href="/curtir/artigo/<?php echo $artigo->id ?>">
                  <p class="blue-text center-align left"><i class="far fa-2x fa-thumbs-up"></i><br>Curtidas: <?php echo $artigo->curtidas ?></p>
                </a>
              </div>
            </div>
            <?php if ($artigo->denunciado != "L"): ?>
              <div class="row">
                <div class="center-align"><button data-target="modalDenuncia" class="btn-flat red-text modal-trigger">Denunciar</button></div>
              </div>
            <?php endif ?>
          </div>
        </div>
      </div>
    </div>
    <?php if (!empty($artigo->img_1) || !empty($artigo->img_2) || !empty($artigo->img_3) || !empty($artigo->img_4) || !empty($artigo->img_5)): ?>
      <div class="row" style="padding: 30px 0">
        <div class="container">
          <div class="row">
            <div class="carousel fotos">
              <?php if (!empty($artigo->img_1)): ?>
                <a class="carousel-item" href="#img1"><img src="<?php echo '/img/'.$artigo->img_1 ?>"></a>
              <?php endif ?>
              <?php if (!empty($artigo->img_2)): ?>
                <a class="carousel-item" href="#img2"><img src="<?php echo '/img/'.$artigo->img_2 ?>"></a>
              <?php endif ?>
              <?php if (!empty($artigo->img_3)): ?>
                <a class="carousel-item" href="#img3"><img src="<?php echo '/img/'.$artigo->img_3 ?>"></a>
              <?php endif ?>
              <?php if (!empty($artigo->img_4)): ?>
                <a class="carousel-item" href="#img4"><img src="<?php echo '/img/'.$artigo->img_4 ?>"></a>
              <?php endif ?>
              <?php if (!empty($artigo->img_5)): ?>
                <a class="carousel-item" href="#img5"><img src="<?php echo '/img/'.$artigo->img_5 ?>"></a>
              <?php endif ?>
              </div>
          </div>
        </div>
      </div>
    <?php endif ?>
    <?php if (!empty($artigo->descricao)): ?>
      <div class="row grey lighten-5" style="padding: 30px 0">
        <div class="container">
          <div class="row">
              <h4 class="header"><?php echo $artigo->titulo ?></h4>
              <?php if (!empty($artigo->img_principal2)): ?>
                <div class="center-align">
                  <img class="responsive-img" src="/img/<?php echo $artigo->img_principal ?>" />
                </div>
              <?php endif ?>
              <p class="flow-text"><?php echo $artigo->descricao ?></p>
          </div>
        </div>
      </div>
    <?php endif ?>
    <?php if (!empty($artigo->fontes_autores)): ?>
        <div class="row" style="padding: 30px 0">
          <div class="container">
            <div class="row infos">
                <div class="col s6 m3 info">
                  <p class="truncate">
                    <b>Fonte(s):</b> 
                    <?php 
                      echo $artigo->fontes_autores;
                    ?>
                  </p>
                </div>
          </div>
        </div>
      <?php endif ?>
    </div>
    <?php if (!empty($aves)): ?>
    <div class="row grey lighten-5" style="padding: 30px 0">
      <div class="container">
        <div class="row carousel">
          <h3 class="header">Aves de <?php echo $artigo->nome_usuario ?></h3>
          <?php foreach ($aves as $outra): ?>
            <div class="col s12 m8 l6 carousel-item">
                <div class="card horizontal">
                  <div class="card-image">
                    <img src="<?php echo !empty($outra->img_principal) ? '/img/'.$outra->img_principal : '/img/pena.jpg' ?>">
                  </div>
                  <div class="card-stacked">
                    <div class="card-content">
                      <span class="card-title truncate"><?php echo $outra->nome ?></span>
                      <p class='truncate'>Classe: <?php echo $outra->classe ?></p>
                      <p class="truncate">Mutação: <?php echo $outra->mutacao ?></p>
                      <div class="row">
                        <div class="col s6"><p class="blue-text center-align"><i class="fas fa-2x fa-eye"></i><br><?php echo $outra->contador ?></p></div>
                        <div class="col s6"><p class="blue-text center-align"><i class="far fa-2x fa-thumbs-up"></i><br><?php echo $outra->curtidas ?></p></div>
                      </div>
                    </div>
                    <div class="card-action">
                      <div class="right-align">
                        <a class="waves-effect waves-teal btn-flat btn-block blue-text" href="/ver/ave/<?php echo $outra->id ?>">Visitar</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          <?php endforeach ?>
          </div>
      </div>
    </div>  
    <?php endif ?>

    <?php if (!empty($produtos)): ?>
    <div class="row" style="padding: 30px 0">
      <div class="container">
        <div class="row carousel">
          <h3 class="header">Outros Produtos de <?php echo $artigo->nome_usuario ?></h3>
          <?php foreach ($produtos as $pro): ?>
            <div class="col s12 m8 l6 carousel-item">
                <div class="card horizontal">
                  <div class="card-image">
                    <img src="<?php echo !empty($pro->img_principal) ? '/img/'.$pro->img_principal : '/img/pena.jpg' ?>">
                  </div>
                  <div class="card-stacked">
                    <div class="card-content">
                      <span class="card-title truncate"><?php echo $pro->nome ?></span>
                      <div class="row">
                        <div class="col s6"><p class="blue-text center-align"><i class="fas fa-2x fa-eye"></i><br><?php echo $pro->contador ?></p></div>
                        <div class="col s6"><p class="blue-text center-align"><i class="far fa-2x fa-thumbs-up"></i><br><?php echo $pro->curtidas ?></p></div>
                      </div>
                    </div>
                    <div class="card-action">
                      <div class="right-align">
                        <a class="waves-effect waves-teal btn-flat btn-block blue-text" href="/ver/produto/<?php echo $pro->id ?>">Visitar</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          <?php endforeach ?>
        </div>
      </div>
    </div>  
    <?php endif ?>
  </main>
  <?php include 'inc/site-footer.php' ?>

  <div id="modalDenuncia" class="modal">
    <form action="/denuncia/realizar" method="post">
      <div class="modal-content">
          <h4>Denunciar Artigo</h4>
          <p class="red-text">Você está prestes a realizar uma denúncia deste artigo.</p>
            <div class="row">
              <div class="input-field">
                <textarea id="descricao_denuncia" class="materialize-textarea" name="descricao_denuncia" required></textarea>
                <label for="descricao_denuncia">Descreva o motivo de sua denúncia</label>
                <input type="hidden" name="id_artigo" value="<?php echo $artigo->id ?>" />
                <input type="hidden" name="tipo" value="artigo" />
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="waves-effect waves-green btn red">Denunciar</button>
        </div>
      </form>
    </div>

  <script type="text/javascript" src="/js/jquery.min.js"></script>
  <script type="text/javascript" src="/js/materialize.min.js"></script>
  <script src="/js/perfect-scrollbar.min.js"></script>
  <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
  <script type="text/javascript" src="/js/select2.full.min.js"></script>
  <script type="text/javascript" src="/js/i18n/pt-BR.js"></script>
	<script type="text/javascript">

  String.prototype.stripHTML = function() {return this.replace(/<.*?>/g, '');}
	$(function(){
    $('.carousel').carousel();
    $(".button-collapse").sideNav();
    $('.infos .col').each(function(i, obj) {
        conteudo = $(obj).html();
        $(obj).attr("title", conteudo.stripHTML());
    });

		$(window).scroll(function() {
			if($(document).scrollTop() > 40) {
				$( "#top-bird" ).css("height", "0");
				$( "#top-bird" ).css("padding", "0");
			}else{
				$( "#top-bird" ).css("height", "100px");
				$( "#top-bird" ).css("padding", "10px");
			}
			if($(document).scrollTop() < 40){
			}
		});

		$('.slider').slider();
    $('.modal').modal();

    $(".carousel a").click(function(){
      location.href = $(this).attr("href");
    })
	});

  function visitar(){
    $.post("/ver/visitar", {
      tipo: "artigo",
      id: <?php echo $artigo->id ?>
    }, function(result){

    });
  }

  setTimeout(visitar, 5000);
	</script>
  <?php include("inc/site-js.php") ?>
</body>
</html>