<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <title><?php echo $ave->nome ?> - SuperBirds</title>
	<?php include 'inc/site-head.php'; ?>

  <meta property="og:title" content="<?php echo $ave->nome ?> - SuperBirds"/>
  <meta property="og:type" content="product"/>
  <meta property="og:description" content="Veja esta e muitas outras aves no SuperBirds!" />
  <meta property="og:image" content="<?php echo empty(!$ave->img_principal) ? base_url().'img/'.$ave->img_principal : '/img/pena.jpg' ?>" />
  <meta property="og:url" content="<?= base_url()."ver/ave/".$ave->id ?>" />
  <meta property="og:site_name" content="SuperBirds" />

  <link href="/css/select2.css" rel="stylesheet">
  <style type="text/css">
    .fotos{
      height: 200px !important;
    }

    .rating {
      unicode-bidi: bidi-override;
      direction: rtl;
      font-size: 2rem;
    }
    .rating > span {
      display: inline-block;
      position: relative;
      width: 2rem;
      cursor: pointer;
    }
    .rating > span:hover:before,
    .rating > span:hover ~ span:before {
       content: "\2605";
       position: absolute;
       color: #1B5E20;
    }
    .clicado:before{
       content: "\2605";
       position: absolute;
       color: #1B5E20;
    }
    .media:before{
       content: "\2605";
       position: absolute;
       color: gold;
    }
  </style>
</head>
<body class="">
  <?php include 'inc/site-topo.php' ?>
  <main>
    <div class="row grey lighten-5" style="padding: 30px 0 10px 0">
      <div class="container">
        <div class="row">
          <div class="col s10 offset-s1 m6 l3">
            <img class="responsive-img circle materialboxed" src="<?php echo empty(!$ave->img_principal) ? '/img/'.$ave->img_principal : '/img/pena.jpg' ?>" />
            <div class="center-align">
              <div class="rating">
                <span class="voto-5" data-voto="5">☆</span>
                <span class="voto-4" data-voto="4">☆</span>
                <span class="voto-3" data-voto="3">☆</span>
                <span class="voto-2" data-voto="2">☆</span>
                <span class="voto-1" data-voto="1">☆</span>
              </div>
            </div>
          </div>
          <div class="col s12 m6 l9">
            <h3 class="blue-grey-text truncate" style="margin-bottom: 0;">
              <?php echo $ave->nome ?>
            </h3>
            <?php if ($ave->a_avenda): ?>
              <h3 style="margin-top: 0; margin-bottom: 0;">
                <small class="red-text text-darken-4"><small> <i class="fas fa-tag"></i> À Venda</small></small>
              </h3>
            <?php endif ?>
            <?php if (!empty($medias)): ?>
              <?php if ($medias->quantidade > 1): ?>
                <p class="blue-grey-text">Preços encontrados no <b>SuperBirds</b></p>
                <?php if (!empty($medias->min)): ?>
                  <span class="blue-grey-text" style="padding-right: .5rem;"><b>Mínimo</b>: R$<?php echo number_format($medias->min, 2, ',', ' ') ?></span>
                <?php endif ?>
                <?php if (!empty($medias->media)): ?>
                  <span class="blue-grey-text" style="padding-right: .5rem;"><b>Média</b>: R$<?php echo number_format($medias->media, 2, ',', ' ') ?></span>
                <?php endif ?>
                <?php if (!empty($medias->max)): ?>
                  <span class="blue-grey-text" style="padding-right: .5rem;"><b>Máximo</b>: R$<?php echo number_format($medias->max, 2, ',', ' ') ?></span>
                <?php endif ?>
                <?php if (!empty($medias->quantidade)): ?>
                  <span class="blue-grey-text" style="padding-right: .5rem;">Em <b><?php echo $medias->quantidade ?></b> anúncios.</span>
                <?php endif ?>
              <?php endif ?>
            <?php endif ?>
            <?php if (!empty($ave->nome_cidade)): ?>
              <h5 class="blue-grey-text truncate">
                  <?php echo $ave->nome_cidade ?> / <?php echo $ave->uf ?>
              </h5>
            <?php endif ?>
            <?php if (!empty($ave->nome_usuario)): ?>
              <h6 class="blue-grey-text truncate">
                  Criador: <a href="/criadores/ver/<?php echo $ave->id_usuario ?>"><?php echo $ave->nome_usuario ?></a>
              </h6>
            <?php endif ?>
            <?php if (!empty($ave->frase)): ?>
              <blockquote>
                <h5 class="blue-grey-text truncate text-lighten-2">
                    <i class="fas fa-2x fa-quote-left text-lighten-4"></i>
                    <span><?php echo $ave->frase ?></span>
                </h5>
              </blockquote>
            <?php endif ?>
          </div>
          <div class="col s12">
            <div class="row">
              <div class="col s6">
                <p class="blue-text center-align right"><i class="fas fa-2x fa-eye"></i><br>Visitas: <?php echo $ave->contador ?></p>
              </div>
              <div class="col s6">
                <a href="/curtir/anuncio/<?php echo $ave->id ?>">
                  <p class="blue-text center-align left"><i class="far fa-2x fa-thumbs-up"></i><br>Curtidas: <?php echo $ave->curtidas ?></p>
                </a>
              </div>
            </div>
            <?php if ($ave->denunciado != "L"): ?>
              <div class="row">
                <div class="center-align"><button data-target="modalDenuncia" class="btn-flat red-text modal-trigger">Denunciar</button></div>
              </div>
            <?php endif ?>
          </div>
        </div>
      </div>
    </div>
    <?php if (!empty($ave->img_1) || !empty($ave->img_2) || !empty($ave->img_3) || !empty($ave->img_4) || !empty($ave->img_5)): ?>
      <div class="row" style="padding: 30px 0">
        <div class="container">
          <div class="row">
            <div class="carousel fotos">
              <?php if (!empty($ave->img_1)): ?>
                <a class="carousel-item" href="#img1"><img class="materialboxed responsive-img" src="<?php echo '/img/'.$ave->img_1 ?>"></a>
              <?php endif ?>
              <?php if (!empty($ave->img_2)): ?>
                <a class="carousel-item" href="#img2"><img class="materialboxed responsive-img" src="<?php echo '/img/'.$ave->img_2 ?>"></a>
              <?php endif ?>
              <?php if (!empty($ave->img_3)): ?>
                <a class="carousel-item" href="#img3"><img class="materialboxed responsive-img" src="<?php echo '/img/'.$ave->img_3 ?>"></a>
              <?php endif ?>
              <?php if (!empty($ave->img_4)): ?>
                <a class="carousel-item" href="#img4"><img class="materialboxed responsive-img" src="<?php echo '/img/'.$ave->img_4 ?>"></a>
              <?php endif ?>
              <?php if (!empty($ave->img_5)): ?>
                <a class="carousel-item" href="#img5"><img class="materialboxed responsive-img" src="<?php echo '/img/'.$ave->img_5 ?>"></a>
              <?php endif ?>
              </div>
          </div>
        </div>
      </div>
    <?php endif ?>
    <?php if (!empty($ave->descricao)): ?>
      <div class="row grey lighten-5" style="padding: 30px 0">
        <div class="container">
          <div class="row">
              <h3 class="header">Descrição</h3>
              <p class="flow-text"><?php echo $ave->descricao ?></p>
          </div>
        </div>
      </div>
    <?php endif ?>
    <div class="row" style="padding: 30px 0">
      <div class="container">
        <div class="row infos">
          <h3 class="header">Informações</h3>
          <?php if (!empty($ave->a_anilha)): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Anilha:</b> 
                <?php 
                  echo $ave->a_anilha;
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($ave->a_anilhado_em)): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Anilhado em:</b> 
                <?php 
                  echo $ave->a_anilhado_em;
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($ave->a_ibama)): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Ibama:</b> 
                <?php 
                  echo $ave->a_ibama;
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($ave->a_ibamaobs)): ?>
            <div class="col s6 m3 info">
              <p class="">
                <b>Obs. Ibama:</b> 
                <?php 
                  echo $ave->a_ibamaobs;
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($ave->a_sexado)): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Sexo:</b> 
                <?php 
                  if($ave->a_sexado == "NAOINFORMADO"){
                    echo "Não Informado";
                  }else if($ave->a_sexado == "MACHO"){
                    echo "Macho";
                  }else if($ave->a_sexado == "FEMEA"){
                    echo "Fêmea";
                  }
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($ave->a_manso)): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Manso:</b> 
                <?php 
                  if($ave->a_manso == "NAOINFORMADO"){
                    echo "Não Informado";
                  }else if($ave->a_manso == "SIM"){
                    echo "Sim";
                  }else if($ave->a_manso == "NAO"){
                    echo "Não";
                  }
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($ave->entrega)): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Entrega:</b> 
                <?php 
                  if($ave->entrega == "ACOMBINAR"){
                    echo "A Combinar";
                  }else if($ave->entrega == "SIM"){
                    echo "Sim";
                  }else if($ave->entrega == "NAO"){
                    echo "Não";
                  }
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($ave->entrega_obs)): ?>
            <div class="col s6 m3 info">
              <p>
                <b>Obs: </b>
                <?php 
                    echo $ave->entrega_obs;
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($ave->aceita_troca)): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Troca: </b>
                <?php 
                    if($ave->aceita_troca == "ACOMBINAR"){
                    echo "A Combinar";
                  }else if($ave->aceita_troca == "SIM"){
                    echo "Sim";
                  }else if($ave->aceita_troca == "NAO"){
                    echo "Não";
                  }
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($ave->tipo_unidade)): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Quantidade: </b>
                <?php 
                    echo $ave->tipo_unidade;
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($ave->link_externo)): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Link Externo: </b>
                <?php 
                    echo "<a href='$ave->link_externo' class='truncate'>$ave->link_externo</a>";
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($ave->link_comercial)): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Link Comercial: </b>
                <?php 
                    echo "<a href='$ave->link_comercial' class='truncate'>$ave->link_comercial</a>";
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($ave->valor) && $ave->valor_mostrar): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Valor(R$): </b>
                <?php 
                    echo $ave->valor;
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($ave->classe)): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Classe: </b>
                <?php 
                    echo $ave->classe;
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($ave->cor)): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Cores: </b>
                <?php 
                    if(!empty($ave->subcor)){
                      echo $ave->cor.", ".$ave->subcor;
                    }else{
                      echo $ave->cor;
                    }
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($ave->parcela)): ?>
            <div class="col s6 m3 info">
              <p>
                <b>Parcela: </b>
                <?php 
                    echo $ave->parcela;
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($ave->outras_opcoes)): ?>
            <div class="col s6 m3 info">
              <p>
                <b>Outras Opções: </b>
                <?php 
                    echo $ave->outras_opcoes;
                ?>
              </p>
            </div>
          <?php endif ?>
        </div>
      </div>
    </div>
    <?php if (!empty($outras)): ?>
    <div class="row grey lighten-5" style="padding: 30px 0">
      <div class="container">
        <div class="row carousel">
          <h3 class="header">Outras Aves de <?php echo $ave->nome_usuario ?></h3>
          <?php foreach ($outras as $outra): ?>
            <div class="col s12 m8 l6 carousel-item">
                <div class="card horizontal">
                  <div class="card-image">
                    <img src="<?php echo !empty($outra->img_principal) ? '/img/'.$outra->img_principal : '/img/pena.jpg' ?>">
                  </div>
                  <div class="card-stacked">
                    <div class="card-content">
                      <span class="card-title truncate"><?php echo $outra->nome ?></span>
                      <p class='truncate'>Classe: <?php echo $outra->classe ?></p>
                      <p class="truncate">Mutação: <?php echo $outra->mutacao ?></p>
                      <div class="row">
                        <div class="col s6"><p class="blue-text center-align"><i class="fas fa-2x fa-eye"></i><br><?php echo $outra->contador ?></p></div>
                        <div class="col s6"><p class="blue-text center-align"><i class="far fa-2x fa-thumbs-up"></i><br><?php echo $outra->curtidas ?></p></div>
                      </div>
                    </div>
                    <div class="card-action">
                      <div class="right-align">
                        <a class="waves-effect waves-teal btn-flat btn-block blue-text" href="/ver/ave/<?php echo $outra->id ?>">Visitar</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          <?php endforeach ?>
          </div>
      </div>
    </div>  
    <?php endif ?>

    <?php if (!empty($produtos)): ?>
    <div class="row" style="padding: 30px 0">
      <div class="container">
        <div class="row carousel">
          <h3 class="header">Produtos de <?php echo $ave->nome_usuario ?></h3>
          <?php foreach ($produtos as $produto): ?>
            <div class="col s12 m8 l6 carousel-item">
                <div class="card horizontal">
                  <div class="card-image">
                    <img src="<?php echo !empty($produto->img_principal) ? '/img/'.$produto->img_principal : '/img/pena.jpg' ?>">
                  </div>
                  <div class="card-stacked">
                    <div class="card-content">
                      <span class="card-title truncate"><?php echo $produto->nome ?></span>
                      <div class="row">
                        <div class="col s6"><p class="blue-text center-align"><i class="fas fa-2x fa-eye"></i><br><?php echo $produto->contador ?></p></div>
                        <div class="col s6"><p class="blue-text center-align"><i class="far fa-2x fa-thumbs-up"></i><br><?php echo $produto->curtidas ?></p></div>
                      </div>
                    </div>
                    <div class="card-action">
                      <div class="right-align">
                        <a class="waves-effect waves-teal btn-flat btn-block blue-text" href="/ver/produto/<?php echo $produto->id ?>">Visitar</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          <?php endforeach ?>
        </div>
      </div>
    </div>  
    <?php endif ?>
  </main>
  <?php include 'inc/site-footer.php' ?>

  <div id="modalDenuncia" class="modal">
    <form action="/denuncia/realizar" method="post">
      <div class="modal-content">
          <h4>Denunciar Anúncio</h4>
          <p class="red-text">Você está prestes a realizar uma denúncia neste anúncio.</p>
            <div class="row">
              <div class="input-field">
                <textarea id="descricao_denuncia" class="materialize-textarea" name="descricao_denuncia" required></textarea>
                <label for="descricao_denuncia">Descreva o motivo de sua denúncia</label>
                <input type="hidden" name="id_anuncio" value="<?php echo $ave->id ?>" />
                <input type="hidden" name="tipo" value="ave" />
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="waves-effect waves-green btn red">Denunciar</button>
        </div>
      </form>
    </div>

  <script type="text/javascript" src="/js/jquery.min.js"></script>
  <script type="text/javascript" src="/js/materialize.min.js"></script>
  <script src="/js/perfect-scrollbar.min.js"></script>
  <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
  <script type="text/javascript" src="/js/select2.full.min.js"></script>
  <script type="text/javascript" src="/js/i18n/pt-BR.js"></script>
	<script type="text/javascript">

  String.prototype.stripHTML = function() {return this.replace(/<.*?>/g, '');}
	$(function(){
    $('.carousel').carousel();
    $(".button-collapse").sideNav();
    $(document).ready(function(){
        $('.materialboxed').materialbox();
      });

    $('.infos .col').each(function(i, obj) {
        conteudo = $(obj).html();
        $(obj).attr("title", conteudo.stripHTML());
    });

		$(window).scroll(function() {
			if($(document).scrollTop() > 40) {
				$( "#top-bird" ).css("height", "0");
				$( "#top-bird" ).css("padding", "0");
			}else{
				$( "#top-bird" ).css("height", "100px");
				$( "#top-bird" ).css("padding", "10px");
			}
			if($(document).scrollTop() < 40){
			}
		});

		$('.slider').slider();
    $('.modal').modal();

    $(".carousel a").click(function(){
      location.href = $(this).attr("href");
    });

    // -----------VOTOS--------------

    $(".rating span").click(function(){
      // console.log();
      voto = $(this).data("voto");
      $(".rating span").css("color", "inherit");
      $(".rating span").removeClass("clicado");
      for (var i = voto; i >= 1; i--) {
        $(".voto-"+i).css("color", "blue");
        $(".voto-"+i).addClass("clicado");
      }

      $.post("/curtir/votar", {
        id_anuncio: <?php echo $ave->id ?>,
        voto: voto
      }, function(result){
        if(result == ""){
          location.reload();
        }else{
          alert(result);
        }
      });
    });

    <?php
      if(!empty($votos->media)){ ?>

        $(".rating span").css("color", "inherit");
        $(".rating span").removeClass("clicado");
        for (var i = <?php echo floor($votos->media); ?>; i >= 1; i--) {
          $(".voto-"+i).css("color", "gold");
          $(".voto-"+i).addClass("media");
        }

      <?php } ?>
    // -----------VOTOS--------------
	});

  function visitar(){
    $.post("/ver/visitar", {
      tipo: "ave",
      id: <?php echo $ave->id ?>
    }, function(result){

    });
  }

  setTimeout(visitar, 5000);
	</script>
  <?php include("inc/site-js.php") ?>
</body>
</html>