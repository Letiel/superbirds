<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <title><?php echo $criador->nome ?> - SuperBirds</title>
	<?php include 'inc/site-head.php'; ?>
  
  <meta name="description" content="Veja o perfil completo de <?php echo $criador->nome ?> no SuperBirds!">

  <meta property="og:title" content="<?php echo $criador->nome ?> - SuperBirds"/>
  <meta property="og:type" content="product"/>
  <meta property="og:description" content="Veja o perfil completo de <?php echo $criador->nome ?> no SuperBirds!" />
  <meta property="og:image" content="<?php echo empty(!$criador->img_principal) ? '/img/'.$criador->img_principal : '/img/pena.jpg' ?>" />
  <meta property="og:url" content="<?= base_url()."/criadores/ver/".$criador->id ?>" />
  <meta property="og:site_name" content="SuperBirds" />

  <link href="/css/select2.css" rel="stylesheet" />
</head>
<body class="">
  <?php include 'inc/site-topo.php' ?>
  <main>
        <div class="row grey lighten-5" style="padding: 30px 0">
          <div class="container">
            <div class="row">
              <div class="col s10 offset-s1 m6 l3">
                <img class="responsive-img circle materialboxed" src="<?php echo empty(!$criador->img_principal) ? '/img/'.$criador->img_principal : '/img/pena.jpg' ?>" />
                <?php if (($criador->email_form_contato == "U" && !empty($criador->email)) || ($criador->email_form_contato == "S" && !empty($criador->superbirds_email))): ?>
                  <p class="center-align">
                    <button data-target="modal_contato" class="blue-text waves-effect waves-blue btn-flat modal-trigger">Contato</button>
                  </p>
                <?php endif ?>
              </div>
              <div class="col s12 m6 l9">
                <h3 class="blue-grey-text truncate"><?php echo $criador->nome ?></h3>
                <?php if (!empty($criador->nome_cidade)): ?>
                  <h5 class="blue-grey-text truncate">
                      <?php echo $criador->nome_cidade ?> / <?php echo $criador->uf ?>
                  </h5>
                <?php endif ?>
                <?php if (!empty($criador->site)): ?>
                  <h6 class="blue-grey-text truncate">
                      <span><a href="<?php echo $criador->site ?>" target="_blank">Site do criador</a></span>
                  </h6>
                <?php endif ?>
                <?php if (!empty($criador->frase)): ?>
                  <blockquote>
                    <h5 class="blue-grey-text truncate text-lighten-2">
                        <i class="fas fa-2x fa-quote-left text-lighten-4"></i>
                        <span><?php echo $criador->frase ?></span>
                    </h5>
                  </blockquote>
                <?php endif ?>
              </div>
            </div>
              <?php if (!empty($criador->anuncios_principais)): ?>
                <h6 class="center blue-grey-text truncate">
                    <?php echo $criador->anuncios_principais ?>
                </h6>
              <?php endif ?>
            <div class="col s12">
              <div class="row">
                <div class="col s6">
                  <p class="blue-text center-align right"><i class="fas fa-2x fa-eye"></i><br>Visitas: <?php echo $criador->contador ?></p>
                </div>
                <div class="col s6">
                  <a href="/curtir/usuario/<?php echo $criador->id ?>">
                    <p class="blue-text center-align left"><i class="far fa-2x fa-thumbs-up"></i><br>Curtidas: <?php echo $criador->curtidas ?></p>
                  </a>
                </div>
              </div>
              <?php if ($criador->denunciado != "L"): ?>
                <div class="row">
                  <div class="center-align"><button data-target="modalDenuncia" class="btn-flat red-text modal-trigger">Denunciar</button></div>
                </div>
              <?php endif ?>
            </div>
          </div>
        </div>

        <?php if (!empty($criador->img_banner)): ?>
          <div class="row">
            <div class="center-align">
              <img class="responsive-img" style="width: 100%;" src="/img/<?= $criador->img_banner ?>" />
            </div>
          </div>
        <?php endif ?>

        <?php if ($criador->tipo == "CAG"): ?>
          
        <div class="row" style="padding: 30px 0">
          <div class="container">
            <div class="row">
              <div class="col s12">
                <h3 class="blue-grey-text truncate">Informações do Clube / Associação</h3>
                <?php if ($criador->cag_tipo == "A"): ?>
                  <div class="col s6 m6 info">
                    <p class="truncate">
                      <b>Tipo: Amador</b> 
                    </p>
                  </div>
                <?php endif ?>
                <?php if ($criador->cag_tipo == "F"): ?>
                  <div class="col s6 m6 info">
                    <p class="truncate">
                      <b>Tipo: Federação Ornitológica Brasileira</b>
                    </p>
                  </div>
                <?php endif ?>

                <?php if (!empty($criador->cag_sigla)): ?>
                  <div class="col s6 m6 info">
                    <p class="truncate">
                      <b>Sigla: <?php echo $criador->cag_sigla ?></b>
                    </p>
                  </div>
                <?php endif ?>

                <?php if (!empty($criador->cag_anilha)): ?>
                  <div class="col s6 m6 info">
                    <p class="truncate">
                      <b>Anilha: <?php echo $criador->cag_anilha ?></b>
                    </p>
                  </div>
                <?php endif ?>
              </div>
            </div>
          </div>
        </div>
        <?php endif ?>
        
        <?php if (($criador->mostra_cpfcnpj && !empty($criador->cpfcnpj)) || ($criador->mostra_razaosocial && !empty($criador->razaosocial)) || ($criador->mostra_endereco && !empty($criador->endereco)) || ($criador->mostra_fone && !empty($criador->fone)) || ($criador->mostra_cel && !empty($criador->cel)) || ($criador->mostra_whats && !empty($criador->whats)) || ($criador->usu_mostra_listaclubes && !empty($criador->usu_listaclubes))): ?>
          <div class="row" style="padding: 30px 0">
            <div class="container">
              <div class="row">
                <div class="col s12">
                  <h3 class="blue-grey-text truncate">Informações</h3>
                  <?php if ($criador->mostra_cpfcnpj && !empty($criador->cpfcnpj)): ?>
                    <div class="col s6 m6 info">
                      <p class="truncate">
                        <b>CPF/CNPJ: <?php echo $criador->cpfcnpj ?></b> 
                      </p>
                    </div>
                  <?php endif ?>

                  <?php if ($criador->mostra_razaosocial && !empty($criador->razaosocial)): ?>
                    <div class="col s6 m6 info">
                      <p class="truncate">
                        <b>Razão Social: <?php echo $criador->razaosocial ?></b> 
                      </p>
                    </div>
                  <?php endif ?>

                  <?php if ($criador->mostra_endereco && !empty($criador->endereco)): ?>
                    <div class="col s6 m6 info">
                      <p class="truncate">
                        <b>Endereço: <?php echo $criador->endereco ?></b> 
                      </p>
                    </div>
                  <?php endif ?>

                  <?php if ($criador->mostra_fone && !empty($criador->fone)): ?>
                    <div class="col s6 m6 info">
                      <p class="truncate">
                        <b>Telefone: <?php echo $criador->fone ?></b> 
                      </p>
                    </div>
                  <?php endif ?>

                  <?php if ($criador->mostra_cel && !empty($criador->cel)): ?>
                    <div class="col s6 m6 info">
                      <p class="truncate">
                        <b>Celular: <?php echo $criador->cel ?></b> 
                      </p>
                    </div>
                  <?php endif ?>

                  <?php if ($criador->mostra_whats && !empty($criador->whats)): ?>
                    <div class="col s6 m6 info">
                      <p class="truncate">
                        <b>WhatsApp: <?php echo $criador->whats ?></b> 
                      </p>
                    </div>
                  <?php endif ?>

                  <?php if ($criador->usu_mostra_listaclubes && !empty($criador->usu_listaclubes)): ?>
                    <div class="col s6 m6 info">
                      <p class="truncate">
                        <b>Clubes: <?php echo $criador->usu_mostra_listaclubes ?></b> 
                      </p>
                    </div>
                  <?php endif ?>
                </div>
              </div>
            </div>
          </div>
        <?php endif ?>

        <?php if (!empty($aves)): ?>
          <div class="row" style="padding: 30px">
            <h4 class="blue-grey-text truncate">Aves</h4>
            <?php foreach ($aves as $ave): ?>
              <div class="col s12 m12 l4">
                  <div class="card horizontal">
                    <div class="card-image">
                      <img src="<?php echo !empty($ave->img_principal) ? '/img/'.$ave->img_principal : '/img/pena.jpg' ?>">
                    </div>
                    <div class="card-stacked">
                      <div class="card-content">
                        <span class="card-title truncate"><?php echo $ave->nome ?></span>
                        <p class='truncate'>Classe: <?php echo $ave->classe ?></p>
                        <p class="truncate">Mutação: <?php echo $ave->mutacao ?></p>
                        <div class="row">
                          <div class="col s6"><p class="blue-text center-align"><i class="fas fa-2x fa-eye"></i><br><?php echo $ave->contador ?></p></div>
                          <div class="col s6"><p class="blue-text center-align"><i class="far fa-2x fa-thumbs-up"></i><br><?php echo $ave->curtidas ?></p></div>
                        </div>
                      </div>
                      <div class="card-action">
                        <div class="right-align">
                          <a class="waves-effect waves-teal btn-flat btn-block blue-text" href="/ver/ave/<?php echo $ave->id ?>">Visitar</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
            <?php endforeach ?>
          </div>
          <div class="row">
            <div class="col s12 m6 offset-m3 l4 offset-l4">
              <a href="/criadores/aves/<?php echo $criador->id ?>" class="waves-effect waves-teal btn btn-block blue">Ver mais</a>
            </div>
          </div>
        <?php endif ?>
        
        <?php if (!empty($produtos)): ?>
          <div class="row grey lighten-5" style="padding: 30px">
            <h4 class="blue-grey-text truncate">Produtos</h4>
            <?php foreach ($produtos as $produto): ?>
              <div class="col s12 m12 l4">
                <div class="card horizontal">
                  <div class="card-image">
                    <img class="img-responsive" src="<?php echo empty(!$produto->img_principal) ? '/img/'.$produto->img_principal : '/img/pena.jpg' ?>">
                  </div>
                  <div class="card-stacked">
                    <div class="card-content">
                      <span class="card-title truncate"><?php echo $produto->nome ?></span>
                      <div class="row">
                        <div class="col s6"><p class="blue-text center-align"><i class="fas fa-2x fa-eye"></i><br><?php echo $ave->contador ?></p></div>
                        <div class="col s6"><p class="blue-text center-align"><i class="far fa-2x fa-thumbs-up"></i><br><?php echo $ave->curtidas ?></p></div>
                      </div>
                    </div>
                    <div class="card-action">
                      <a class="waves-effect waves-teal btn-flat btn-block blue-text" href="/ver/produto/<?php echo $produto->id ?>">Visitar</a>
                    </div>
                  </div>
                </div>
              </div>
            <?php endforeach ?>
          </div>
          <div class="row">
            <div class="col s12 m6 offset-m3 l4 offset-l4">
              <a href="/criadores/produtos/<?php echo $criador->id ?>" class="waves-effect waves-teal btn btn-block blue">Ver mais</a>
            </div>
          </div>
        <?php endif ?>
      
        <?php if (!empty($servicos)): ?>
          <div class="row" style="padding: 30px">
            <h4 class="blue-grey-text truncate">Serviços</h4>
            <?php foreach ($servicos as $servico): ?>
              <div class="col s12 m12 l4">
                <div class="card horizontal">
                  <div class="card-image">
                    <img src="<?php echo empty(!$servico->img_principal) ? '/img/'.$servico->img_principal : '/img/pena.jpg' ?>">
                  </div>
                  <div class="card-stacked">
                    <div class="card-content">
                      <span class="card-title truncate"><?php echo $servico->nome ?></span>
                      <div class="row">
                        <div class="col s6"><p class="blue-text center-align"><i class="fas fa-2x fa-eye"></i><br><?php echo $ave->contador ?></p></div>
                        <div class="col s6"><p class="blue-text center-align"><i class="far fa-2x fa-thumbs-up"></i><br><?php echo $ave->curtidas ?></p></div>
                      </div>
                    </div>
                    <div class="card-action">
                      <a class="waves-effect waves-teal btn-flat btn-block blue-text" href="/ver/servico/<?php echo $servico->id ?>">Visitar</a>
                    </div>
                  </div>
                </div>
              </div>
            <?php endforeach ?>
          </div>
          <div class="row">
            <div class="col s12 m6 offset-m3 l4 offset-l4">
              <a href="/criadores/servicos/<?php echo $criador->id ?>" class="waves-effect waves-teal btn btn-block blue">Ver mais</a>
            </div>
          </div>
        <?php endif ?>

        <?php if (!empty($artigos)): ?>
          <div class="row" style="padding: 30px">
            <h4 class="blue-grey-text truncate">Artigos</h4>
            <?php foreach ($artigos as $artigo): ?>
              <div class="col s12 m12 l4">
                <div class="card horizontal">
                  <div class="card-image">
                    <img src="<?php echo empty(!$artigo->img_principal) ? '/img/'.$artigo->img_principal : '/img/pena.jpg' ?>">
                  </div>
                  <div class="card-stacked">
                    <div class="card-content">
                      <span class="card-title truncate"><?php echo $artigo->titulo ?></span>
                      <p>
                        <?php echo substr(strip_tags($artigo->descricao), 0, 100);
                        echo strlen(strip_tags($artigo->descricao)) > 100 ? "..." : ""; ?>
                      </p>
                    </div>
                    <div class="card-action">
                      <a class="btn btn-block blue darken-2 waves-effect waves-light" href="/ver/artigo/<?php echo $artigo->id ?>">Visitar</a>
                    </div>
                  </div>
                </div>
              </div>
            <?php endforeach ?>
          </div>
          <div class="row">
            <div class="col s12 m6 offset-m3 l4 offset-l4">
              <a href="/criadores/artigos/<?php echo $criador->id ?>" class="waves-effect waves-teal btn btn-block blue">Ver mais</a>
            </div>
          </div>
        <?php endif ?>

        <?php if (!empty($eventos)): ?>
          <div class="row" style="padding: 30px">
            <h4 class="blue-grey-text truncate">Eventos</h4>
            <?php foreach ($eventos as $evento): ?>
              <div class="col s12 m12 l4">
                <div class="card">
                  <div class="card-image blue-grey lighten-3">
                    <p class="white-text center-align" style="font-size: 100px; padding: 10px 0; margin: 0;"><i class="far fa-calendar-alt"></i></p>
                  </div>
                  <div class="card-stacked">
                    <div class="card-content">
                      <span class="card-title truncate"><?php echo $evento->titulo ?></span>
                      <p>Data: <b><?php echo date("d/m/Y", strtotime($evento->data_evento)) ?></b></p>
                      <p>
                        <?php echo substr(strip_tags($evento->descricao), 0, 100);
                        echo strlen(strip_tags($evento->descricao)) > 100 ? "..." : ""; ?>
                      </p>
                    </div>
                    <div class="card-action">
                      <a class="btn btn-block blue darken-2 waves-effect waves-light" href="/ver/evento/<?php echo $evento->id ?>">Visitar</a>
                    </div>
                  </div>
                </div>
              </div>
            <?php endforeach ?>
          </div>
          <div class="row">
            <div class="col s12 m6 offset-m3 l4 offset-l4">
              <a href="/criadores/eventos/<?php echo $criador->id ?>" class="waves-effect waves-teal btn btn-block blue">Ver mais</a>
            </div>
          </div>
        <?php endif ?>



        <!-- MODAL DE CONTATO -->
        <div id="modal_contato" class="modal modal-fixed-footer">
            <form class="form_contato">
              <div class="modal-content">
                <h4>Contato com <?php echo $criador->nome ?></h4>
                <h5><small>Enviar uma mensagem</small></h5>
                  <div class="row">
                    <div class="col s12 input-field">
                      <input id="contato_nome" type="text" class="validate" required>
                      <label for="nome">Seu nome</label>
                    </div>

                    <div class="col s12 input-field">
                      <input id="contato_email" type="email" class="validate" required>
                      <label for="email">Seu e-mail</label>
                    </div>

                    <div class="col s12 input-field">
                      <textarea id="contato_mensagem" class="materialize-textarea" required></textarea>
                      <label for="mensagem">Sua mensagem</label>
                    </div>
                  </div>
              </div>
              <div class="modal-footer">
                <a href="#!" class="modal-action modal-close waves-effect waves-red btn-flat">Cancelar</a>
                <button class="btn waves-effect waves-green blue" type="submit">Enviar</button>
              </div>
            </form>
          </div>
        <!-- MODAL DE CONTATO -->
	</main>
  <?php include 'inc/site-footer.php' ?>

  <div id="modalDenuncia" class="modal">
    <form action="/denuncia/realizar" method="post">
      <div class="modal-content">
          <h4>Denunciar Criador</h4>
          <p class="red-text">Você está prestes a realizar uma denúncia deste criador.</p>
            <div class="row">
              <div class="input-field">
                <textarea id="descricao_denuncia" class="materialize-textarea" name="descricao_denuncia" required></textarea>
                <label for="descricao_denuncia">Descreva o motivo de sua denúncia</label>
                <input type="hidden" name="id_criador" value="<?php echo $criador->id ?>" />
                <input type="hidden" name="tipo" value="usuario" />
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="waves-effect waves-green btn red">Denunciar</button>
        </div>
      </form>
    </div>

	<script type="text/javascript" src="/js/jquery.min.js"></script>
	<script type="text/javascript" src="/js/materialize.min.js"></script>
	<script src="/js/perfect-scrollbar.min.js"></script>
	<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
  <script type="text/javascript" src="/js/select2.full.min.js"></script>
  <script type="text/javascript" src="/js/i18n/pt-BR.js"></script>
	<script type="text/javascript">

	$(function(){
    $('.modal').modal();
    $(".button-collapse").sideNav();
		$(window).scroll(function() {
			if($(document).scrollTop() > 40) {
				$( "#top-bird" ).css("height", "0");
				$( "#top-bird" ).css("padding", "0");
			}else{
				$( "#top-bird" ).css("height", "100px");
				$( "#top-bird" ).css("padding", "10px");
			}
			if($(document).scrollTop() < 40){
			}
		});

		$('.slider').slider();

    $(".form_contato").submit(function(e){
      e.preventDefault();
      $.post("/criadores/contato", {
        id: <?php echo $criador->id ?>,
        nome: $("#contato_nome").val(),
        email: $("#contato_email").val(),
        mensagem: $("#contato_mensagem").val()
      }, function(result){
        if(result != ""){
          alert(result);
        }else{
          location.reload();
        }
      });
    });
	});

  function visitar(){
    $.post("/ver/visitar", {
      tipo: "usuario",
      id: <?php echo $criador->id ?>
    }, function(result){

    });
  }

  setTimeout(visitar, 5000);
	</script>
  <?php include("inc/site-js.php") ?>
</body>
</html>