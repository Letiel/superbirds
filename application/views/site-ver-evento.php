<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <title><?php echo $evento->titulo ?> - SuperBirds</title>
	<?php include 'inc/site-head.php'; ?>
  <link href="/css/select2.css" rel="stylesheet">
  <style type="text/css">
    .fotos{
      height: 200px !important;
    }
  </style>
</head>
<body class="">
  <?php include 'inc/site-topo.php' ?>
  <main>
    <div class="row grey lighten-5" style="padding: 30px 0">
      <div class="container">
        <div class="row">
          <div class="col s10 offset-s1 m6 l3 white">
            <p class="blue-grey-text center-align" style="font-size: 100px; padding: 10px 0; margin: 0;"><i class="far fa-calendar-alt"></i></p>
          </div>
          
          <div class="col s12 m6 l9">
            <h3 class="blue-grey-text truncate" style="margin-bottom: 0;">
              <?php echo $evento->titulo ?>
            </h3>
            <?php if (!empty($evento->data_evento)): ?>
              <h5 class="blue-grey-text truncate" style="margin-bottom: 0;">
                Data do Evento: <b><?php echo date("d/m/Y", strtotime($evento->data_evento)) ?></b>
              </h5>
            <?php endif ?>
            <?php if (!empty($evento->tipo)): ?>
              <h6 class="blue-grey-text truncate">
                  <?php echo $evento->tipo ?>
              </h6>
            <?php endif ?>
            <?php if (!empty($evento->nome_usuario)): ?>
              <h6 class="blue-grey-text truncate">
                  Por: <a href="/criadores/ver/<?php echo $evento->id_usuario ?>"><?php echo $evento->nome_usuario ?></a>
              </h6>
            <?php endif ?>
            <?php if (!empty($evento->frase)): ?>
              <blockquote>
                <h5 class="blue-grey-text truncate text-lighten-2">
                    <i class="fas fa-2x fa-quote-left text-lighten-4"></i>
                    <span><?php echo $evento->frase ?></span>
                </h5>
              </blockquote>
            <?php endif ?>
          </div>
          <div class="col s12">
            <div class="row">
              <div class="col s6">
                <p class="blue-text center-align right"><i class="fas fa-2x fa-eye"></i><br>Visitas: <?php echo $evento->contador ?></p>
              </div>
              <div class="col s6">
                <a href="/curtir/evento/<?php echo $evento->id ?>">
                  <p class="blue-text center-align left"><i class="far fa-2x fa-thumbs-up"></i><br>Curtidas: <?php echo $evento->curtidas ?></p>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php if (!empty($evento->descricao)): ?>
      <div class="row grey lighten-5" style="padding: 30px 0">
        <div class="container">
          <div class="row">
              <p class="flow-text"><?php echo $evento->descricao ?></p>
          </div>
        </div>
      </div>
    <?php endif ?>
    <?php if (!empty($aves)): ?>
    <div class="row grey lighten-5" style="padding: 30px 0">
      <div class="container">
        <div class="row carousel">
          <h3 class="header">Aves de <?php echo $evento->nome_usuario ?></h3>
          <?php foreach ($aves as $outra): ?>
            <div class="col s12 m8 l6 carousel-item">
                <div class="card horizontal">
                  <div class="card-image">
                    <img src="<?php echo !empty($outra->img_principal) ? '/img/'.$outra->img_principal : '/img/pena.jpg' ?>">
                  </div>
                  <div class="card-stacked">
                    <div class="card-content">
                      <span class="card-title truncate"><?php echo $outra->nome ?></span>
                      <p class='truncate'>Classe: <?php echo $outra->classe ?></p>
                      <p class="truncate">Mutação: <?php echo $outra->mutacao ?></p>
                      <div class="row">
                        <div class="col s6"><p class="blue-text center-align"><i class="fas fa-2x fa-eye"></i><br><?php echo $outra->contador ?></p></div>
                        <div class="col s6"><p class="blue-text center-align"><i class="far fa-2x fa-thumbs-up"></i><br><?php echo $outra->curtidas ?></p></div>
                      </div>
                    </div>
                    <div class="card-action">
                      <div class="right-align">
                        <a class="waves-effect waves-teal btn-flat btn-block blue-text" href="/ver/ave/<?php echo $outra->id ?>">Visitar</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          <?php endforeach ?>
          </div>
      </div>
    </div>  
    <?php endif ?>

    <?php if (!empty($produtos)): ?>
    <div class="row" style="padding: 30px 0">
      <div class="container">
        <div class="row carousel">
          <h3 class="header">Outros Produtos de <?php echo $evento->nome_usuario ?></h3>
          <?php foreach ($produtos as $pro): ?>
            <div class="col s12 m8 l6 carousel-item">
                <div class="card horizontal">
                  <div class="card-image">
                    <img src="<?php echo !empty($pro->img_principal) ? '/img/'.$pro->img_principal : '/img/pena.jpg' ?>">
                  </div>
                  <div class="card-stacked">
                    <div class="card-content">
                      <span class="card-title truncate"><?php echo $pro->nome ?></span>
                      <div class="row">
                        <div class="col s6"><p class="blue-text center-align"><i class="fas fa-2x fa-eye"></i><br><?php echo $pro->contador ?></p></div>
                        <div class="col s6"><p class="blue-text center-align"><i class="far fa-2x fa-thumbs-up"></i><br><?php echo $pro->curtidas ?></p></div>
                      </div>
                    </div>
                    <div class="card-action">
                      <div class="right-align">
                        <a class="waves-effect waves-teal btn-flat btn-block blue-text" href="/ver/produto/<?php echo $pro->id ?>">Visitar</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          <?php endforeach ?>
        </div>
      </div>
    </div>  
    <?php endif ?>
  </main>
  <?php include 'inc/site-footer.php' ?>

  <script type="text/javascript" src="/js/jquery.min.js"></script>
  <script type="text/javascript" src="/js/materialize.min.js"></script>
  <script src="/js/perfect-scrollbar.min.js"></script>
  <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
  <script type="text/javascript" src="/js/select2.full.min.js"></script>
  <script type="text/javascript" src="/js/i18n/pt-BR.js"></script>
	<script type="text/javascript">

  String.prototype.stripHTML = function() {return this.replace(/<.*?>/g, '');}
	$(function(){
    $('.carousel').carousel();
    $(".button-collapse").sideNav();
    $('.infos .col').each(function(i, obj) {
        conteudo = $(obj).html();
        $(obj).attr("title", conteudo.stripHTML());
    });

		$(window).scroll(function() {
			if($(document).scrollTop() > 40) {
				$( "#top-bird" ).css("height", "0");
				$( "#top-bird" ).css("padding", "0");
			}else{
				$( "#top-bird" ).css("height", "100px");
				$( "#top-bird" ).css("padding", "10px");
			}
			if($(document).scrollTop() < 40){
			}
		});

		$('.slider').slider();
    $('.modal').modal();

    $(".carousel a").click(function(){
      location.href = $(this).attr("href");
    })
	});

  function visitar(){
    $.post("/ver/visitar", {
      tipo: "evento",
      id: <?php echo $evento->id ?>
    }, function(result){

    });
  }

  setTimeout(visitar, 5000);
	</script>
  <?php include("inc/site-js.php") ?>
</body>
</html>