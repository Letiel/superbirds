<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <title><?php echo $produto->nome ?> - SuperBirds</title>
	<?php include 'inc/site-head.php'; ?>

  <meta property="og:title" content="<?php echo $produto->nome ?> - SuperBirds"/>
  <meta property="og:type" content="product"/>
  <meta property="og:description" content="Veja este e muitos outros produtos no SuperBirds!" />
  <meta property="og:image" content="<?php echo empty(!$produto->img_principal) ? base_url().'img/'.$produto->img_principal : '/img/pena.jpg' ?>" />
  <meta property="og:url" content="<?= base_url()."ver/produto/".$produto->id ?>" />
  <meta property="og:site_name" content="SuperBirds" />

  <link href="/css/select2.css" rel="stylesheet">
  <style type="text/css">
    .fotos{
      height: 200px !important;
    }
  </style>
</head>
<body class="">
  <?php include 'inc/site-topo.php' ?>
  <main>
    <div class="row grey lighten-5" style="padding: 30px 0">
      <div class="container">
        <div class="row">
          <div class="col s10 offset-s1 m6 l3">
            <img class="responsive-img circle materialboxed" src="<?php echo empty(!$produto->img_principal) ? '/img/'.$produto->img_principal : '/img/pena.jpg' ?>" />
          </div>
          <div class="col s12 m6 l9">
            <h3 class="blue-grey-text truncate" style="margin-bottom: 0;">
              <?php echo $produto->nome ?>
            </h3>
            <?php if ($produto->a_avenda): ?>
              <h3 style="margin-top: 0; margin-bottom: 0;">
                <small class="red-text text-darken-4"><small> <i class="fas fa-tag"></i> À Venda</small></small>
              </h3>
            <?php endif ?>
            <?php if (!empty($produto->nome_cidade)): ?>
              <h5 class="blue-grey-text truncate">
                  <?php echo $produto->nome_cidade ?> / <?php echo $produto->uf ?>
              </h5>
            <?php endif ?>
            <?php if (!empty($produto->nome_usuario)): ?>
              <h6 class="blue-grey-text truncate">
                  Criador: <a href="/criadores/ver/<?php echo $produto->id_usuario ?>"><?php echo $produto->nome_usuario ?></a>
              </h6>
            <?php endif ?>
            <?php if (!empty($produto->frase)): ?>
              <blockquote>
                <h5 class="blue-grey-text truncate text-lighten-2">
                    <i class="fas fa-2x fa-quote-left text-lighten-4"></i>
                    <span><?php echo $produto->frase ?></span>
                </h5>
              </blockquote>
            <?php endif ?>
          </div>
          <div class="col s12">
            <div class="row">
              <div class="col s6">
                <p class="blue-text center-align right"><i class="fas fa-2x fa-eye"></i><br>Visitas: <?php echo $produto->contador ?></p>
              </div>
              <div class="col s6">
                <a href="/curtir/anuncio/<?php echo $produto->id ?>">
                  <p class="blue-text center-align left"><i class="far fa-2x fa-thumbs-up"></i><br>Curtidas: <?php echo $produto->curtidas ?></p>
                </a>
              </div>
            </div>
            <?php if ($produto->denunciado != "L"): ?>
              <div class="row">
                <div class="center-align"><button data-target="modalDenuncia" class="btn-flat red-text modal-trigger">Denunciar</button></div>
              </div>
            <?php endif ?>
          </div>
        </div>
      </div>
    </div>
    <?php if (!empty($produto->img_1) || !empty($produto->img_2) || !empty($produto->img_3) || !empty($produto->img_4) || !empty($produto->img_5)): ?>
      <div class="row" style="padding: 30px 0">
        <div class="container">
          <div class="row">
            <div class="carousel fotos">
              <?php if (!empty($produto->img_1)): ?>
                <a class="carousel-item" href="#img1"><img src="<?php echo '/img/'.$produto->img_1 ?>"></a>
              <?php endif ?>
              <?php if (!empty($produto->img_2)): ?>
                <a class="carousel-item" href="#img2"><img src="<?php echo '/img/'.$produto->img_2 ?>"></a>
              <?php endif ?>
              <?php if (!empty($produto->img_3)): ?>
                <a class="carousel-item" href="#img3"><img src="<?php echo '/img/'.$produto->img_3 ?>"></a>
              <?php endif ?>
              <?php if (!empty($produto->img_4)): ?>
                <a class="carousel-item" href="#img4"><img src="<?php echo '/img/'.$produto->img_4 ?>"></a>
              <?php endif ?>
              <?php if (!empty($produto->img_5)): ?>
                <a class="carousel-item" href="#img5"><img src="<?php echo '/img/'.$produto->img_5 ?>"></a>
              <?php endif ?>
              </div>
          </div>
        </div>
      </div>
    <?php endif ?>
    <div class="row grey lighten-5" style="padding: 30px 0">
      <div class="container">
        <div class="row">
          <?php if (!empty($produto->descricao)): ?>
            <h3 class="header">Descrição</h3>
            <p class="flow-text"><?php echo $produto->descricao ?></p>
          <?php endif ?>
        </div>
      </div>
    </div>
    <div class="row" style="padding: 30px 0">
      <div class="container">
        <div class="row infos">
          <h3 class="header">Informações</h3>
          <?php if (!empty($produto->p_quantidade)): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Quantidade:</b> 
                <?php 
                  echo $produto->p_quantidade;
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($produto->a_anilha)): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Anilha:</b> 
                <?php 
                  echo $produto->a_anilha;
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($produto->a_anilhado_em)): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Anilhado em:</b> 
                <?php 
                  echo $produto->a_anilhado_em;
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($produto->a_ibama)): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Ibama:</b> 
                <?php 
                  echo $produto->a_ibama;
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($produto->a_ibamaobs)): ?>
            <div class="col s6 m3 info">
              <p class="">
                <b>Obs. Ibama:</b> 
                <?php 
                  echo $produto->a_ibamaobs;
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($produto->a_sexado)): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Sexo:</b> 
                <?php 
                  if($produto->a_sexado == "NAOINFORMADO"){
                    echo "Não Informado";
                  }else if($produto->a_sexado == "MACHO"){
                    echo "Macho";
                  }else if($produto->a_sexado == "FEMEA"){
                    echo "Fêmea";
                  }
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($produto->a_manso)): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Manso:</b> 
                <?php 
                  if($produto->a_manso == "NAOINFORMADO"){
                    echo "Não Informado";
                  }else if($produto->a_manso == "SIM"){
                    echo "Sim";
                  }else if($produto->a_manso == "NAO"){
                    echo "Não";
                  }
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($produto->entrega)): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Entrega:</b> 
                <?php 
                  if($produto->entrega == "ACOMBINAR"){
                    echo "A Combinar";
                  }else if($produto->entrega == "SIM"){
                    echo "Sim";
                  }else if($produto->entrega == "NAO"){
                    echo "Não";
                  }
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($produto->entrega_obs)): ?>
            <div class="col s6 m3 info">
              <p>
                <b>Obs: </b>
                <?php 
                    echo $produto->entrega_obs;
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($produto->aceita_troca)): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Troca: </b>
                <?php 
                    if($produto->aceita_troca == "ACOMBINAR"){
                    echo "A Combinar";
                  }else if($produto->aceita_troca == "SIM"){
                    echo "Sim";
                  }else if($produto->aceita_troca == "NAO"){
                    echo "Não";
                  }
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($produto->link_externo)): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Link Externo: </b>
                <?php 
                    echo "<a href='$produto->link_externo' class='truncate'>$produto->link_externo</a>";
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($produto->link_comercial)): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Link Comercial: </b>
                <?php 
                    echo "<a href='$produto->link_comercial' class='truncate'>$produto->link_comercial</a>";
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($produto->valor) && $produto->valor_mostrar): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Valor(R$): </b>
                <?php 
                    echo $produto->valor;
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($produto->classe)): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Classe: </b>
                <?php 
                    echo $produto->classe;
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($produto->cor)): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Cor: </b>
                <?php 
                    if(!empty($produto->subcor)){
                      echo $produto->cor.", ".$produto->subcor;
                    }else{
                      echo $produto->cor;
                    }
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($produto->parcela)): ?>
            <div class="col s6 m3 info">
              <p>
                <b>Parcela: </b>
                <?php 
                    echo $produto->parcela;
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($produto->outras_opcoes)): ?>
            <div class="col s6 m3 info">
              <p>
                <b>Outras Opções: </b>
                <?php 
                    echo $produto->outras_opcoes;
                ?>
              </p>
            </div>
          <?php endif ?>
        </div>
      </div>
    </div>
    <?php if (!empty($aves)): ?>
    <div class="row grey lighten-5" style="padding: 30px 0">
      <div class="container">
        <div class="row carousel">
          <h3 class="header">Aves de <?php echo $produto->nome_usuario ?></h3>
          <?php foreach ($aves as $outra): ?>
            <div class="col s12 m8 l6 carousel-item">
                <div class="card horizontal">
                  <div class="card-image">
                    <img src="<?php echo !empty($outra->img_principal) ? '/img/'.$outra->img_principal : '/img/pena.jpg' ?>">
                  </div>
                  <div class="card-stacked">
                    <div class="card-content">
                      <span class="card-title truncate"><?php echo $outra->nome ?></span>
                      <p class='truncate'>Classe: <?php echo $outra->classe ?></p>
                      <p class="truncate">Mutação: <?php echo $outra->mutacao ?></p>
                      <div class="row">
                        <div class="col s6"><p class="blue-text center-align"><i class="fas fa-2x fa-eye"></i><br><?php echo $outra->contador ?></p></div>
                        <div class="col s6"><p class="blue-text center-align"><i class="far fa-2x fa-thumbs-up"></i><br><?php echo $outra->curtidas ?></p></div>
                      </div>
                    </div>
                    <div class="card-action">
                      <div class="right-align">
                        <a class="waves-effect waves-teal btn-flat btn-block blue-text" href="/ver/ave/<?php echo $outra->id ?>">Visitar</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          <?php endforeach ?>
          </div>
      </div>
    </div>  
    <?php endif ?>

    <?php if (!empty($produtos)): ?>
    <div class="row" style="padding: 30px 0">
      <div class="container">
        <div class="row carousel">
          <h3 class="header">Outros Produtos de <?php echo $produto->nome_usuario ?></h3>
          <?php foreach ($produtos as $pro): ?>
            <div class="col s12 m8 l6 carousel-item">
                <div class="card horizontal">
                  <div class="card-image">
                    <img src="<?php echo !empty($pro->img_principal) ? '/img/'.$pro->img_principal : '/img/pena.jpg' ?>">
                  </div>
                  <div class="card-stacked">
                    <div class="card-content">
                      <span class="card-title truncate"><?php echo $pro->nome ?></span>
                      <div class="row">
                        <div class="col s6"><p class="blue-text center-align"><i class="fas fa-2x fa-eye"></i><br><?php echo $pro->contador ?></p></div>
                        <div class="col s6"><p class="blue-text center-align"><i class="far fa-2x fa-thumbs-up"></i><br><?php echo $pro->curtidas ?></p></div>
                      </div>
                    </div>
                    <div class="card-action">
                      <div class="right-align">
                        <a class="waves-effect waves-teal btn-flat btn-block blue-text" href="/ver/produto/<?php echo $pro->id ?>">Visitar</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          <?php endforeach ?>
        </div>
      </div>
    </div>  
    <?php endif ?>
  </main>
  <?php include 'inc/site-footer.php' ?>

  <div id="modalDenuncia" class="modal">
    <form action="/denuncia/realizar" method="post">
      <div class="modal-content">
          <h4>Denunciar Anúncio</h4>
          <p>Você está prestes a realizar uma denúncia neste anúncio.</p>
            <div class="row">
              <div class="input-field">
                <textarea id="descricao_denuncia" class="materialize-textarea" name="descricao_denuncia" required></textarea>
                <label for="descricao_denuncia">Descreva o motivo de sua denúncia</label>
                <input type="hidden" name="id_anuncio" value="<?php echo $produto->id ?>" />
                <input type="hidden" name="tipo" value="produto" />
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="waves-effect waves-green btn red">Denunciar</button>
        </div>
      </form>
    </div>

  <script type="text/javascript" src="/js/jquery.min.js"></script>
  <script type="text/javascript" src="/js/materialize.min.js"></script>
  <script src="/js/perfect-scrollbar.min.js"></script>
  <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
  <script type="text/javascript" src="/js/select2.full.min.js"></script>
  <script type="text/javascript" src="/js/i18n/pt-BR.js"></script>
	<script type="text/javascript">

  String.prototype.stripHTML = function() {return this.replace(/<.*?>/g, '');}
	$(function(){
    $('.carousel').carousel();
    $(".button-collapse").sideNav();
    $('.infos .col').each(function(i, obj) {
        conteudo = $(obj).html();
        $(obj).attr("title", conteudo.stripHTML());
    });

		$(window).scroll(function() {
			if($(document).scrollTop() > 40) {
				$( "#top-bird" ).css("height", "0");
				$( "#top-bird" ).css("padding", "0");
			}else{
				$( "#top-bird" ).css("height", "100px");
				$( "#top-bird" ).css("padding", "10px");
			}
			if($(document).scrollTop() < 40){
			}
		});

		$('.slider').slider();
    $('.modal').modal();

    $(".carousel a").click(function(){
      location.href = $(this).attr("href");
    })
	});
  function visitar(){
    $.post("/ver/visitar", {
      tipo: "produto",
      id: <?php echo $produto->id ?>
    }, function(result){

    });
  }
  setTimeout(visitar, 5000);
	</script>
  <?php include("inc/site-js.php") ?>
</body>
</html>