<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <title><?php echo $servico->nome ?> - SuperBirds</title>
	<?php include 'inc/site-head.php'; ?>

  <meta property="og:title" content="<?php echo $servico->nome ?> - SuperBirds"/>
  <meta property="og:type" content="product"/>
  <meta property="og:description" content="Veja este e muitos outros serviços no SuperBirds!" />
  <meta property="og:image" content="<?php echo empty(!$servico->img_principal) ? base_url().'img/'.$servico->img_principal : '/img/pena.jpg' ?>" />
  <meta property="og:url" content="<?= base_url()."ver/servico/".$servico->id ?>" />
  <meta property="og:site_name" content="SuperBirds" />

  <link href="/css/select2.css" rel="stylesheet">
  <style type="text/css">
    .fotos{
      height: 200px !important;
    }
  </style>
</head>
<body class="">
  <?php include 'inc/site-topo.php' ?>
  <main>
    <div class="row grey lighten-5" style="padding: 30px 0">
      <div class="container">
        <div class="row">
          <div class="col s10 offset-s1 m6 l3">
            <img class="responsive-img circle materialboxed" src="<?php echo empty(!$servico->img_principal) ? '/img/'.$servico->img_principal : '/img/pena.jpg' ?>" />
          </div>
          <div class="col s12 m6 l9">
            <h3 class="blue-grey-text truncate" style="margin-bottom: 0;">
              <?php echo $servico->nome ?>
            </h3>
            <?php if ($servico->a_avenda): ?>
              <h3 style="margin-top: 0; margin-bottom: 0;">
                <small class="red-text text-darken-4"><small> <i class="fas fa-tag"></i> À Venda</small></small>
              </h3>
            <?php endif ?>
            <?php if (!empty($servico->nome_cidade)): ?>
              <h5 class="blue-grey-text truncate">
                  <?php echo $servico->nome_cidade ?> / <?php echo $servico->uf ?>
              </h5>
            <?php endif ?>
            <?php if (!empty($servico->nome_usuario)): ?>
              <h6 class="blue-grey-text truncate">
                  Criador: <a href="/criadores/ver/<?php echo $servico->id_usuario ?>"><?php echo $servico->nome_usuario ?></a>
              </h6>
            <?php endif ?>
            <?php if (!empty($servico->frase)): ?>
              <blockquote>
                <h5 class="blue-grey-text truncate text-lighten-2">
                    <i class="fas fa-2x fa-quote-left text-lighten-4"></i>
                    <span><?php echo $servico->frase ?></span>
                </h5>
              </blockquote>
            <?php endif ?>
          </div>
          <div class="col s12">
            <div class="row">
              <div class="col s6">
                <p class="blue-text center-align right"><i class="fas fa-2x fa-eye"></i><br>Visitas: <?php echo $servico->contador ?></p>
              </div>
              <div class="col s6">
                <a href="/curtir/anuncio/<?php echo $servico->id ?>">
                  <p class="blue-text center-align left"><i class="far fa-2x fa-thumbs-up"></i><br>Curtidas: <?php echo $servico->curtidas ?></p>
                </a>
              </div>
            </div>
            <?php if ($servico->denunciado != "L"): ?>
              <div class="row">
                <div class="center-align"><button data-target="modalDenuncia" class="btn-flat red-text modal-trigger">Denunciar</button></div>
              </div>
            <?php endif ?>
          </div>
        </div>
      </div>
    </div>
    <?php if (!empty($servico->img_1) || !empty($servico->img_2) || !empty($servico->img_3) || !empty($servico->img_4) || !empty($servico->img_5)): ?>
      <div class="row" style="padding: 30px 0">
        <div class="container">
          <div class="row">
            <div class="carousel fotos">
              <?php if (!empty($servico->img_1)): ?>
                <a class="carousel-item" href="#img1"><img src="<?php echo '/img/'.$servico->img_1 ?>"></a>
              <?php endif ?>
              <?php if (!empty($servico->img_2)): ?>
                <a class="carousel-item" href="#img2"><img src="<?php echo '/img/'.$servico->img_2 ?>"></a>
              <?php endif ?>
              <?php if (!empty($servico->img_3)): ?>
                <a class="carousel-item" href="#img3"><img src="<?php echo '/img/'.$servico->img_3 ?>"></a>
              <?php endif ?>
              <?php if (!empty($servico->img_4)): ?>
                <a class="carousel-item" href="#img4"><img src="<?php echo '/img/'.$servico->img_4 ?>"></a>
              <?php endif ?>
              <?php if (!empty($servico->img_5)): ?>
                <a class="carousel-item" href="#img5"><img src="<?php echo '/img/'.$servico->img_5 ?>"></a>
              <?php endif ?>
              </div>
          </div>
        </div>
      </div>
    <?php endif ?>
    <?php if (!empty($servico->descricao)): ?>
      <div class="row grey lighten-5" style="padding: 30px 0">
        <div class="container">
          <div class="row">
              <h3 class="header">Descrição</h3>
              <p class="flow-text"><?php echo $servico->descricao ?></p>
          </div>
        </div>
      </div>
    <?php endif ?>
    <div class="row" style="padding: 30px 0">
      <div class="container">
        <div class="row infos">
          <h3 class="header">Informações</h3>
          <?php if (!empty($servico->p_quantidade)): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Quantidade:</b> 
                <?php 
                  echo $servico->p_quantidade;
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($servico->a_anilha)): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Anilha:</b> 
                <?php 
                  echo $servico->a_anilha;
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($servico->a_anilhado_em)): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Anilhado em:</b> 
                <?php 
                  echo $servico->a_anilhado_em;
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($servico->a_ibama)): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Ibama:</b> 
                <?php 
                  echo $servico->a_ibama;
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($servico->a_ibamaobs)): ?>
            <div class="col s6 m3 info">
              <p class="">
                <b>Obs. Ibama:</b> 
                <?php 
                  echo $servico->a_ibamaobs;
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($servico->a_sexado)): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Sexo:</b> 
                <?php 
                  if($servico->a_sexado == "NAOINFORMADO"){
                    echo "Não Informado";
                  }else if($servico->a_sexado == "MACHO"){
                    echo "Macho";
                  }else if($servico->a_sexado == "FEMEA"){
                    echo "Fêmea";
                  }
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($servico->a_manso)): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Manso:</b> 
                <?php 
                  if($servico->a_manso == "NAOINFORMADO"){
                    echo "Não Informado";
                  }else if($servico->a_manso == "SIM"){
                    echo "Sim";
                  }else if($servico->a_manso == "NAO"){
                    echo "Não";
                  }
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($servico->entrega)): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Entrega:</b> 
                <?php 
                  if($servico->entrega == "ACOMBINAR"){
                    echo "A Combinar";
                  }else if($servico->entrega == "SIM"){
                    echo "Sim";
                  }else if($servico->entrega == "NAO"){
                    echo "Não";
                  }
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($servico->entrega_obs)): ?>
            <div class="col s6 m3 info">
              <p>
                <b>Obs: </b>
                <?php 
                    echo $servico->entrega_obs;
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($servico->aceita_troca)): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Troca: </b>
                <?php 
                    if($servico->aceita_troca == "ACOMBINAR"){
                    echo "A Combinar";
                  }else if($servico->aceita_troca == "SIM"){
                    echo "Sim";
                  }else if($servico->aceita_troca == "NAO"){
                    echo "Não";
                  }
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($servico->link_externo)): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Link Externo: </b>
                <?php 
                    echo "<a href='$servico->link_externo' class='truncate'>$servico->link_externo</a>";
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($servico->link_comercial)): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Link Comercial: </b>
                <?php 
                    echo "<a href='$servico->link_comercial' class='truncate'>$servico->link_comercial</a>";
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($servico->valor) && $servico->valor_mostrar): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Valor(R$): </b>
                <?php 
                    echo $servico->valor;
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($servico->classe)): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Classe: </b>
                <?php 
                    echo $servico->classe;
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($servico->cor)): ?>
            <div class="col s6 m3 info">
              <p class="truncate">
                <b>Cor: </b>
                <?php 
                    if(!empty($servico->subcor)){
                      echo $servico->cor.", ".$servico->subcor;
                    }else{
                      echo $servico->cor;
                    }
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($servico->parcela)): ?>
            <div class="col s6 m3 info">
              <p>
                <b>Parcela: </b>
                <?php 
                    echo $servico->parcela;
                ?>
              </p>
            </div>
          <?php endif ?>

          <?php if (!empty($servico->outras_opcoes)): ?>
            <div class="col s6 m3 info">
              <p>
                <b>Outras Opções: </b>
                <?php 
                    echo $servico->outras_opcoes;
                ?>
              </p>
            </div>
          <?php endif ?>
        </div>
      </div>
    </div>
    <?php if (!empty($aves)): ?>
    <div class="row grey lighten-5" style="padding: 30px 0">
      <div class="container">
        <div class="row carousel">
          <h3 class="header">Aves de <?php echo $servico->nome_usuario ?></h3>
          <?php foreach ($aves as $outra): ?>
            <div class="col s12 m8 l6 carousel-item">
                <div class="card horizontal">
                  <div class="card-image">
                    <img src="<?php echo !empty($outra->img_principal) ? '/img/'.$outra->img_principal : '/img/pena.jpg' ?>">
                  </div>
                  <div class="card-stacked">
                    <div class="card-content">
                      <span class="card-title truncate"><?php echo $outra->nome ?></span>
                      <p class='truncate'>Classe: <?php echo $outra->classe ?></p>
                      <p class="truncate">Mutação: <?php echo $outra->mutacao ?></p>
                      <div class="row">
                        <div class="col s6"><p class="blue-text center-align"><i class="fas fa-2x fa-eye"></i><br><?php echo $outra->contador ?></p></div>
                        <div class="col s6"><p class="blue-text center-align"><i class="far fa-2x fa-thumbs-up"></i><br><?php echo $outra->curtidas ?></p></div>
                      </div>
                    </div>
                    <div class="card-action">
                      <div class="right-align">
                        <a class="waves-effect waves-teal btn-flat btn-block blue-text" href="/ver/ave/<?php echo $outra->id ?>">Visitar</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          <?php endforeach ?>
          </div>
      </div>
    </div>  
    <?php endif ?>

    <?php if (!empty($produtos)): ?>
    <div class="row" style="padding: 30px 0">
      <div class="container">
        <div class="row carousel">
          <h3 class="header">Outros Produtos de <?php echo $servico->nome_usuario ?></h3>
          <?php foreach ($produtos as $pro): ?>
            <div class="col s12 m8 l6 carousel-item">
                <div class="card horizontal">
                  <div class="card-image">
                    <img src="<?php echo !empty($pro->img_principal) ? '/img/'.$pro->img_principal : '/img/pena.jpg' ?>">
                  </div>
                  <div class="card-stacked">
                    <div class="card-content">
                      <span class="card-title truncate"><?php echo $pro->nome ?></span>
                      <div class="row">
                        <div class="col s6"><p class="blue-text center-align"><i class="fas fa-2x fa-eye"></i><br><?php echo $pro->contador ?></p></div>
                        <div class="col s6"><p class="blue-text center-align"><i class="far fa-2x fa-thumbs-up"></i><br><?php echo $pro->curtidas ?></p></div>
                      </div>
                    </div>
                    <div class="card-action">
                      <div class="right-align">
                        <a class="waves-effect waves-teal btn-flat btn-block blue-text" href="/ver/produto/<?php echo $pro->id ?>">Visitar</a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          <?php endforeach ?>
        </div>
      </div>
    </div>  
    <?php endif ?>
  </main>
  <?php include 'inc/site-footer.php' ?>

  <div id="modalDenuncia" class="modal">
    <form action="/denuncia/realizar" method="post">
      <div class="modal-content">
          <h4>Denunciar Anúncio</h4>
          <p>Você está prestes a realizar uma denúncia neste anúncio.</p>
            <div class="row">
              <div class="input-field">
                <textarea id="descricao_denuncia" class="materialize-textarea" name="descricao_denuncia" required></textarea>
                <label for="descricao_denuncia">Descreva o motivo de sua denúncia</label>
                <input type="hidden" name="id_anuncio" value="<?php echo $servico->id ?>" />
                <input type="hidden" name="tipo" value="servico" />
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="waves-effect waves-green btn red">Denunciar</button>
        </div>
      </form>
    </div>

  <script type="text/javascript" src="/js/jquery.min.js"></script>
  <script type="text/javascript" src="/js/materialize.min.js"></script>
  <script src="/js/perfect-scrollbar.min.js"></script>
  <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
  <script type="text/javascript" src="/js/select2.full.min.js"></script>
  <script type="text/javascript" src="/js/i18n/pt-BR.js"></script>
	<script type="text/javascript">

  String.prototype.stripHTML = function() {return this.replace(/<.*?>/g, '');}
	$(function(){
    $('.carousel').carousel();
    $(".button-collapse").sideNav();
    $('.infos .col').each(function(i, obj) {
        conteudo = $(obj).html();
        $(obj).attr("title", conteudo.stripHTML());
    });

		$(window).scroll(function() {
			if($(document).scrollTop() > 40) {
				$( "#top-bird" ).css("height", "0");
				$( "#top-bird" ).css("padding", "0");
			}else{
				$( "#top-bird" ).css("height", "100px");
				$( "#top-bird" ).css("padding", "10px");
			}
			if($(document).scrollTop() < 40){
			}
		});

		$('.slider').slider();
    $('.modal').modal();

    $(".carousel a").click(function(){
      location.href = $(this).attr("href");
    })
	});

  function visitar(){
    $.post("/ver/visitar", {
      tipo: "servico",
      id: <?php echo $servico->id ?>
    }, function(result){

    });
  }

  setTimeout(visitar, 5000);
	</script>
  <?php include("inc/site-js.php") ?>
</body>
</html>