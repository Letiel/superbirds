<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <title>Quem Somos- SuperBirds</title>
	<?php include 'inc/site-head.php'; ?>

  <meta property="og:title" content="Quem Somos - SuperBirds"/>
  <meta property="og:type" content="product"/>
  <meta property="og:description" content="Veja este e muitos outros serviços no SuperBirds!" />
  <meta property="og:image" content="" />
  <meta property="og:url" content="<?= base_url()."quem_somos" ?>" />
  <meta property="og:site_name" content="SuperBirds" />

  <link href="/css/select2.css" rel="stylesheet">
  <style type="text/css">
    .fotos{
      height: 200px !important;
    }
  </style>
</head>
<body class="">
  <?php include 'inc/site-topo.php' ?>
  <main>
  
  <div class="row" style="margin-top:30px;">
    <div class="col xl4 l4 m4 s6 offset-xl4 offset-l4 offset-m4 offset-s3">
      <img src="/img/superbirds.png" class="responsive-img" />
    </div>
  </div>
    
  <div class="row">
    <div class="col s12 xl10 offset-xl1 l10 offset-l1 m10 offset-m1">
      <h4 class="center-align">Quem Somos</h4>
      <p class="flow-text">
        
        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
      consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
      cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
      proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        
      </p>
      <p class="flow-text">
        
        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
      consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
      cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
      proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
      consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
      cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
      proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

      </p>
    </div>
  </div>

  
  </main>
  <?php include 'inc/site-footer.php' ?>

  

  <script type="text/javascript" src="/js/jquery.min.js"></script>
  <script type="text/javascript" src="/js/materialize.min.js"></script>
  <script src="/js/perfect-scrollbar.min.js"></script>
  <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
  <script type="text/javascript" src="/js/select2.full.min.js"></script>
  <script type="text/javascript" src="/js/i18n/pt-BR.js"></script>
	<script type="text/javascript">

  String.prototype.stripHTML = function() {return this.replace(/<.*?>/g, '');}
	$(function(){
    $('.carousel').carousel();
    $(".button-collapse").sideNav();
    $('.infos .col').each(function(i, obj) {
        conteudo = $(obj).html();
        $(obj).attr("title", conteudo.stripHTML());
    });

		$(window).scroll(function() {
			if($(document).scrollTop() > 40) {
				$( "#top-bird" ).css("height", "0");
				$( "#top-bird" ).css("padding", "0");
			}else{
				$( "#top-bird" ).css("height", "100px");
				$( "#top-bird" ).css("padding", "10px");
			}
			if($(document).scrollTop() < 40){
			}
		});

		$('.slider').slider();
    $('.modal').modal();

    $(".carousel a").click(function(){
      location.href = $(this).attr("href");
    })
	});

  function visitar(){
    $.post("/ver/visitar", {
      tipo: "servico",
      id: <?php echo $servico->id ?>
    }, function(result){

    });
  }

  setTimeout(visitar, 5000);
	</script>
  <?php include("inc/site-js.php") ?>
</body>
</html>