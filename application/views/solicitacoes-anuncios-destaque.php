<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<title>Solicitar Anúncios Destaque - SuperBirds</title>
	<?php include 'inc/interno-head.php' ?>
	<style type="text/css">
		.btn-block{
			width: 100%;
		}
	</style>
</head>
<body class="blue-grey lighten-5">
	<?php include 'inc/interno-menu_lateral.php' ?>
	<?php include 'inc/interno-topo.php' ?>
	<main>
		<div class="container-fluid">
			<div class="card">
				<div class="card-content">
					<div class="left">
						<p class="card-title blue-grey-text">Solicitar Anúncios Destaque</p>
					</div>
					<div class="clearfix"></div>
					<hr>
					<div class="row">
						<form method="post">
							<div class="col s12">
								<div class="carousel">
									<?php foreach ($planos as $plano): ?>
										<div class="col s6 m3 carousel-item" data-id="<?php echo $plano->id ?>">
											<div class="card <?php echo $plano->planofree == 0 ? 'blue darken-3' : 'blue' ?>">
												<div class="card-content white-text">
													<span class="card-title"><?php echo $plano->nome ?></span>
													<hr />
													<p><?php echo $plano->descricao ?></p>
													<p>Qtd. Anúncios Destaque: <b><?php echo $plano->num_anuncios_destaque ?></b></p>
													<p><b>R$ <?php echo number_format($plano->valor, 2, ",", " ") ?></b></p>
												</div>
											</div>
										</div>
									<?php endforeach ?>
								</div>
								<input type="hidden" name="id_tab_valores" id="id_tab_valores" style="display: none" />
								<div class="input-field col s12">
									<button type="submit" class="btn btn-block blue">Solicitar</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</main>
	<?php include 'inc/interno-footer.php' ?>
	<?php include 'inc/interno-js.php' ?>
	<script type="text/javascript">
		$(document).ready(function(){
			Materialize.updateTextFields();
			$('select').material_select();
			$('.carousel').carousel({
			    indicators: true,
			    onCycleTo: function(data) {
			      id = $(data).data().id;
			      $("#id_tab_valores").val(id);
			   }
			});
		});
	</script>
</body>
</html>