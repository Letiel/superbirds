<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<title>Solicitar Evento - SuperBirds</title>
	<?php include 'inc/interno-head.php' ?>
	<style type="text/css">
		.btn-block{
			width: 100%;
		}
	</style>
</head>
<body class="blue-grey lighten-5">
	<?php include 'inc/interno-menu_lateral.php' ?>
	<?php include 'inc/interno-topo.php' ?>
	<main>
		<div class="container-fluid">
			<div class="card">
				<div class="card-content">
					<div class="left">
						<p class="card-title blue-grey-text">Solicitar Evento</p>
					</div>
					<div class="clearfix"></div>
					<hr>
					<div class="row">
						<form method="post">
							<div class="input-field col s12">
								<textarea name="solicitacao" id="solicitacao" class="materialize-textarea"><?php echo set_value("solicitacao") ?></textarea>
								<label for="solicitacao">Descrição</label>
								<p class="blue-grey-text">Descreva seu pedido de cadastro de <b>evento</b>. Em breve entraremos em contato por e-mail.</p>
								<p class="red-text"><?php echo form_error("solicitacao") ?></p>
							</div>
							<div class="input-field col s12">
								<button type="submit" class="btn btn-block blue">Solicitar</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</main>
	<?php include 'inc/interno-footer.php' ?>
	<?php include 'inc/interno-js.php' ?>
	<script type="text/javascript">
		$(document).ready(function(){
			$('textarea').trigger('autoresize');
		});
	</script>
</body>
</html>