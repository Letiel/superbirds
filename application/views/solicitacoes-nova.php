<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<title>Tipo de Solicitação - SuperBirds</title>
	<?php include 'inc/interno-head.php' ?>
</head>
<body class="blue-grey lighten-5">
	<?php include 'inc/interno-menu_lateral.php' ?>
	<?php include 'inc/interno-topo.php' ?>
	<main>
		<div class="container-fluid">
			<div class="card">
				<div class="card-content">
					<div class="left">
						<p class="card-title blue-grey-text">Qual o tipo de sua solicitação?</p>
					</div>
					<div class="clearfix"></div>
					<hr>
					<div class="row">
						<div class="col s12">
							<a href="/solicitacoes/planos" class="btn blue white-text btn-block waves-effect waves-teal">PLANO</a>
						</div>
					</div>
					<div class="row">
						<div class="col s12">
							<a href="/solicitacoes/artigos" class="btn blue white-text btn-block waves-effect waves-teal">ARTIGO</a>
						</div>
					</div>
					<div class="row">
						<div class="col s12">
							<a href="/solicitacoes/eventos" class="btn blue white-text btn-block waves-effect waves-teal">EVENTO</a>
						</div>
					</div>
					<div class="row">
						<div class="col s12">
							<a href="/solicitacoes/anuncios-destaque" class="btn blue white-text btn-block waves-effect waves-teal">ANÚNCIO DESTAQUE</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>

	<?php include 'inc/interno-footer.php' ?>
	<?php include 'inc/interno-js.php' ?>
	<script type="text/javascript">
		$(document).ready(function(){
			Materialize.updateTextFields();
			$('select').material_select();
		});
	</script>
</body>
</html>