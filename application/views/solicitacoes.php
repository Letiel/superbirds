<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<title>Solicitações - SuperBirds</title>
	<?php include 'inc/interno-head.php' ?>
</head>
<body class="blue-grey lighten-5">
	<?php include 'inc/interno-menu_lateral.php' ?>
	<?php include 'inc/interno-topo.php' ?>
	<main>
		<div class="container-fluid">

			<div class="card">
				<div class="card-content">
					<div class="left">
						<p class="card-title blue-grey-text">Suas Solicitações - SuperBirds</p>
					</div>
					<div class="right">
						<a href='/solicitacoes/nova' class="btn blue"><i class="fas fa-plus"></i> Nova Solicitação</a>
					</div>
					<div class="clearfix"></div>
					<hr>
					<div class="row">
						<table class='bordered highlight responsive-table'>
							<tr>
								<th>Tipo de solicitação</th>
								<th>Descrição</th>
								<th>Data</th>
								<th>Situação</th>
							</tr>
							<?php foreach ($solicitacoes as $solicitacao): ?>
								<?php
									$tipo = "";
									switch ($solicitacao->tipo_solicitacao) {
										case 'PL':
											$tipo_solicitacao = "Troca de Plano";
											$tipo = "Plano: ";
											break;
										case 'AR':
											$tipo_solicitacao = "Compra de Artigo";
											break;
										case 'EV':
											$tipo_solicitacao = "Compra de Evento";
											break;
										case 'AD':
											$tipo_solicitacao = "Compra de Anúncio Destaque";
											break;
										default:
											$tipo_solicitacao = "Solicitação Desconhecida";
											break;
									}

									switch ($solicitacao->situacao) {
										case 'N':
											$situacao = "<span class='red-text'>Negado</span>";
											break;
										case 'R':
											$situacao = "<span class='green-text'>Realizado</span>";
											break;
										default:
											$situacao = "<span class='blue-text'>Solicitado</span>";
											break;
									}
								?>
								<tr>
									<td><?php echo $tipo_solicitacao ?></td>
									<td><?php echo $tipo."<b>".$solicitacao->selecionado."</b>" ?></td>
									<td><?php echo empty($solicitacao->data_criacao) ? "" : date("d/m/Y", strtotime($solicitacao->data_criacao)) ?></td>
									<td><?php echo $situacao ?></td>
								</tr>
							<?php endforeach ?>
						</table>
					</div>
					<div class="right-align">
						<?php echo $paginacao ?>
					</div>
				</div>
			</div>
		</div>
	</main>

	<?php include 'inc/interno-footer.php' ?>
	<?php include 'inc/interno-js.php' ?>
</body>
</html>