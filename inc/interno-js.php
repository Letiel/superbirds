<script type="text/javascript" src="/js/jquery.min.js"></script>
<script type="text/javascript" src="/js/materialize.min.js"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
<script type="text/javascript">
	var ps2 = new PerfectScrollbar('ul');
	$(".dropdown-button").dropdown();
	$(".button-collapse").sideNav({
		menuWidth: 250,
		draggable: true
	});
	$(document).ready(function(){
		<?php echo $this->session->flashdata("toast"); ?>
	});
</script>