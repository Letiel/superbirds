<!-- MENU LATERAL -->
<ul id="slide-out" class="side-nav fixed green darken-3">
	<li><a href="/interno" class="white-text"><i class="material-icons white-text">dashboard</i> Quadro Inicial</a></li>

	<li><a class="waves-effect white-text" href="/perfil"><i class="fas fa-user white-text" style="margin-right: 32px; font-size: 24px;"></i> Seu Perfil</a></li>
	<li><a class="waves-effect white-text" href="/aves"><i class="fab fa-earlybirds white-text" style="margin-right: 32px; font-size: 24px;"></i> Aves</a></li>
	<li><a class="waves-effect white-text" href="/produtos"><i class="fas fa-cubes white-text" style="margin-right: 32px; font-size: 24px;"></i> Produtos</a></li>
	<li><a class="waves-effect white-text" href="/servicos"><i class="fas fa-wrench white-text" style="margin-right: 32px; font-size: 24px;"></i> Serviços</a></li>
	<li><a class="waves-effect white-text" href="/eventos"><i class="fas fa-calendar white-text" style="margin-right: 32px; font-size: 24px;"></i> Eventos</a></li>
	<li><a class="waves-effect white-text" href="/artigos"><i class="fas fa-quote-right white-text" style="margin-right: 32px; font-size: 24px;"></i> Artigos</a></li>
	<li><a class="waves-effect white-text" href="/solicitacoes"><i class="fas fa-shopping-basket white-text" style="margin-right: 32px; font-size: 24px;"></i> Solicitações</a></li>
</ul>
<!-- FIM MENU LATERAL -->