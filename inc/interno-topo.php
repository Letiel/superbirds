
<header class="navbar-fixed">
	<nav class="white">
		<div class="nav-wrapper">
			<a href="/interno" class="brand-logo grey-text text-darken-4">Super<span>Birds</span></a>
			<a href="#" data-activates="slide-out" class="button-collapse grey-text text-darken-4"><i class="material-icons">menu</i></a>
			<ul class="right hide-on-med-and-down">
				<<li><a class="dropdown-button grey-text text-darken-4" href="#!" data-activates="dropdownSuperior">Opções<i class="material-icons right">arrow_drop_down</i></a></li>
			</ul>
		</div>
	</nav>
</header>
<ul id="dropdownSuperior" class="dropdown-content">
	<li><a href="/perfil" class='grey-text text-darken-4'>Perfil</a></li>
	<li><a href="/perfil/alterar-senha" class='grey-text text-darken-4'>Alterar Senha</a></li>
	<li class="divider" class='grey-text text-darken-4'></li>
	<li><a href="/logout" class='grey-text text-darken-4'>Sair</a></li>
</ul>