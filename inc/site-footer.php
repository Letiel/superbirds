<footer class="green darken-4 page-footer" style="margin-top: 7rem;">
	<div class="container">
		<div class="row">
			<div class="col l6 s12" style="margin-top: 20px;">
				<!-- <h5 class="white-text">SuperBirds</h5> -->
				<img src="/img/superbirds_logo_branco.png" class="responsive-img" />
				<p class="grey-text text-lighten-4">Portal de Criadores de Aves Exóticas, Produtos e Serviços.</p>
				<div class="col s4 offset-s4">
				</div>
			</div>
			<div class="col l4 offset-l2 s12">
				<h5 class="white-text">Conteúdo</h5>
				<ul>
					<li><a class="grey-text text-lighten-3" href="/criadores">Criadores</a></li>
					<li><a class="grey-text text-lighten-3" href="/clubes">Clubes</a></li>
					<li><a class="grey-text text-lighten-3" href="/lista/aves">Aves</a></li>
					<li><a class="grey-text text-lighten-3" href="/lista/produtos">Produtos</a></li>
					<li><a class="grey-text text-lighten-3" href="/lista/servicos">Serviços</a></li>
					<li><a class="grey-text text-lighten-3" href="/lista/artigos">Artigos</a></li>
					<li><a class="grey-text text-lighten-3" href="/lista/eventos">Eventos</a></li>
					<li><a class="grey-text text-lighten-3" href="/fale-conosco">Fale Conosco</a></li>

					<li><a class="grey-text text-lighten-3" href="/quem-somos">Quem Somos</a></li>
					<li><a class="grey-text text-lighten-3" href="/termos-de-uso">Termos de Uso</a></li>

					<li><a href="/cadastro" class="grey-text text-lighten-3">Cadastrar-se</a></a></li>
					<li><a href="/interno" class="grey-text text-lighten-3">Entrar</a></a></li>

				</ul>
			</div>
		</div>
	</div>
	<div class="footer-copyright">
		<div class="container center-align">
			© 2018 Copyright
			<b><a class="grey-text text-lighten-4" href="/">SuperBirds</a></b>
		</div>
	</div>
</footer>