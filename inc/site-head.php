<meta charset="utf-8">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="/css/materialize.min.css">
<link href="/css/perfect-scrollbar.css" rel="stylesheet">
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<style media="screen">

	.brand-logo{
		width: 250px;
		text-align: center;
		font-weight: 100;
		color: #fff !important;
		font-weight: bold;
	}

	.icon-topo{
		margin-top: 1rem;
		margin-right: .8rem;
	}

	.btn-block{
		display: block !important;
		width: 100% !important;
	}
	/*SLIDER*/
	.slider{
		height: 400px !important;
	}
	.slider ul.indicators{
		z-index: 9;
		bottom: 22px !important;
	}
	.slider .indicators .indicator-item.active {
		background-color: #1b5e20;
	}
	/*SLICK*/

	.slick-slider {
		width: 100%;
		margin: 20px 0;
	}

	.slick-slide {
		margin: 0px 20px;
	}

	.slick-slide img {
		width: 100%;
	}

	.slick-prev:before,
	.slick-next:before {
		color: black;
	}


	/*.slick-slide {
		transition: all ease-in-out .3s;
		opacity: .2;
	}

	.slick-active {
		opacity: .5;
	}

	.slick-current {
		opacity: 1;
	}*/

	.slick-slider .card .card-image .card-title{
		width: 100%;
		text-shadow: 2px 2px rgba(0,0,0,0.87);
	}
	.slick-slider .card.horizontal .card-image {
		max-width: 30%;
	}
	.slick-slider .card.small{
		height: 175px;
	}
	.slick-slider a.link{

		display: block;
		width: 150px;
		float: right;
		padding: 2px 5px;
	}
	.slick-slider a.link i{
		border:1px solid #fff;
	}
	.slick-slider a.link:hover i{
		background-color:  #2e7d32;
		color:#fff !important;
	}
</style>