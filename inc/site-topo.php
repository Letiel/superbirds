<!-- NAVBAR -->

<header class="navbar-fixed">
	<nav class="green darken-4">
		<div class="nav-wrapper">
			<a href="/" class="brand-logo">Super<span>Birds</span></a>
			<a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
			<ul class="right hide-on-med-and-down">
				<li><a href="/criadores" class="white-text text-darken-2">Criadores</a></a></li>
				<li><a href="/clubes" class="white-text text-darken-2">Clubes</a></a></li>
				<li><a href="/lista/aves" class="white-text text-darken-2">Aves</a></a></li>
				<li><a href="/lista/produtos" class="white-text text-darken-2">Produtos</a></a></li>
				<li><a href="/lista/servicos" class="white-text text-darken-2">Serviços</a></a></li>
				<li><a href="/lista/artigos" class="white-text text-darken-2">Artigos</a></a></li>
				<li><a href="/lista/eventos" class="white-text text-darken-2">Eventos</a></a></li>
				<li><a href="/fale-conosco" class="white-text text-darken-2">Fale Conosco</a></a></li>
				<li><a href="/cadastro" class="white-text text-darken-2">Cadastrar-se</a></a></li>
				<li><a href="/interno" class="white-text text-darken-2">Entrar</a></a></li>
			</ul>
		</div>
	</nav>

</header>
<ul class="side-nav" id="mobile-demo">
	<li><a href="/criadores" class="">Criadores</a></a></li>
	<li><a href="/clubes" class="">Clubes</a></a></li>
	<li><a href="/lista/aves" class="">Aves</a></a></li>
	<li><a href="/lista/produtos" class="">Produtos</a></a></li>
	<li><a href="/lista/servicos" class="">Serviços</a></a></li>
	<li><a href="/lista/artigos" class="">Artigos</a></a></li>
	<li><a href="/lista/eventos" class="">Eventos</a></a></li>
	<li><a href="/fale-conosco" class="">Fale Conosco</a></a></li>
	<li><a href="/cadastro" class="">Cadastrar-se</a></a></li>
	<li><a href="/interno" class="">Entrar</a></a></li>
</ul>

<!-- NAVBAR -->