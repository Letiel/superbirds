<?php

require __DIR__.'/ImgPicker.php';

if (isset($_POST['data']['uniqid'])) {
    if($_POST['data']['uniqid'] != null && $_POST['data']['uniqid'] != "")
        $_POST['data']['uniqid'] = str_replace(array('/img/', '.png', '.jpg', '.jpeg'), "", $_POST['data']['uniqid']);
    else
        $_POST['data']['uniqid'] = uniqid();
}else{
    $_POST['data']['uniqid'] = uniqid();
}

$options = array(
    // Upload directory path.
    'upload_dir' => __DIR__.'/../img/',

    // Upload directory url.
    'upload_url' => '/img/',

    // Image versions.
    'versions' => array(
        // This will create 2 image versions: the original one and a 500x500 one
        '' => array(
            //'upload_dir' => '',
            //'upload_url' => '',
            // Create square image
            'crop' => true,
            'max_width' => 500,
            'max_height' => 500
            ),
        ),

    /**
     * Load callback.
     *
     * @return string|array
     */
    'load' => function () {
        // return 'avatar.jpg';
    },

    /**
     * Delete callback.
     *
     * @param  string $filename
     * @return bool
     */
    'delete' => function ($filename) {
        return true;
    },

    /**
     * Upload start callback.
     *
     * @param  stdClass $image
     * @return void
     */
    'upload_start' => function ($image) {
        $image->name = $_POST['data']['uniqid'] . "." . $image->type;
    },

    /**
     * Upload complete callback.
     *
     * @param  stdClass $image
     * @return void
     */
    'upload_complete' => function ($image) {

    },

    /**
     * Crop start callback.
     *
     * @param  stdClass $image
     * @return void
     */
    'crop_start' => function ($image) {
        $image->name = $_POST['data']['uniqid'] . "." . $image->type;
    },

    /**
     * Crop complete callback.
     *
     * @param  stdClass $image
     * @return void
     */
    'crop_complete' => function ($image) {

    }
    );

// Create new ImgPicker instance.
new ImgPicker($options);
